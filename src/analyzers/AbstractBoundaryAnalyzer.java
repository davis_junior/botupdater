package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractBoundary;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.Interactable;
import data.Model;
import data.Player;

public class AbstractBoundaryAnalyzer extends Analyzer {
	
	public static AbstractBoundaryAnalyzer get = new AbstractBoundaryAnalyzer();
	
	public AbstractBoundaryAnalyzer() {
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.isAbstract())
			return false;
		
		if (!cg.getSuperclassName().equals(Interactable.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			if (mg.getArgumentTypes() == null || mg.getArgumentTypes().length < 1)
				continue;
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1_1 = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.SHORT),
					new PI_BIPUSH(cpg).adjustValue(16),
					IfInstruction.class
			);
			
			final CompareInstruction[] pattern1_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(16),
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.SHORT),
					IfInstruction.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1_1);
			patterns1.add(pattern1_2);
			
			final CompareInstruction[] pattern2_1 = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.SHORT),
					new PI_BIPUSH(cpg).adjustValue(32),
					IfInstruction.class
			);
			
			final CompareInstruction[] pattern2_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(32),
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.SHORT),
					IfInstruction.class
			);
			
			List<CompareInstruction[]> patterns2 = new ArrayList<>();
			patterns2.add(pattern2_1);
			patterns2.add(pattern2_2);
			
			final CompareInstruction[] pattern3_1 = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.SHORT),
					new PI_BIPUSH(cpg).adjustValue(64),
					IfInstruction.class
			);
			
			final CompareInstruction[] pattern3_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(64),
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.SHORT),
					IfInstruction.class
			);
			
			List<CompareInstruction[]> patterns3 = new ArrayList<>();
			patterns3.add(pattern3_1);
			patterns3.add(pattern3_2);
			
			final CompareInstruction[] pattern4_1 = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.SHORT),
					new PI_SIPUSH(cpg).adjustValue(128),
					IfInstruction.class
			);
			
			final CompareInstruction[] pattern4_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_SIPUSH(cpg).adjustValue(128),
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.SHORT),
					IfInstruction.class
			);
			
			List<CompareInstruction[]> patterns4 = new ArrayList<>();
			patterns4.add(pattern4_1);
			patterns4.add(pattern4_2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns1);
			InstructionHandle[][] matches2 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns2);
			InstructionHandle[][] matches3 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns3);
			InstructionHandle[][] matches4 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns4);
			
			if (matches1 != null && matches2 != null && matches3 != null && matches4 != null) {
				InstructionHandle[] matches1combined = Util.combine(matches1[0], matches1[1]);
				InstructionHandle[] matches2combined = Util.combine(matches2[0], matches2[1]);
				InstructionHandle[] matches3combined = Util.combine(matches3[0], matches3[1]);
				InstructionHandle[] matches4combined = Util.combine(matches4[0], matches4[1]);
				
				if (matches1combined != null && matches1combined.length == 1
						&& matches2combined != null && matches2combined.length == 1
						&& matches3combined != null && matches3combined.length == 1
						&& matches4combined != null && matches4combined.length == 1) {
					
					found = true;
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Interactable.get.getInternalName() == null) {
			System.out.println("ERROR: AbstractBoundaryAnalyzer: Interactable internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			AbstractBoundary.get.setInternalName(cg.getClassName());
		}
	}

}
