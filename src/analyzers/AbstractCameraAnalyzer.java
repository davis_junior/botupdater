package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractCamera;
import data.AbstractCameraLocationData;
import data.AbstractCentralLocationData;
import data.Model;
import data.Player;

public class AbstractCameraAnalyzer extends Analyzer {
	
	public static AbstractCameraAnalyzer get = new AbstractCameraAnalyzer();

	//CountInfoItem findCameraLocationData = new CountInfoItem("findCameraLocationData");
	//CountInfoItem findCentralLocationData = new CountInfoItem("findCentralLocationData");
	
	public AbstractCameraAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findCameraLocationData, findCentralLocationData);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.isAbstract())
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						ALOAD.class,
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(5120.0F), new PI_LDC_W(cpg).adjustValue(5120.0F)),
						PUTFIELD.class,
						ALOAD.class,
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(10.0F), new PI_LDC_W(cpg).adjustValue(10.0F)),
						PUTFIELD.class,
						ALOAD.class,
						new FCONST(1),
						PUTFIELD.class
						));
	
				if (matches != null) {
					//for (InstructionHandle ih : matches) {
						found = true;
					//}
				}
			//}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean foundCameraLocationData = false;
		boolean foundCentralLocationData = false;
		
		if (cg.getClassName().equals(AbstractCamera.get.getInternalName())) {
			for (Field field : cg.getFields()) {
				if (field.getType().toString().equals(AbstractCameraLocationData.get.getInternalName())) {
					AbstractCamera.get.cameraLocationData.set(cg.getClassName(), field.getName(), field.getSignature());
					foundCameraLocationData = true;
					//findCameraLocationData.add(method, pos);
				}
				
				if (field.getType().toString().equals(AbstractCentralLocationData.get.getInternalName())) {
					AbstractCamera.get.centralLocationData.set(cg.getClassName(), field.getName(), field.getSignature());
					foundCentralLocationData = true;
					//findCentralLocationData.add(method, pos);
				}
			}
			
			if (foundCameraLocationData && foundCentralLocationData)
				found = true;
		}
	
		return found;
	}
	
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			AbstractCamera.get.setInternalName(cg.getClassName());

			if (AbstractCameraLocationData.get.getInternalName() == null) {
				System.out.println("ERROR: AbstractCameraAnalyzer: AbstractCameraLocationData internal class name is null!");
				return;
			}
			
			if (AbstractCentralLocationData.get.getInternalName() == null) {
				System.out.println("ERROR: AbstractCameraAnalyzer: AbstractCentralLocationData internal class name is null!");
				return;
			}
			
			findFields(cg);
		}
	}

}
