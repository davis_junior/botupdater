package analyzers;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import searcher.Comparator.CompareType;
import searcher.Searcher;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractCameraLocationData;
import data.CameraLocationData;
import data.Model;
import data.Player;

public class AbstractCameraLocationDataAnalyzer extends Analyzer {
	
	public static AbstractCameraLocationDataAnalyzer get = new AbstractCameraLocationDataAnalyzer();

	//CountInfoItem findPoint1 = new CountInfoItem("findPoint1");
	//CountInfoItem findPoint2 = new CountInfoItem("findPoint2");
	//CountInfoItem findQuaternion = new CountInfoItem("findQuaternion");
	
	public AbstractCameraLocationDataAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findPoint1, findPoint2, findQuaternion);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		/*if (!cg.isAbstract())
			return false;

		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (Method m : cg.getMethods()) {
			MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			if (mg.getArgumentTypes().length > 0 && mg.getArgumentTypes()[0].equals(Type.getType(int[][][].class))) {
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new PI_LDC_W(cpg).adjustType(Type.FLOAT).adjustValue(3.1415927F)
						);
	
				if (matches != null) {
					//for (InstructionHandle ih : matches) {
						found = true;
					//}
				}
			}
		}
	
		return found;*/
		return false;
	}
	
	private boolean findClass(ClassGen cg) {
		if (cg.getClassName().equals(CameraLocationData.get.getInternalName())) {
			AbstractCameraLocationData.get.setInternalName(cg.getSuperclassName());
			return true;
		}
		
		return false;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (CameraLocationData.get.getInternalName() == null) {
			System.out.println("ERROR: AbstractCameraLocationData: CameraLocationData internal class name is null!");
			return;
		}
		
		findClass(cg);
		
		//if (verifyClass(cg)) {
		//	AbstractCameraLocationData.get.setInternalName(cg.getClassName());
		//}
	}

}
