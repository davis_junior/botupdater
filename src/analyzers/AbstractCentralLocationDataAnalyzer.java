package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DCONST;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import searcher.Comparator.CompareType;
import searcher.Searcher;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractCameraLocationData;
import data.AbstractCentralLocationData;
import data.CameraLocationData;
import data.CentralLocationData;
import data.Player;

public class AbstractCentralLocationDataAnalyzer extends Analyzer {
	
	public static AbstractCentralLocationDataAnalyzer get = new AbstractCentralLocationDataAnalyzer();

	//CountInfoItem findName = new CountInfoItem("findName");
	//CountInfoItem findTitle = new CountInfoItem("findTitle");
	
	public AbstractCentralLocationDataAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findName, findTitle);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		/*
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (Method m : cg.getMethods()) {
			MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new DCONST(0),
					new DCONST(0),
					new DCONST(0)
			};
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new FCONST(0),
					new FCONST(1),
					new FCONST(0)
			};
			
			List<Object[]> patterns = new ArrayList<Object[]>();
			patterns.add(pattern1);
			patterns.add(pattern2);
			
			InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);

			if (matches != null && matches.length == patterns.size()) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
		
		return found;*/
		return false;
	}
	
	private boolean findClass(ClassGen cg) {
		if (cg.getClassName().equals(CentralLocationData.get.getInternalName())) {
			AbstractCentralLocationData.get.setInternalName(cg.getSuperclassName());
			return true;
		}
		
		return false;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (CentralLocationData.get.getInternalName() == null) {
			System.out.println("ERROR: AbstractCentralLocationData: CentralLocationData internal class name is null!");
			return;
		}
		
		findClass(cg);
		
		//if (verifyClass(cg)) {
		//	AbstractCentralLocationData.get.setInternalName(cg.getClassName());

			//findNameAndTitle(cg);
		//}
	}

}
