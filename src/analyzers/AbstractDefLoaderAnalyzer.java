package analyzers;

import org.apache.bcel.generic.ClassGen;

import data.AbstractDefLoader;
import data.DefLoader;

public class AbstractDefLoaderAnalyzer extends Analyzer {
	
	public static AbstractDefLoaderAnalyzer get = new AbstractDefLoaderAnalyzer();
	
	public AbstractDefLoaderAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add();
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		return false;
	}
	
	protected boolean findClass(ClassGen cg) {
		
		boolean found = false;
		
		if (cg.getClassName().equals(DefLoader.get.getInternalName())) {
			String[] interfaces = cg.getInterfaceNames();
			if (interfaces != null && interfaces.length > 0) {
				AbstractDefLoader.get.setInternalName(interfaces[0]);
			}
		}
		
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (DefLoader.get.getInternalName() == null) {
			System.out.println("ERROR: AbstractDefLoaderAnalyzer: DefLoader internal class name is null!");
			return;
		}
		
		findClass(cg);
	}

}
