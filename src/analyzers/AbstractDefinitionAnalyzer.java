package analyzers;

import org.apache.bcel.generic.ClassGen;

import data.AbstractDefinition;
import data.ObjectDefinition;

public class AbstractDefinitionAnalyzer extends Analyzer {
	
	public static AbstractDefinitionAnalyzer get = new AbstractDefinitionAnalyzer();
	
	public AbstractDefinitionAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add();
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		return false;
	}
	
	protected boolean findClass(ClassGen cg) {
		
		boolean found = false;
		
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			String[] interfaces = cg.getInterfaceNames();
			if (interfaces != null && interfaces.length > 0) {
				AbstractDefinition.get.setInternalName(interfaces[0]);
			}
		}
		
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (ObjectDefinition.get.getInternalName() == null) {
			System.out.println("ERROR: AbstractDefinitionAnalyzer: ObjectDefinition internal class name is null!");
			return;
		}
		
		findClass(cg);
	}

}
