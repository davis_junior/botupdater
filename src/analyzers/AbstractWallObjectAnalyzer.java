package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractBoundary;
import data.AbstractFloorObject;
import data.AbstractWallObject;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.GroundInfo;
import data.Interactable;
import data.Model;
import data.Player;

public class AbstractWallObjectAnalyzer extends Analyzer {
	
	public static AbstractWallObjectAnalyzer get = new AbstractWallObjectAnalyzer();
	
	public AbstractWallObjectAnalyzer() {
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.isAbstract())
			return false;
		
		if (!cg.getSuperclassName().equals(Interactable.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		int nonStaticFieldCount = 0;
		int shortCount = 0;
		int intCount = 0;
		int arrayCount = 0;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				nonStaticFieldCount++;
				
				if (f.getType().equals(Type.SHORT))
					shortCount++;
				else if (f.getType().equals(Type.INT))
					intCount++;
				else if (f.getType().toString().contains("[]"))
					arrayCount++;
			}
		}
		
		if (nonStaticFieldCount != 4)
			return false;
		
		if (shortCount != 2)
			return false;
		
		if (intCount != 1)
			return false;
		
		if (arrayCount != 1)
			return false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustTypeString(false, GroundInfo.get.getInternalName())
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					ISHR.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null) {
				//InstructionHandle[] matches1combined = Util.combine(matches1[0], matches1[1]);
				
				//if (matches1combined != null && matches1combined.length == 1) {
					found = true;
				//}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Interactable.get.getInternalName() == null) {
			System.out.println("ERROR: AbstractWallObjectAnalyzer: Interactable internal class name is null!");
			return;
		}
		
		if (GroundInfo.get.getInternalName() == null) {
			System.out.println("ERROR: AbstractWallObjectAnalyzer: GroundInfo internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			AbstractWallObject.get.setInternalName(cg.getClassName());
		}
	}

}
