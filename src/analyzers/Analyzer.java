package analyzers;

import org.apache.bcel.generic.ClassGen;

import analyzers.countinfo.CountInfo;

public abstract class Analyzer {

	protected CountInfo countInfo;
	
	public void resetCountInfo() {
		countInfo.reset();
	}
	
	public void printCountInfo(boolean detailed) {
		countInfo.print(detailed);
	}
	
	protected abstract boolean verifyClass(ClassGen cg);
	
	public abstract void analyze(ClassGen cg);
}
