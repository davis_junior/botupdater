package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IF_ICMPLT;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC_W;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_GETSTATIC;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AnimableNode;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.Player;

public class AnimableNodeAnalyzer extends Analyzer {
	
	public static AnimableNodeAnalyzer get = new AnimableNodeAnalyzer();

	CountInfoItem findNext = new CountInfoItem("findNext");
	CountInfoItem findAnimable = new CountInfoItem("findAnimable");
	
	public AnimableNodeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findNext, findAnimable);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_GETSTATIC(cpg).adjustClassName(cg.getClassName()),
					new SynonymInstruction(LDC.class, LDC_W.class),
					IMUL.class,
					new PI_SIPUSH(cpg).adjustValue(500),
					IF_ICMPLT.class,
					RETURN.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(LDC.class, LDC_W.class),
					new PI_GETSTATIC(cpg).adjustClassName(cg.getClassName()),
					IMUL.class,
					new PI_SIPUSH(cpg).adjustValue(500),
					IF_ICMPLT.class,
					RETURN.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null) {
				//InstructionHandle[] matches1combined = BotFinder.combine(matches1[0], matches1[1]);
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			
			
			
				// find fields
				
				final CompareInstruction[] patternAnimable = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						ACONST_NULL.class,
						PUTFIELD.class
				);
				
				InstructionHandle[] matchesAnimable = searcher.searchAllFromStart(false, CompareType.Opcode, patternAnimable);
				
				if (matchesAnimable != null) {
					for (InstructionHandle ih : matchesAnimable) {
						//searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih.getNext().getNext();
						
						if (fieldIh != null) {
							PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
							AnimableNode.get.animable.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							//found = true;
							
							findAnimable.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						}
					}
				}
				
				final CompareInstruction[] patternNext = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETSTATIC(cpg).adjustTypeString(false, cg.getClassName()),
						new PI_PUTFIELD(cpg).adjustClassName(cg.getClassName()).adjustTypeString(false, cg.getClassName())
				);
				
				InstructionHandle[] matchesNext = searcher.searchAllFromStart(false, CompareType.Opcode, patternNext);
				
				if (matchesNext != null) {
					for (InstructionHandle ih : matchesNext) {
						//searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih.getNext().getNext();
						
						if (fieldIh != null) {
							PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
							AnimableNode.get.next.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							//found = true;
							
							findNext.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						}
					}
				}
			}
			//}
		}
	
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			AnimableNode.get.setInternalName(cg.getClassName());

			//findFields(cg);
		}
	}

}
