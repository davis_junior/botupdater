package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;

import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AnimatedAnimableObject;
import data.AnimatedObject;
import data.RSAnimable;

public class AnimatedAnimableObjectAnalyzer extends Analyzer {
	
	public static AnimatedAnimableObjectAnalyzer get = new AnimatedAnimableObjectAnalyzer();

	CountInfoItem findAnimatedObject = new CountInfoItem("findAnimatedObject");
	
	public AnimatedAnimableObjectAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findAnimatedObject);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(RSAnimable.get.getInternalName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (Field f : cg.getFields()) {
			if (f.getType().toString().equals(AnimatedObject.get.getInternalName())) {
				AnimatedAnimableObject.get.animatedObject.set(cg.getClassName(), f.getName(), f.getSignature());					
				found = true;
				
				findAnimatedObject.add(cg.getClassName() + ".<FIELDS>", 0);
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (RSAnimable.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedAnimableObjectAnalyzer: RSAnimable internal class name is null!");
			return;
		}
		
		if (AnimatedObject.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedAnimableObjectAnalyzer: AnimatedObject internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			AnimatedAnimableObject.get.setInternalName(cg.getClassName());
		}
	}

}
