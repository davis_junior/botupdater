package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;

import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractBoundary;
import data.AnimatedAnimableObject;
import data.AnimatedBoundaryObject;
import data.AnimatedObject;
import data.RSAnimable;

public class AnimatedBoundaryObjectAnalyzer extends Analyzer {
	
	public static AnimatedBoundaryObjectAnalyzer get = new AnimatedBoundaryObjectAnalyzer();

	CountInfoItem findAnimatedObject = new CountInfoItem("findAnimatedObject");
	
	public AnimatedBoundaryObjectAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findAnimatedObject);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(AbstractBoundary.get.getInternalName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (Field f : cg.getFields()) {
			if (f.getType().toString().equals(AnimatedObject.get.getInternalName())) {
				AnimatedBoundaryObject.get.animatedObject.set(cg.getClassName(), f.getName(), f.getSignature());					
				found = true;
				
				findAnimatedObject.add(cg.getClassName() + ".<FIELDS>", 0);
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (AbstractBoundary.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedBoundaryObjectAnalyzer: AbstractBoundary internal class name is null!");
			return;
		}
		
		if (AnimatedObject.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedBoundaryObjectAnalyzer: AnimatedObject internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			AnimatedBoundaryObject.get.setInternalName(cg.getClassName());
		}
	}

}
