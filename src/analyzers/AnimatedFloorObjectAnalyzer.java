package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;

import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractBoundary;
import data.AbstractFloorObject;
import data.AnimatedAnimableObject;
import data.AnimatedBoundaryObject;
import data.AnimatedFloorObject;
import data.AnimatedObject;
import data.RSAnimable;

public class AnimatedFloorObjectAnalyzer extends Analyzer {
	
	public static AnimatedFloorObjectAnalyzer get = new AnimatedFloorObjectAnalyzer();

	CountInfoItem findAnimatedObject = new CountInfoItem("findAnimatedObject");
	
	public AnimatedFloorObjectAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findAnimatedObject);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(AbstractFloorObject.get.getInternalName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (Field f : cg.getFields()) {
			if (f.getType().toString().equals(AnimatedObject.get.getInternalName())) {
				AnimatedFloorObject.get.animatedObject.set(cg.getClassName(), f.getName(), f.getSignature());					
				found = true;
				
				findAnimatedObject.add(cg.getClassName() + ".<FIELDS>", 0);
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (AbstractFloorObject.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedFloorObjectAnalyzer: AbstractFloorObject internal class name is null!");
			return;
		}
		
		if (AnimatedObject.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedFloorObjectAnalyzer: AnimatedObject internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			AnimatedFloorObject.get.setInternalName(cg.getClassName());
		}
	}

}
