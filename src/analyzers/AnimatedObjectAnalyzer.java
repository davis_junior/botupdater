package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC_W;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AnimableObject;
import data.AnimatedObject;
import data.Animator;
import data.BaseInfo;
import data.Cache;
import data.Character;
import data.GameInfo;
import data.IdComposite_AnimatedObject;
import data.Interactable;
import data.Model;
import data.ObjectCacheLoader;
import data.ObjectDefLoader;
import data.ObjectDefinition;
import data.Player;

public class AnimatedObjectAnalyzer extends Analyzer {
	
	public static AnimatedObjectAnalyzer get = new AnimatedObjectAnalyzer();

	CountInfoItem findId = new CountInfoItem("findId");
	CountInfoItem findModel = new CountInfoItem("findModel");
	CountInfoItem findDefinitionLoader = new CountInfoItem("findDefinitionLoader");
	CountInfoItem findAnimator1 = new CountInfoItem("findAnimator1");
	CountInfoItem findAnimator2 = new CountInfoItem("findAnimator2");
	CountInfoItem findFace = new CountInfoItem("findFace");
	CountInfoItem findAnimationId = new CountInfoItem("findAnimationId");
	CountInfoItem findIdComposite = new CountInfoItem("findIdComposite");
	
	public AnimatedObjectAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findId, findModel, findDefinitionLoader, findAnimator1, findAnimator2, findFace, findAnimationId, findIdComposite);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (!mg.getReturnType().toString().equals(Model.get.getInternalName()))
				continue;
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(CompareType.Opcode, new PI_LDC(cpg).adjustValue(0x40000), new PI_LDC_W(cpg).adjustValue(0x40000)),
					IOR.class
			);
			
			// just used for finding model field
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_GETFIELD(cpg).adjustClassName(AnimatedObject.get.getInternalName()).adjustTypeString(false, Model.get.getInternalName())
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				for (InstructionHandle ih : matches1[1]) {
					InstructionHandle fieldIh = ih;
					
					if (fieldIh != null) {
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						AnimatedObject.get.model.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
						found = true;
						
						findModel.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
					}
				}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		boolean found = false;
		
		List<Field> animatorFields = new ArrayList<>();
		
		for (Field f : cg.getFields()) {
			if (f.isStatic())
				continue;
			
			// TODO: use patterns (0x40000) to find the right animator order (1 and 2)
			if (f.getType().getSignature().equals("L" + Animator.get.getInternalName() + ";")) {
				animatorFields.add(f);
			}
			
			if (f.getType().getSignature().equals("L" + Interactable.get.getInternalName() + ";")) {
				AnimatedObject.get.interactable.set(cg.getClassName(), f.getName(), f.getSignature());
			}
		}
		
		if (animatorFields.size() == 2) {
			Field field1 = animatorFields.get(0);
			AnimatedObject.get.animator1.set(cg.getClassName(), field1.getName(), field1.getSignature());
			
			Field field2 = animatorFields.get(1);
			AnimatedObject.get.animator2.set(cg.getClassName(), field2.getName(), field2.getSignature());
			
			found = true;
		}
		
		return found;
	}
	
	private boolean findIdAndObjectDefLoader(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(AnimatedObject.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.getReturnType().toString().equals(ObjectDefinition.get.getInternalName()))
					continue;
				
				//if (mg.getArgumentTypes().length != 0)
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(AnimatedObject.get.getInternalName()).adjustType(Type.INT)
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(AnimatedObject.get.getInternalName()).adjustTypeString(false, ObjectDefLoader.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							AnimatedObject.get.id.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findId.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoStart();
							AnimatedObject.get.id.set(searcher.findNextGetMultiplier(false, false, AnimatedObject.get.id, cpg));
						}
					}
					
					for (InstructionHandle ih : matches1[1]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							AnimatedObject.get.definitionLoader.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findDefinitionLoader.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	// TODO: could use actual pattern for animationId instead of searching previous from face
	// similar to AnimableObject.findAnimationIdAndFace() except the fields are multipliers and are ints
	private boolean findAnimationIdAndFace(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(AnimatedObject.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1_1_1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, LDC.class, LDC_W.class),
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustType(Type.INT),
						IMUL.class,
						new ICONST(4),
						IADD.class
				);
				
				// switched multiplier
				final CompareInstruction[] pattern1_1_2 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustType(Type.INT),
						new SynonymInstruction(CompareType.Opcode, LDC.class, LDC_W.class),
						IMUL.class,
						new ICONST(4),
						IADD.class
				);
				
				// switched inconst_4
				final CompareInstruction[] pattern1_2_1 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(4),
						new SynonymInstruction(CompareType.Opcode, LDC.class, LDC_W.class),
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustType(Type.INT),
						IMUL.class,
						IADD.class
				);
				
				// switched inconst_4, switch multiplier
				final CompareInstruction[] pattern1_2_2 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(4),
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustType(Type.INT),
						new SynonymInstruction(CompareType.Opcode, LDC.class, LDC_W.class),
						IMUL.class,
						IADD.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1_1_1);
				patterns1.add(pattern1_1_2);
				patterns1.add(pattern1_2_1);
				patterns1.add(pattern1_2_2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns1);
				
				if (matches1 != null) {
					InstructionHandle[] matches1combined = Util.combine(matches1);
					
					for (InstructionHandle ih : matches1combined) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							AnimatedObject.get.face.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findFace.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoHandle(ih);
							AnimatedObject.get.face.set(searcher.findNextGetMultiplier(false, false, AnimatedObject.get.face, cpg));
							
							
							searcher.gotoHandle(ih);
							InstructionHandle animationIh = ih;
							while (animationIh != null) {
								animationIh = searcher.searchPrev(true, CompareType.Opcode, new PI_GETFIELD(cpg).adjustType(Type.INT));
								
								if (animationIh != null) {
									GETFIELD instr2 = (GETFIELD) animationIh.getInstruction();
									
									if (!instr2.getFieldName(cpg).equals(AnimatedObject.get.face.getInternalName())) {
										AnimatedObject.get.animationId.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));					
										found = true;
										
										findAnimationId.add(cg.getClassName() + "." + mg.getName(), animationIh.getPosition());
										
										searcher.gotoHandle(ih);
										AnimatedObject.get.animationId.set(searcher.findPrevGetMultiplier(false, false, AnimatedObject.get.animationId, cpg));
										
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findIdComposite(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(AnimatedObject.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.getReturnType().toString().equals(Model.get.getInternalName()))
					continue;
				
				//if (mg.getArgumentTypes().length != 0)
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(AnimatedObject.get.getInternalName()).adjustTypeString(false, IdComposite_AnimatedObject.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							AnimatedObject.get.idComposite.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findIdComposite.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedObjectAnalyzer: Model internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			AnimatedObject.get.setInternalName(cg.getClassName());
			
			findAnimationIdAndFace(cg);
			
			
			if (Animator.get.getInternalName() == null) {
				System.out.println("ERROR: AnimatedObjectAnalyzer: Animator internal class name is null!");
				return;
			} else 	if (Interactable.get.getInternalName() == null) {
				System.out.println("ERROR: AnimatedObjectAnalyzer: Interactable internal class name is null!");
				return;
			} else
				findFields(cg);
		}
	}
	
	public void analyze_pass3(ClassGen cg) {
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedObjectAnalyzer: Model internal class name is null!");
			//return;
		} else if (IdComposite_AnimatedObject.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedObjectAnalyzer: IdComposite_AnimatedObject internal class name is null!");
			//return;
		} else
			findIdComposite(cg);
	}
	
	public void analyze_ObjectDefLoader(ClassGen cg) {
		if (ObjectDefinition.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedObjectAnalyzer: ObjectDefinition internal class name is null!");
			//return;
		} else if (ObjectDefLoader.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedObjectAnalyzer: ObjectDefLoader internal class name is null!");
			//return;
		} else
			findIdAndObjectDefLoader(cg);
	}
}
