package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;

import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractWallObject;
import data.AnimatedObject;
import data.AnimatedWallObject;

public class AnimatedWallObjectAnalyzer extends Analyzer {
	
	public static AnimatedWallObjectAnalyzer get = new AnimatedWallObjectAnalyzer();

	CountInfoItem findAnimatedObject = new CountInfoItem("findAnimatedObject");
	
	public AnimatedWallObjectAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findAnimatedObject);
	}
	
	@Override	
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(AbstractWallObject.get.getInternalName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (Field f : cg.getFields()) {
			if (f.getType().toString().equals(AnimatedObject.get.getInternalName())) {
				AnimatedWallObject.get.animatedObject.set(cg.getClassName(), f.getName(), f.getSignature());					
				found = true;
				
				findAnimatedObject.add(cg.getClassName() + ".<FIELDS>", 0);
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (AbstractWallObject.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedWallObjectAnalyzer: AbstractWallObject internal class name is null!");
			return;
		}
		
		if (AnimatedObject.get.getInternalName() == null) {
			System.out.println("ERROR: AnimatedWallObjectAnalyzer: AnimatedObject internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			AnimatedWallObject.get.setInternalName(cg.getClassName());
		}
	}

}
