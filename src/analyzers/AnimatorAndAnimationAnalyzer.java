package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Animation;
import data.Animator;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.Player;

public class AnimatorAndAnimationAnalyzer extends Analyzer {
	
	public static AnimatorAndAnimationAnalyzer get = new AnimatorAndAnimationAnalyzer();

	CountInfoItem Animator_findAnimation = new CountInfoItem("Animator_findAnimation");
	CountInfoItem Animation_findId = new CountInfoItem("Animation_findId");
	
	public AnimatorAndAnimationAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(Animator_findAnimation, Animation_findId);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("random"),
					ALOAD.class,
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()),
					new PI_GETFIELD(cpg).adjustTypeString(false, "int[]")
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					ALOAD.class,
					new ICONST(0),
					new PI_PUTFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.INT)
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				if (!(matches1[1].length == 3))
					continue;
				
				for (InstructionHandle ih : matches1[0]) {
					String firstGetFieldType = ((GETFIELD) ih.getNext().getNext().getInstruction()).getType(cpg).toString();
					String secondGetFieldClassName = ((GETFIELD) ih.getNext().getNext().getNext().getInstruction()).getClassName(cpg);
					
					if (firstGetFieldType.equals(secondGetFieldClassName) && !firstGetFieldType.equals(cg.getClassName())) {
						GETFIELD instr = ((GETFIELD) ih.getNext().getNext().getInstruction());
						Animator.get.animation.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
						found = true;
						
						Animator_findAnimation.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
						
						
						// Animation
						Animation.get.setInternalName(firstGetFieldType);
						
						searcher.gotoStart();
						InstructionHandle fieldIh = searcher.searchNext(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(firstGetFieldType).adjustType(Type.INT));
						if (fieldIh != null) {
							instr = (GETFIELD) fieldIh.getInstruction();
							Animation.get.id.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							Animation_findId.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
							
							searcher.gotoStart();
							Animation.get.id.set(searcher.findNextGetMultiplier(false, false, Animation.get.id, cpg));
						}
					}
				}
			}
			//}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Animator.get.setInternalName(cg.getClassName());
		}
	}

}
