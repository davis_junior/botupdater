package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.Player;

public class BaseInfoAnalyzer extends Analyzer {
	
	public static BaseInfoAnalyzer get = new BaseInfoAnalyzer();

	CountInfoItem findX = new CountInfoItem("findX");
	CountInfoItem findY = new CountInfoItem("findY");
	
	public BaseInfoAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findX, findY);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean found1 = false;
		boolean found2 = false;
		boolean found3 = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					IMUL.class,
					new PI_BIPUSH(cpg).adjustValue(28),
					ISHL.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					IMUL.class,
					new PI_BIPUSH(cpg).adjustValue(14),
					ISHL.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				//for (InstructionHandle ih : matches) {
					found1 = true;
				//}
			}
			
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					IMUL.class,
					new PI_BIPUSH(cpg).adjustValue(6),
					ISHR.class
			);
			
			final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
					IMUL.class,
					new PI_BIPUSH(cpg).adjustValue(63),
					IAND.class
			);
			
			List<CompareInstruction[]> patterns2 = new ArrayList<>();
			patterns2.add(pattern3);
			patterns2.add(pattern4);
			
			InstructionHandle[][] matches2 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns2);
			
			if (matches2 != null && matches2.length == patterns2.size()) {
				//for (InstructionHandle ih : matches) {
					found2 = true;
				//}
			}
			
			
			final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(9),
					ISHR.class
			);
			
			List<CompareInstruction[]> patterns3 = new ArrayList<>();
			patterns3.add(pattern5);
			
			InstructionHandle[][] matches3 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns3);
			
			if (matches3 != null && matches3.length == patterns3.size()) {
				//for (InstructionHandle ih : matches) {
					found3 = true;
				//}
			}
			//}
		}
		
		if (found1 && found2 && found3)
			found = true;
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(BaseInfo.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1_1 = CompareInstruction.Methods.toCompareInstructions(
						LDC.class,
						new ALOAD(0),
						GETFIELD.class,
						IMUL.class,
						new PI_BIPUSH(cpg).adjustValue(28),
						ISHL.class
				);
				
				final CompareInstruction[] pattern1_2 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						GETFIELD.class,
						LDC.class,
						IMUL.class,
						new PI_BIPUSH(cpg).adjustValue(28),
						ISHL.class
				);
				
				final CompareInstruction[] pattern2_1 = CompareInstruction.Methods.toCompareInstructions(
						LDC.class,
						new ALOAD(0),
						GETFIELD.class,
						IMUL.class,
						new PI_BIPUSH(cpg).adjustValue(14),
						ISHL.class
				);
				
				final CompareInstruction[] pattern2_2 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						GETFIELD.class,
						LDC.class,
						IMUL.class,
						new PI_BIPUSH(cpg).adjustValue(14),
						ISHL.class
				);
				
				final CompareInstruction[] pattern3_1 = CompareInstruction.Methods.toCompareInstructions(
						LDC.class,
						new ALOAD(0),
						GETFIELD.class,
						IMUL.class,
						new RejectedInstruction(BIPUSH.class)
				);
				
				final CompareInstruction[] pattern3_2 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						GETFIELD.class,
						LDC.class,
						IMUL.class,
						new RejectedInstruction(BIPUSH.class)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1_1);
				patterns1.add(pattern1_2);
				
				List<CompareInstruction[]> patterns2 = new ArrayList<>();
				patterns2.add(pattern2_1);
				patterns2.add(pattern2_2);
				
				List<CompareInstruction[]> patterns3 = new ArrayList<>();
				patterns3.add(pattern3_1);
				patterns3.add(pattern3_2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns1);
				InstructionHandle[][] matches2 = null;
				InstructionHandle[][] matches3 = null;
				
				if (matches1 != null)
					matches2 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns2);
				
				if (matches2 != null)
					matches3 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns3);
				
				if (matches1 != null && matches2 != null && matches3 != null) {
					// TODO: add match combiner for search method
					InstructionHandle[] matches2All = Util.combine(matches2[0], matches2[1]);
					
					for (InstructionHandle ih : matches2All) {
						searcher.gotoHandle(ih);
						InstructionHandle xIh = searcher.getNext(false, GETFIELD.class);
						//InstructionHandle xIh = searcher.searchNext(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(BaseInfo.get.getInternalName()).adjustType(Type.INT));
						
						if (xIh != null) {
							GETFIELD instr = (GETFIELD) xIh.getInstruction();
							BaseInfo.get.x.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findX.add(cg.getClassName() + "." + mg.getName(), xIh.getPosition());
							
							searcher.gotoHandle(ih);
							BaseInfo.get.x.set(searcher.findNextGetMultiplier(false, false, BaseInfo.get.x, cpg));
						}
					}
					
					InstructionHandle[] matches3Combined = Util.combine(matches3[0], matches3[1]);
					
					for (InstructionHandle ih : matches3Combined) {
						searcher.gotoHandle(ih);
						InstructionHandle yIh = searcher.getNext(false, GETFIELD.class);
						//InstructionHandle yIh = searcher.searchNext(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(BaseInfo.get.getInternalName()).adjustType(Type.INT));
						
						if (yIh != null) {
							GETFIELD instr = (GETFIELD) yIh.getInstruction();
							BaseInfo.get.y.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findY.add(cg.getClassName() + "." + mg.getName(), yIh.getPosition());		
							
							searcher.gotoHandle(ih);
							BaseInfo.get.y.set(searcher.findNextGetMultiplier(false, false, BaseInfo.get.y, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			BaseInfo.get.setInternalName(cg.getClassName());

			findFields(cg);
		}
	}

}
