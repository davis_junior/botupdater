package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.SIPUSH;

import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import boot.BotUpdater;
import data.Client;

public class BuildNumberAnalyzer {
	
	/**
	 * Note: also sets version info.
	 * 
	 * @param cg
	 * @return <tt>true</tt> if found, <tt>false</tt> if not found.
	 */
	public static boolean findVersion(ClassGen cg) {
		return findVersion(false, cg) != null ? true : false;
	}
	
	/**
	 * Attempts to locate the RS full build/revision number.
	 * 
	 * @param returnResultOnly returns the result only and does not set version info.
	 * @param cg
	 * @return build number where result[0] is major number and result[1] is minor number; null on no result.
	 */
	public static int[] findVersion(boolean returnResultOnly, ClassGen cg) {
		int[] result = null;
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				//if (!mg.isStatic())
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						SIPUSH.class,
						ICONST.class,
						GETSTATIC.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle sipushIh = ih;
						InstructionHandle iconstIh = ih.getNext();
						
						if (iconstIh != null) {
							SIPUSH sipush = (SIPUSH) sipushIh.getInstruction();
							ICONST iconst = (ICONST) iconstIh.getInstruction();
							found = true;
							
							//findVersion_Count.add(cg.getClassName() + "." + mg.getName(), iconstIh.getPosition());	
							
							if (!returnResultOnly)
								Client.get.setVersionInfo(sipush.getValue().intValue(), iconst.getValue().intValue());
							
							result = new int[] { sipush.getValue().intValue(), iconst.getValue().intValue() };
						}
					}
				}
			}
		}
	
		//return found;
		return result;
	}
	
}
