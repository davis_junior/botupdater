package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IF_ACMPNE;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Cache;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.HashTable;
import data.Model;
import data.Node;
import data.NodeSub;
import data.NodeSubQueue;
import data.Npc;
import data.Player;

public class CacheAnalyzer extends Analyzer {
	
	public static CacheAnalyzer get = new CacheAnalyzer();

	CountInfoItem findTable = new CountInfoItem("findTable");
	
	public CacheAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findTable);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(java.lang.Object.class.getName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean foundHashTableField = false;
		boolean foundNodeSubQueueField = false;
		boolean foundNodeSubField = false;
		boolean foundAbstractInterfaceField = false;
		
		FieldData tempTable = FieldData.createTemp();
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				if (f.getType().getSignature().equals("L" + HashTable.get.getInternalName() + ";")) {
					tempTable.set(cg.getClassName(), f.getName(), f.getSignature());
					foundHashTableField = true;
				}
				
				if (f.getType().getSignature().equals("L" + NodeSubQueue.get.getInternalName() + ";")) {
					foundNodeSubQueueField = true;
				}
				
				if (f.getType().getSignature().equals("L" + NodeSub.get.getInternalName() + ";"))
					foundNodeSubField = true;
				
				if ((f.getType().getSignature().startsWith("L") || f.getType().getSignature().contains("[L"))
						&& !f.getType().getSignature().contains("Ljava/")
						&& !f.getType().getSignature().contains("Ljavax/")
						&& !f.getType().getSignature().contains("Lsun/")) {
					
					ClassGen cg2 = BotUpdater.getClass(f.getSignature().replace("L", "").replace("[", "").replace(";", "").replace("/", "."));
					
					if (cg2 == null)
						System.out.println("*ERROR* CacheAnalyzer: Unable to find class from field! Type: " + f.getType().getSignature());
					else {
						if (cg2.isAbstract() && cg2.isInterface())
							foundAbstractInterfaceField = true;
					}
				}
			}
		}
		
		if (foundHashTableField && foundNodeSubQueueField && !foundNodeSubField && foundAbstractInterfaceField) {
			Cache.get.table.setData(tempTable);
			
			found = true;
		}
	
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (HashTable.get.getInternalName() == null) {
			System.out.println("ERROR: CacheAnalyzer: HashTable internal class name is null!");
			return;
		}
		
		if (NodeSub.get.getInternalName() == null) {
			System.out.println("ERROR: CacheAnalyzer: NodeSub internal class name is null!");
			return;
		}
		
		if (NodeSubQueue.get.getInternalName() == null) {
			System.out.println("ERROR: CacheAnalyzer: NodeSubQueue internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			Cache.get.setInternalName(cg.getClassName());
			
			//findFields(cg);
		}
	}

}
