package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_LDC;
import data.Camera;

public class CameraAnalyzer extends Analyzer {
	
	public static CameraAnalyzer get = new CameraAnalyzer();

	//CountInfoItem findCameraLocationData = new CountInfoItem("findCameraLocationData");
	//CountInfoItem findCentralLocationData = new CountInfoItem("findCentralLocationData");
	
	public CameraAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findCameraLocationData, findCentralLocationData);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(8),
					IAND.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(16),
					IAND.class
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(7),
					ISHR.class,
					new ICONST(1),
					IAND.class
			);
			
			final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
					new ICONST(5),
					ISHR.class,
					new ICONST(1),
					IAND.class
			);
			
			List<CompareInstruction[]> patterns = new ArrayList<>();
			patterns.add(pattern1);
			patterns.add(pattern2);
			patterns.add(pattern3);
			patterns.add(pattern4);
			
			InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
			
			if (matches != null && matches.length == patterns.size()) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Camera.get.setInternalName(cg.getClassName());
		}
	}

}
