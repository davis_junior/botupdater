package analyzers;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.CameraLocationData;
import data.Player;

public class CameraLocationDataAnalyzer extends Analyzer {
	
	public static CameraLocationDataAnalyzer get = new CameraLocationDataAnalyzer();

	CountInfoItem findPoint1 = new CountInfoItem("findPoint1");
	CountInfoItem findPoint2 = new CountInfoItem("findPoint2");
	CountInfoItem findQuaternion = new CountInfoItem("findQuaternion");
	
	public CameraLocationDataAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findPoint1, findPoint2, findQuaternion);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			if (mg.getArgumentTypes().length > 0 && mg.getArgumentTypes()[0].equals(Type.getType(int[][][].class))) {
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new SynonymInstruction(new PI_LDC_W(cpg).adjustType(Type.FLOAT).adjustValue(3.1415927F), new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(3.1415927F))
						);
	
				if (matches != null) {
					//for (InstructionHandle ih : matches) {
						found = true;
					//}
				}
			}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(CameraLocationData.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.getArgumentTypes().length != 0)
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						GETFIELD.class
						));

				if (matches != null) {
					if (matches.length == 3) {
						//for (InstructionHandle ih : matches) {
						for (int matchi = 0; matchi < matches.length; matchi++) {
							InstructionHandle ih = matches[matchi];
							searcher.gotoHandle(ih);
	
							GETFIELD instr2 = (GETFIELD) ih.getNext().getInstruction();
							
							switch (matchi) {
								case 0:
								CameraLocationData.get.point1.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
								findPoint1.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
								break;
								
								case 1:
								CameraLocationData.get.quaternion.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
								findQuaternion.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
								break;
								
								case 2:
								CameraLocationData.get.point2.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
								findPoint2.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
								break;
							}
							
							found = true;
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			CameraLocationData.get.setInternalName(cg.getClassName());

			findFields(cg);
		}
	}

}
