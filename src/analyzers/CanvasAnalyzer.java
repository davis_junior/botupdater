package analyzers;

import java.io.ObjectOutputStream.PutField;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2D;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FLOAD;
import org.apache.bcel.generic.FMUL;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import searcher.Comparator.CompareType;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Canvas;
import data.Player;
import data.Quaternion;

public class CanvasAnalyzer extends Analyzer {
	
	public static CanvasAnalyzer get = new CanvasAnalyzer();

	CountInfoItem findComponent = new CountInfoItem("findComponent");
	
	public CanvasAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findComponent);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;

		if (cg.getSuperclassName().equals("java.awt.Canvas"))
			found = true;
		
		/*for (Method m : cg.getMethods()) {
			MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						FLOAD.class,
						new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(0.5F),
						FMUL.class,
						F2D.class,
						new SynonymInstruction(new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("cos"), new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("sin"))
						);
	
				if (matches != null) {
					//for (InstructionHandle ih : matches) {
						found = true;
					//}
				}
			//}
		}*/
	
		return found;
	}
	
	private boolean findComponent(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Canvas.get.getInternalName())) {
			for (Field field : cg.getFields()) {
				if (field.getType().equals(Type.getType(java.awt.Component.class))) {
					Canvas.get.component.set(cg.getClassName(), field.getName(), field.getSignature());
					
					found = true;
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Canvas.get.setInternalName(cg.getClassName());

			findComponent(cg);
		}
	}

}
