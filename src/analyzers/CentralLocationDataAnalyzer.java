package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ASTORE;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DCONST;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FMUL;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractCentralLocationData;
import data.CameraLocationData;
import data.CentralLocationData;
import data.FieldData;
import data.Location;
import data.Model;
import data.Player;

public class CentralLocationDataAnalyzer extends Analyzer {
	
	public static CentralLocationDataAnalyzer get = new CentralLocationDataAnalyzer();

	CountInfoItem findPoint1 = new CountInfoItem("findPoint1");
	CountInfoItem findPoint2 = new CountInfoItem("findPoint2");
	
	public CentralLocationDataAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findPoint1, findPoint2);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			// the following patterns are also in another class
			//if (mg.getArgumentTypes().length == 2) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(-1.0F),
					FMUL.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new FCONST(0),
					new FCONST(1),
					new FCONST(0)
			);
			
			List<CompareInstruction[]> patterns = new ArrayList<>();
			patterns.add(pattern1);
			patterns.add(pattern2);
			
			InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
			
			if (matches != null && matches.length == patterns.size()) {
				//for (InstructionHandle ih : matches) {
				if (matches[0].length == 2) {
					int locationFieldCount = 0;
					for (Field field : cg.getFields()) {
						if (field.isFinal() && !field.isStatic()
								&& field.getType().toString().equals(Location.get.getInternalName()))
							locationFieldCount++;
					}
					
					if (locationFieldCount == 4)
						found = true;
				}
				//}
			}
			//}
		}
		
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(CentralLocationData.get.getInternalName())) {
			List<FieldData> matches2_fields = new ArrayList<FieldData>();
			
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches2 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						GETFIELD.class,
						GETFIELD.class,
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Float").adjustMethodName("isNaN")
						));
				
				if (matches2 != null) {
					for (int matchi = 0; matchi < matches2.length; matchi++) {
						InstructionHandle ih = matches2[matchi];
						searcher.gotoHandle(ih);
	
						GETFIELD instr2 = (GETFIELD) ih.getNext().getInstruction();
	
						FieldData fieldTemp = FieldData.createTemp();
						fieldTemp.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
						matches2_fields.add(fieldTemp);
					}
				}
			}
			
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.getArgumentTypes().length != 0)
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches1 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						GETFIELD.class,
						INVOKESTATIC.class,
						ASTORE.class
						));

				if (matches1 != null && matches2_fields.size() > 0) {
					if (matches1.length == 2) {
						FieldData point1Temp = FieldData.createTemp("point1");
						FieldData point2Temp = FieldData.createTemp("point2");
						
						//for (InstructionHandle ih : matches) {
						for (int matchi = 0; matchi < matches1.length; matchi++) {
							InstructionHandle ih = matches1[matchi];
							searcher.gotoHandle(ih);
	
							GETFIELD instr2 = (GETFIELD) ih.getNext().getInstruction();

							switch (matchi) {
								case 0:
								point1Temp.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
								break;
								
								case 1:
								point2Temp.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
								break;
							}
						}
						
						for (FieldData fieldTemp : matches2_fields) {
							if (fieldTemp.compareData(point1Temp) || fieldTemp.compareData(point2Temp)) {
								CentralLocationData.get.point1.setData(point1Temp);
								findPoint1.add(cg.getClassName() + "." + mg.getName(), -1);
								
								CentralLocationData.get.point2.setData(point2Temp);
								findPoint2.add(cg.getClassName() + "." + mg.getName(), -1);
								
								found = true;
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Location.get.getInternalName() == null) {
			System.out.println("ERROR: CentralLocationDataAnalyzer: Location internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			CentralLocationData.get.setInternalName(cg.getClassName());

			findFields(cg);
		}
	}

}
