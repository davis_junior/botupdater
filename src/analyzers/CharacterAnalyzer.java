package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.InstructionClass;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractCentralLocationData;
import data.Animator;
import data.BaseInfo;
import data.CentralLocationData;
import data.Character;
import data.FieldData;
import data.GameInfo;
import data.MessageData;
import data.Model;
import data.Player;

public class CharacterAnalyzer extends Analyzer {
	
	public static CharacterAnalyzer get = new CharacterAnalyzer();

	CountInfoItem findOrientation = new CountInfoItem("findOrientation");
	CountInfoItem findModels = new CountInfoItem("findModels");
	CountInfoItem findMessageData = new CountInfoItem("findMessageData");
	CountInfoItem findMovementSpeed = new CountInfoItem("findMovementSpeed");
	CountInfoItem findInteractingIndex = new CountInfoItem("findInteractingIndex");
	
	public CharacterAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findOrientation, findModels, findMessageData, findMovementSpeed, findInteractingIndex);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						F2I.class,
						ILOAD.class,
						ISUB.class,
						new PI_BIPUSH(cpg).adjustValue(9),
						ISHR.class,
						I2S.class,
						PUTFIELD.class
						));
	
				if (matches != null) {
					//for (InstructionHandle ih : matches) {
						found = true;
					//}
				}
			//}
		}
	
		return found;
	}
	
	private boolean findOrientation(ClassGen cg) {
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Character.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] exclusionPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math")
				);
				
				List<CompareInstruction[]> exclusionPatterns = new ArrayList<>();
				exclusionPatterns.add(exclusionPattern1);
				
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(16383),
						IAND.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] exclusionMatches = searcher.searchAllFromStart(false, false, CompareType.Opcode, exclusionPatterns);
				if (exclusionMatches != null)
					continue;
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						FieldData tempPut = FieldData.createTemp();
						FieldData tempGet = FieldData.createTemp();
						
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.searchNext(true, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustType(Type.INT));
						
						if (fieldIh != null) {
							PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
							tempPut.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));										
						
							fieldIh = searcher.searchNext(true, CompareType.Opcode, new PI_GETFIELD(cpg).adjustType(Type.INT));
						}
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							tempGet.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));										
						}
						
						if (tempPut.compareData(tempGet)) {
							Character.get.orientation.setData(tempGet);					
							found = true;
							
							findOrientation.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
							
							searcher.gotoHandle(ih);
							Character.get.orientation.set(searcher.findNextGetMultiplier(false, false, Character.get.orientation, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	
	public boolean findModels(ClassGen cg) {
		boolean found = false;
		
		for (Field field : cg.getFields()) {
			if (!field.isStatic() && field.getType().toString().equals(Model.get.getInternalName() + "[]")) {
				Character.get.models.set(cg.getClassName(), field.getName(), field.getSignature());
				
				found = true;
			}
		}
		
		return found;
	}
	
	public boolean findAnimator(ClassGen cg) {
		boolean found = false;
		
		for (Field field : cg.getFields()) {
			if (!field.isStatic() && field.getType().toString().equals(Animator.get.getInternalName())) {
				Character.get.animator.set(cg.getClassName(), field.getName(), field.getSignature());
				
				found = true;
			}
		}
		
		return found;
	}
	
	public boolean findMessageData(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Character.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				//if (mg.getArgumentTypes().length == 0) {
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustClassName(Character.get.getInternalName()),
						ALOAD.class,
						new PI_PUTFIELD(cpg).adjustType(Type.STRING)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle getFieldIh = ih.getNext();
						InstructionHandle putFieldIh = ih.getNext().getNext().getNext();
						
						GETFIELD getField = (GETFIELD) getFieldIh.getInstruction();
						PUTFIELD putField = (PUTFIELD) putFieldIh.getInstruction();
						
						if (getField.getType(cpg).toString().equals(putField.getClassName(cpg))) {
							GETFIELD instr = getField;
							Character.get.messageData.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findMessageData.add(cg.getClassName() + "." + mg.getName(), getFieldIh.getPosition());
							
							MessageData.get.setInternalName(putField.getClassName(cpg));
						}
					}
				}
				//}
			}
		}
	
		return found;
	}
	
	public boolean findMovementSpeed(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Character.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				//if (mg.getArgumentTypes().length == 0) {
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(512)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle prevStore = searcher.searchPrev(false, CompareType.Opcode, SynonymInstruction.Store_OR_Put);
						InstructionHandle nextStore = searcher.searchNext(false, CompareType.Opcode, SynonymInstruction.Store_OR_Put);
						
						if (prevStore != null && nextStore != null) {
							searcher.gotoHandle(prevStore);
							InstructionHandle sipush256 = searcher.search(false, true, prevStore, nextStore, CompareType.Opcode, new PI_SIPUSH(cpg).adjustValue(256));
							
							if (sipush256 != null) {
								InstructionHandle fieldIh = searcher.search(false, true, prevStore, nextStore, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(Character.get.getInternalName()).adjustType(Type.INT));
								
								if (fieldIh != null) {
									GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
									Character.get.movementSpeed.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									found = true;
									
									findMovementSpeed.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
									
									searcher.gotoHandle(prevStore);
									Character.get.movementSpeed.set(searcher.findNextGetMultiplier(false, false, Character.get.movementSpeed, cpg));
								}
							}
						}
					}
				}
				//}
			}
		//}
	
		return found;
	}
	
	public boolean findInteracting(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Character.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				//if (mg.getArgumentTypes().length == 0) {
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("atan2")
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC(cpg).adjustValue(32768)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					//if (matches1[1].length != 3)
					//	continue;
					
					for (InstructionHandle ih : matches1[1]) {
						searcher.gotoHandle(ih);
						InstructionHandle prevIfIh = searcher.searchPrev(false, CompareType.Opcode, new InstructionClass(IfInstruction.class));
						InstructionHandle nextIfIh = searcher.searchNext(false, CompareType.Opcode, new InstructionClass(IfInstruction.class));
						
						if (prevIfIh != null && nextIfIh != null) {
							InstructionHandle fieldIh = searcher.search(false, true, prevIfIh, nextIfIh, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(Character.get.getInternalName()).adjustType(Type.INT));
							
							if (fieldIh != null) {
								GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
								Character.get.interactingIndex.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								found = true;
								
								findInteractingIndex.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
								
								searcher.gotoHandle(prevIfIh);
								Character.get.interactingIndex.set(searcher.findNextGetMultiplier(false, false, Character.get.interactingIndex, cpg));
							}
						}
					}
				}
				//}
			}
		//}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Character.get.setInternalName(cg.getClassName());

			findOrientation(cg);
		}
	}
	
	public void analyze_pass2(ClassGen cg) {
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: CharacterAnalyzer: Model internal class name is null!");
			//return;
		} else {
			if (verifyClass(cg)) {
				findModels(cg);
			}
		}
		
		if (Animator.get.getInternalName() == null) {
			System.out.println("ERROR: CharacterAnalyzer: Animator internal class name is null!");
			//return;
		} else {
			if (verifyClass(cg)) {
				findAnimator(cg);
			}
		}
		
		if (Character.get.getInternalName() == null) {
			System.out.println("ERROR: CharacterAnalyzer: Character internal class name is null!");
			//return;
		} else {
			findMovementSpeed(cg);
			findInteracting(cg);
		}
	}
	
	public void analyze_pass3(ClassGen cg) {
		if (Character.get.getInternalName() == null) {
			System.out.println("ERROR: CharacterAnalyzer: Character internal class name is null!");
			//return;
		} else if (MessageData.get.getInternalName() == null) {
			System.out.println("ERROR: CharacterAnalyzer: MessageData internal class name is null!");
			//return;
		} else {
			if (cg.getClassName().equals(Character.get.getInternalName()))
				findMessageData(cg);
		}
	}
}
