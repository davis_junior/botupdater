package analyzers;

import java.awt.Canvas;
import java.awt.Color;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.AALOAD;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ASTORE;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.CHECKCAST;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.ConstantPushInstruction;
import org.apache.bcel.generic.D2I;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.DUP;
import org.apache.bcel.generic.F2D;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FDIV;
import org.apache.bcel.generic.FieldInstruction;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IDIV;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IFNULL;
import org.apache.bcel.generic.IF_ACMPEQ;
import org.apache.bcel.generic.IF_ACMPNE;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.INVOKESPECIAL;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.IRETURN;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISTORE;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC_W;
import org.apache.bcel.generic.LLOAD;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.LXOR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.NEW;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.PUTSTATIC;
import org.apache.bcel.generic.PushInstruction;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.StoreInstruction;
import org.apache.bcel.generic.Type;
import org.apache.commons.lang3.builder.CompareToBuilder;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import report.Report;
import searcher.Comparator;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.InstructionClass;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_CHECKCAST;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_GETSTATIC;
import searcher.pi.PI_INVOKEINTERFACE;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_NEW;
import searcher.pi.PI_PUTFIELD;
import searcher.pi.PI_PUTSTATIC;
import searcher.pi.PI_SIPUSH;
import searcher.Searcher;
import util.Util;
import data.BaseInfo;
import data.Cache;
import data.Camera;
import data.Client;
import data.Component;
import data.FieldData;
import data.GEOffer;
import data.GameInfo;
import data.Graphics;
import data.GraphicsSettingsContainer;
import data.GroundBytes;
import data.HashTable;
import data.ItemDefLoader;
import data.ItemNode;
import data.ItemTable;
import data.Location;
import data.MenuGroupNode;
import data.MenuItemNode;
import data.Model;
import data.Node;
import data.NodeDeque;
import data.NodeSubQueue;
import data.Npc;
import data.NpcNode;
import data.Player;
import data.PlayerDef;
import data.SettingData;
import data.Widget;
import data.WorldInfo;

public class ClientAnalyzer extends Analyzer {
	
	public static ClientAnalyzer get = new ClientAnalyzer();

	CountInfoItem findCanvas_Count = new CountInfoItem("findCanvas");
	CountInfoItem findCanvasWidth_Count = new CountInfoItem("findCanvasWidth");
	CountInfoItem findCanvasHeight_Count = new CountInfoItem("findCanvasHeight");
	CountInfoItem findCamera_Count = new CountInfoItem("findCamera");
	CountInfoItem findCameraPitch_Count = new CountInfoItem("findCameraPitch");
	CountInfoItem findCameraYaw_Count = new CountInfoItem("findCameraYaw");
	CountInfoItem findPlayers_Count = new CountInfoItem("findPlayers");
	CountInfoItem findMyPlayer_Count = new CountInfoItem("findMyPlayer");
	CountInfoItem findPlayerModelCache = new CountInfoItem("findPlayerModelCache");
	CountInfoItem findNpcNodeCacheAndIndexArray_Count = new CountInfoItem("findNpcNodeCacheAndIndexArray");
	CountInfoItem findPlane_Count = new CountInfoItem("findPlane");
	CountInfoItem findGameInfo_Count = new CountInfoItem("findGameInfo");
	CountInfoItem findRender_Count = new CountInfoItem("findRender");
	CountInfoItem findComponentBoundsArray_Count = new CountInfoItem("findComponentBoundsArray");
	CountInfoItem findWidgetCache_Count = new CountInfoItem("findWidgetCache");
	CountInfoItem findValidWidgetArray_Count = new CountInfoItem("findValidWidgetArray");
	CountInfoItem findMenuPositionFields_Count = new CountInfoItem("findMenuPositionFields");
	CountInfoItem findMenuFields_Count = new CountInfoItem("findMenuFields");
	CountInfoItem findIsUsableComponentSelected_Count = new CountInfoItem("findIsUsableComponentSelected");
	CountInfoItem findLastSelectedUsableComponentId_Count = new CountInfoItem("findLastSelectedUsableComponentId");
	CountInfoItem findLastSelectedUsableComponentAction_Count = new CountInfoItem("findLastSelectedUsableComponentAction");
	CountInfoItem findLastSelectedUsableComponentOption_Count = new CountInfoItem("findLastSelectedUsableComponentOption");
	CountInfoItem findMinimapAngle_Count = new CountInfoItem("findMinimapAngle");
	CountInfoItem findMinimapScale_Count = new CountInfoItem("findMinimapScale");
	CountInfoItem findMinimapOffset_Count = new CountInfoItem("findMinimapOffset");
	CountInfoItem findMinimapSetting_Count = new CountInfoItem("findMinimapSetting");
	CountInfoItem findDestX_Count = new CountInfoItem("findDestX");
	CountInfoItem findDestY_Count = new CountInfoItem("findDestY");
	CountInfoItem findSettingData_Count = new CountInfoItem("findSettingData");
	CountInfoItem findItemTables_Count = new CountInfoItem("findItemTables");
	CountInfoItem findEventQueue_Count = new CountInfoItem("findEventQueue");
	CountInfoItem findItemNodeCache_Count = new CountInfoItem("findItemNodeCache");
	CountInfoItem findItemDefLoader_Count = new CountInfoItem("findItemDefLoader");
	CountInfoItem findGeOffers_Count = new CountInfoItem("findGeOffers");
	CountInfoItem findWorldInfo_Count = new CountInfoItem("findGeOffers");
	CountInfoItem findGraphicsSettingsContainer_Count = new CountInfoItem("findGraphicsSettingsContainer");
	
	public ClientAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findCanvas_Count, findCamera_Count, findCameraPitch_Count, findCameraYaw_Count, findMyPlayer_Count, findPlayerModelCache, findNpcNodeCacheAndIndexArray_Count, findPlane_Count, findGameInfo_Count, findRender_Count, findComponentBoundsArray_Count, findWidgetCache_Count, findValidWidgetArray_Count, findMenuPositionFields_Count, findMenuFields_Count, findLastSelectedUsableComponentAction_Count, findMinimapAngle_Count, findMinimapScale_Count, findMinimapOffset_Count, findMinimapSetting_Count, findDestX_Count, findDestY_Count, findSettingData_Count, findItemTables_Count, findEventQueue_Count, findItemNodeCache_Count, findItemDefLoader_Count, findGeOffers_Count, findWorldInfo_Count, findGraphicsSettingsContainer_Count);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {	
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals("client")) {
		    found = true;
		}
		
		return found;
	}
	
	
	// from build 790: method has 1 arg: (int)
	private boolean findCanvas(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new PI_GETSTATIC(cpg).adjustType(Type.getType(Canvas.class)),
						new PI_GETSTATIC(cpg).adjustType(Type.getType(Color.class)),
						new PI_INVOKEVIRTUAL(cpg).adjustMethodName("setBackground")
						);

				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						GETSTATIC instr1 = (GETSTATIC) ih.getInstruction();
						Client.get.canvas.set(instr1.getReferenceType(cpg).toString(), instr1.getFieldName(cpg), instr1.getSignature(cpg));
						found = true;
						
						findCanvas_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findCanvasSizeFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustType(Type.getType(Canvas.class))
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEVIRTUAL(cpg).adjustClassName(Canvas.class.getName()).adjustMethodName("setSize")
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(1024),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(768),
						IfInstruction.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				patterns1.add(pattern4);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					if (matches1[2].length == 1 & matches1[3].length == 1) {
						
						// width
						for (InstructionHandle ih : matches1[2]) {
							searcher.gotoHandle(ih);
							InstructionHandle ifIh = searcher.searchNext(true, CompareType.Opcode, new InstructionClass(IfInstruction.class));
							
							if (ifIh != null) {
								InstructionHandle fieldIh = searcher.searchPrev(true, false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.INT));
								
								if (fieldIh != null) {
									GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
									Client.get.canvasWidth.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									found = true;
									
									findCanvasWidth_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
									
									searcher.gotoHandle(ifIh);
									Client.get.canvasWidth.set(searcher.findPrevGetMultiplier(false, true, Client.get.canvasWidth, cpg));
								}
							}
						}
						
						// height
						for (InstructionHandle ih : matches1[3]) {
							searcher.gotoHandle(ih);
							InstructionHandle ifIh = searcher.searchNext(true, CompareType.Opcode, new InstructionClass(IfInstruction.class));
							
							if (ifIh != null) {
								InstructionHandle fieldIh = searcher.searchPrev(true, false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.INT));
								
								if (fieldIh != null) {
									GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
									Client.get.canvasHeight.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									found = true;
									
									findCanvasHeight_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
									
									searcher.gotoHandle(ifIh);
									Client.get.canvasHeight.set(searcher.findPrevGetMultiplier(false, true, Client.get.canvasHeight, cpg));
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	
	private boolean findCameraPitch(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new FCONST(2),
						FDIV.class,
						F2D.class,
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("tan")
						));

				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(true, PUTSTATIC.class);
						
						if (fieldIh != null) {
							PUTSTATIC instr = (PUTSTATIC) fieldIh.getInstruction();
							Client.get.cameraPitch.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findCameraPitch_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
							
							searcher.gotoHandle(ih);
							
							Number multiplier = searcher.findNextPutMultiplier(false, true, Client.get.cameraPitch, cpg);
							
							if (multiplier == null) {
								InstructionHandle multiIh = searcher.searchPrev(false, CompareType.Opcode, new SynonymInstruction(new PI_LDC(cpg).adjustType(Type.INT), new PI_LDC(cpg).adjustType(Type.LONG), new PI_LDC_W(cpg).adjustType(Type.INT), new PI_LDC_W(cpg).adjustType(Type.LONG)));
								if (multiIh != null) {
									multiplier = (Number) ((LDC) multiIh.getInstruction()).getValue(cpg);
									if (multiplier != null)
										multiplier = MultiplierFinder.findInverse(multiplier.intValue());
								}
							}
							
							Client.get.cameraPitch.set(multiplier);
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findCameraYaw(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new PI_LDC2_W(cpg).adjustValue(-2607.5945876176133D)
						);
				
				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						//InstructionHandle fieldIh = searcher.getNext(true, PUTSTATIC.class);
						InstructionHandle fieldIh = searcher.searchNext(true, CompareType.Opcode, 
								new SynonymInstruction(GETSTATIC.class, PUTSTATIC.class)
								);
						
						if (fieldIh != null && fieldIh.getInstruction() instanceof PUTSTATIC) {
							FieldInstruction instr = (FieldInstruction) fieldIh.getInstruction();
							Client.get.cameraYaw.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findCameraYaw_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());		
							
							searcher.gotoHandle(ih);
							Number multiplier = searcher.findNextPutMultiplier(false, true, Client.get.cameraYaw, cpg);
							
							if (multiplier == null) {
								InstructionHandle multiIh = searcher.searchPrev(false, CompareType.Opcode, new SynonymInstruction(new PI_LDC(cpg).adjustType(Type.INT), new PI_LDC(cpg).adjustType(Type.LONG), new PI_LDC_W(cpg).adjustType(Type.INT), new PI_LDC_W(cpg).adjustType(Type.LONG)));
								if (multiIh != null) {
									multiplier = (Number) ((LDC) multiIh.getInstruction()).getValue(cpg);
									if (multiplier != null)
										multiplier = MultiplierFinder.findInverse(multiplier.intValue());
								}
							}
							
							Client.get.cameraYaw.set(multiplier);
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findCamera(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math")
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC2_W(cpg).adjustValue(2607.5945876176133)
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, data.Camera.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				List<CompareInstruction[]> patterns2 = new ArrayList<>();
				patterns2.add(pattern2);
				patterns2.add(pattern3);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 == null) {
					InstructionHandle[][] matches2 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns2);
					
					if (matches2 != null && matches2.length == patterns2.size()) {
						for (InstructionHandle ih : matches2[0]) {
							searcher.gotoHandle(ih);
							InstructionHandle fieldIh = searcher.searchPrev(true, true, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustTypeString(false, data.Camera.get.getInternalName()));
							
							if (fieldIh != null) {
								GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
								Client.get.camera.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								found = true;
								
								findCamera_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());						
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findPlayers(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new PI_GETSTATIC(cpg).adjustTypeString(false, Player.get.getInternalName() + "[]")
						);

				if (matches != null) {
					for (InstructionHandle ih : matches) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.players.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findPlayers_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());						
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findMyPlayer(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						//new PI_SIPUSH(cpg).adjustValue(732),
						//INVOKEVIRTUAL.class,
						new ICONST(1),
						ISUB.class,
						new PI_SIPUSH(cpg).adjustValue(256),
						IMUL.class,
						 ISTORE.class
						));

				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(true, GETSTATIC.class);
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.myPlayer.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
							findMyPlayer_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());						
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	// field should be in PlayerDef class; pattern is the same as PlayerDef verifyClass method
	private boolean findPlayerModelCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(PlayerDef.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				//if (mg.getArgumentTypes().length == 0) {
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(0x40000000), new PI_LDC_W(cpg).adjustValue(0x40000000)),
						IOR.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC2_W(cpg).adjustValue(0xFFFFFFFFL),
						LXOR.class
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(0x80000000), new PI_LDC_W(cpg).adjustValue(0x80000000)),
						IAND.class
				);
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(0x3FFFFFFF), new PI_LDC_W(cpg).adjustValue(0x3FFFFFFF)),
						IAND.class
				);
				
				final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, Cache.get.getInternalName()),
						LLOAD.class,
						INVOKEVIRTUAL.class,
						new PI_CHECKCAST(cpg).adjustTypeString(false, Model.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				patterns.add(pattern3);
				patterns.add(pattern4);
				patterns.add(pattern5);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[4]) {
						GETSTATIC instr = (GETSTATIC) ih.getInstruction();
						Client.get.playerModelCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
						found = true;
						
						findPlayerModelCache.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
					}
				}
				//}
			}
		//}
	
		return found;
	}
	
	// TODO: this pattern may be off; go9090go's pattern from 794 doesn't match
	// 		EDIT: the pattern was correct for the last revision (796)!
	private boolean findPlane(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			InstructionHandle[] matches1 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
					new PI_GETFIELD(cpg).adjustClassName(GroundBytes.get.getInternalName()).adjustFieldName(GroundBytes.get.bytes.getInternalName()),
					new PI_GETSTATIC(cpg).adjustType(Type.INT),
					new SynonymInstruction(LDC.class, LDC_W.class),
					IMUL.class
					));
			
			InstructionHandle[] matches2 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
					new PI_GETFIELD(cpg).adjustClassName(GroundBytes.get.getInternalName()).adjustFieldName(GroundBytes.get.bytes.getInternalName()),
					new SynonymInstruction(LDC.class, LDC_W.class),
					new PI_GETSTATIC(cpg).adjustType(Type.INT),
					IMUL.class
					));

			InstructionHandle[] matchesCombined = Util.combine(matches1, matches2);
			
			if (matchesCombined != null) {
				for (InstructionHandle ih : matchesCombined) {
					searcher.gotoHandle(ih);
					InstructionHandle fieldIh = searcher.searchFrom(true, true, ih, CompareType.Opcode, new InstructionClass(GETSTATIC.class));
					
					if (fieldIh != null) {
						GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
						Client.get.plane.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
						found = true;
						
						//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
						findPlane_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
						
						searcher.gotoHandle(ih);
						Client.get.plane.set(searcher.findNextGetMultiplier(false, true, Client.get.plane, cpg));
					}
				}
			}
		}
		//}
	
		return found;
	}
	
	private boolean findGameInfo(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1_1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName("java.awt.Dimension").adjustFieldName("width"),
						new ICONST(2),
						IDIV.class,
						I2F.class
				);
				
				final CompareInstruction[] pattern1_2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName("java.awt.Dimension").adjustFieldName("height"),
						new ICONST(2),
						IDIV.class,
						I2F.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(512.0F), new PI_LDC_W(cpg).adjustType(Type.FLOAT).adjustValue(512.0F)),
						new SynonymInstruction(new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(512.0F), new PI_LDC_W(cpg).adjustType(Type.FLOAT).adjustValue(512.0F)),
						GETSTATIC.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1_1);
				patterns1.add(pattern1_2);
				
				List<CompareInstruction[]> patterns2 = new ArrayList<>();
				patterns2.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				/*InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new PI_SIPUSH(cpg).adjustValue(512),
						IMUL.class,
						new ICONST(2),
						IDIV.class,
						I2F.class
						);*/
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				InstructionHandle[][] matches2 = null;
				
				if (matches1 != null && matches1.length == patterns1.size())
					matches2 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns2);
				
				if (matches2 != null && matches2.length == patterns2.size()) {
					for (InstructionHandle ih : matches2[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(true, GETSTATIC.class);
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.gameInfo.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
							findGameInfo_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findRender(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				/*final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						F2D.class,
						new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(2607.5945876176133D),
						DMUL.class,
						D2I.class,
						//new PI_SIPUSH(cpg).adjustValue(16383),
						//IAND.class,
						//ISTORE.class
				);*/
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(2048),
						new PI_BIPUSH(cpg).adjustValue(64),
						new PI_BIPUSH(cpg).adjustValue(64),
						new PI_SIPUSH(cpg).adjustValue(768)
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					//if (matches[0].length == 2) {
						for (InstructionHandle ih : matches[0]) {
							//searcher.gotoHandle(matches[0][matches[0].length - 1]);
							searcher.gotoHandle(ih);
							InstructionHandle fieldIh = searcher.getPrev(true, GETSTATIC.class);
							
							if (fieldIh != null) {
								GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
								Client.get.render.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								found = true;
								
								findRender_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());						
							}
						}
					//}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findApplets(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				//if (mg.getArgumentTypes().length == 0)
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new PI_PUTSTATIC(cpg).adjustTypeString(false, "java.applet.Applet")
						);
				
				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							PUTSTATIC instr = (PUTSTATIC) fieldIh.getInstruction();
							
							FieldData temp = FieldData.createTemp();
							temp.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							
							if (Client.get.applet1.getInternalName() == null)
								Client.get.applet1.setData(temp);
							else if (Client.get.applet1.getInternalName() != null && !Client.get.applet1.compareData(temp) && Client.get.applet2.getInternalName() == null)
								Client.get.applet2.setData(temp);
							else if (Client.get.applet2.getInternalName() != null && !Client.get.applet2.compareData(temp) && Client.get.applet3.getInternalName() == null)
								Client.get.applet3.setData(temp);
							
							found = true;
							
							//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
							//findGameInfo_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findGraphics(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new PI_PUTSTATIC(cpg).adjustTypeString(false, Graphics.get.getInternalName())
						);
				
				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							PUTSTATIC instr = (PUTSTATIC) fieldIh.getInstruction();
							
							Client.get.graphics.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							
							found = true;
							
							//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
							//findGameInfo_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findNpcNodeArray(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, NpcNode.get.getInternalName() + "[]"),
						ILOAD.class,
						AALOAD.class,
						new PI_GETFIELD(cpg).adjustType(Type.getType(java.lang.Object.class)).adjustClassName(NpcNode.get.getInternalName()).adjustFieldName(NpcNode.get.npc.getInternalName()),
						new PI_CHECKCAST(cpg).adjustTypeString(false, Npc.get.getInternalName())
						));
				
				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							
							Client.get.npcNodeArray.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							
							found = true;
							
							//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
							//findGameInfo_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findNpcNodeCacheAndIndexArray(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(PlayerDef.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] rejectedPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEINTERFACE(cpg).adjustClassName("java.util.Iterator")
				);
				
				final CompareInstruction[] rejectedPattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(IOR.class, IAND.class)
				);
				
				List<CompareInstruction[]> rejectedPatterns = new ArrayList<>();
				rejectedPatterns.add(rejectedPattern1);
				rejectedPatterns.add(rejectedPattern2);
				
				InstructionHandle[][] rejectedMatches = searcher.searchAllFromStart(false, false, CompareType.Opcode, rejectedPatterns);
				
				if (rejectedMatches != null)
					continue;
				
				
				//if (mg.getArgumentTypes().length == 0) {
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEVIRTUAL(cpg).adjustClassName(HashTable.get.getInternalName()).adjustTypeString(false, Node.get.getInternalName()),
						new PI_CHECKCAST(cpg).adjustTypeString(false, NpcNode.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						
						InstructionHandle prevIfIh = searcher.getPrev(false, IfInstruction.class);
						
						if (prevIfIh != null) {
							InstructionHandle[] getstaticIhs = searcher.searchAll(false, false, prevIfIh, ih, CompareType.Opcode, new InstructionClass(GETSTATIC.class));
							
							if (getstaticIhs != null && getstaticIhs.length == 2) {
								InstructionHandle prevGetstaticIh = getstaticIhs[0];
								InstructionHandle prev2GetstaticIh = getstaticIhs[1];
								
								GETSTATIC prevGetstatic = (GETSTATIC) prevGetstaticIh.getInstruction();
								GETSTATIC prev2Getstatic = (GETSTATIC) prev2GetstaticIh.getInstruction();
								
								boolean correctTypes = false;
								boolean prev2isHashTable = false;
								
								if (prevGetstatic.getType(cpg).toString().equals("int[]")
										&& prev2Getstatic.getType(cpg).toString().equals(HashTable.get.getInternalName())) {
									
									correctTypes = true;
									prev2isHashTable = true;
								} else if (prev2Getstatic.getType(cpg).toString().equals("int[]")
										&& prevGetstatic.getType(cpg).toString().equals(HashTable.get.getInternalName())) {
									
									correctTypes = true;
									prev2isHashTable = false;
								}
								
								if (correctTypes) {
									GETSTATIC intArray = prev2isHashTable ? prevGetstatic : prev2Getstatic;
									GETSTATIC cache = prev2isHashTable ? prev2Getstatic : prevGetstatic;
									
									Client.get.npcNodeIndexArray.set(intArray.getReferenceType(cpg).toString(), intArray.getFieldName(cpg), intArray.getSignature(cpg));					
									Client.get.npcNodeCache.set(cache.getReferenceType(cpg).toString(), cache.getFieldName(cpg), cache.getSignature(cpg));
									found = true;
									
									findNpcNodeCacheAndIndexArray_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
								}
							}
						}
					}
				}
				//}
			}
		//}
	
		return found;
	}
	
	private boolean findLocationArray(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Location.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						new PI_GETSTATIC(cpg).adjustTypeString(false, Location.get.getInternalName() + "[]")
						);
				
				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							
							Client.get.locationArray.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							
							found = true;
							
							//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
							//findGameInfo_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findComponentBoundsArray(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, "java.awt.Rectangle[]")
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.awt.Rectangle").adjustMethodName("setBounds")
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						//searcher.gotoHandle(ih);
						//InstructionHandle fieldIh = searcher.getNext(true, GETSTATIC.class);
						
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.componentBoundsArray.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
							findComponentBoundsArray_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findComponentNodeCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, HashTable.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle nextGetFieldIh = searcher.getNext(false, GETFIELD.class);
						InstructionHandle prevGetFieldIh = searcher.getPrev(false, GETFIELD.class);
						
						if (nextGetFieldIh != null && prevGetFieldIh != null) {
							FieldInstruction nextGetField = (FieldInstruction) nextGetFieldIh.getInstruction();
							FieldInstruction prevGetField = (FieldInstruction) prevGetFieldIh.getInstruction();
							
							if (new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).compare(nextGetField)
									&& new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).compare(prevGetField)) {
								
								InstructionHandle fieldIh = ih;
								
								if (fieldIh != null) {
									GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
									Client.get.componentNodeCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									found = true;
									
									//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
									//findComponentNodeCache_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findWidgetCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, Widget.get.getInternalName() + "[]")
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle nextGetFieldIh = searcher.getNext(false, GETFIELD.class);
						InstructionHandle prevGetFieldIh = searcher.getPrev(false, GETFIELD.class);
						
						if (nextGetFieldIh != null && prevGetFieldIh != null) {
							FieldInstruction nextGetField = (FieldInstruction) nextGetFieldIh.getInstruction();
							FieldInstruction prevGetField = (FieldInstruction) prevGetFieldIh.getInstruction();
							
							if (new PI_GETFIELD(cpg).adjustClassName(Widget.get.components.getInternalClassName()).adjustFieldName(Widget.get.components.getInternalName()).adjustSignature(Widget.get.components.getSignature()).compare(nextGetField)
									&& new PI_GETFIELD(cpg).adjustClassName(Widget.get.components.getInternalClassName()).adjustFieldName(Widget.get.components.getInternalName()).adjustSignature(Widget.get.components.getSignature()).compare(prevGetField)) {
								
								InstructionHandle fieldIh = ih;
								
								if (fieldIh != null) {
									GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
									Client.get.widgetCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									found = true;
									
									//System.out.println(instr.getSignature(cpg) + cg.getClassName() + "." + mg.getName() + ih.getPosition());
									findWidgetCache_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findValidWidgetArray(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.getReturnType().equals(Type.BOOLEAN))
					continue;
				
				//if (mg.getArgumentTypes().length == 0)
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustClassName(Client.get.widgetCache.getInternalClassName()).adjustFieldName(Client.get.widgetCache.getInternalName()).adjustSignature(Client.get.widgetCache.getSignature())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						GETSTATIC.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				
				final CompareInstruction[] rejectedPattern1 = CompareInstruction.Methods.toCompareInstructions(
						PUTSTATIC.class
				);
				
				final CompareInstruction[] rejectedPattern2 = CompareInstruction.Methods.toCompareInstructions(
						GETFIELD.class
				);
				
				final CompareInstruction[] rejectedPattern3 = CompareInstruction.Methods.toCompareInstructions(
						PUTFIELD.class
				);
				
				List<CompareInstruction[]> rejectedPatterns1 = new ArrayList<>();
				rejectedPatterns1.add(rejectedPattern1);
				rejectedPatterns1.add(rejectedPattern2);
				rejectedPatterns1.add(rejectedPattern3);

				
				Searcher searcher = new Searcher(il);
				
				
				InstructionHandle[][] rejectedMatches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, rejectedPatterns1);
				
				if (rejectedMatches1 != null)
					continue;
				
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					//if (matches1[0].length == 3) {
					boolean rejectedGetStaticFound = false;
					for (InstructionHandle ih : matches1[1]) {
						if (!(pattern1[0].compare(ih.getInstruction())
								|| new PI_GETSTATIC(cpg).adjustSignature("[Z").compare(ih.getInstruction()))) {
							
							rejectedGetStaticFound = true;
							break;
						}
					}
					
					if (!rejectedGetStaticFound) {
						for (InstructionHandle ih : matches1[0]) {
							searcher.gotoHandle(ih);
							InstructionHandle fieldIh = searcher.getPrev(false, GETSTATIC.class);
							
							if (fieldIh != null) {
								GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
								
								if (instr.getSignature(cpg).equals("[Z")) {
									Client.get.validWidgetArray.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									found = true;
									
									//System.out.println(cg.getClassName() + "." + mg.getName() + ih.getPosition());
									findValidWidgetArray_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
								}
							}
						}
					}
					//}
				}
			}
		//}
	
		return found;
	}
	
	
	// BCEL version
	/*public boolean findVersion(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (Method m : cg.getMethods()) {
				MethodGen mg = null;
				//MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);

				try {
					mg = new MethodGen(m, cg.getClassName(), cpg);
				} catch (ClassGenException e) {
					e.printStackTrace();
					continue;
				}
				
				//if (mg.getArgumentTypes().length != 0)
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				InstructionHandle[] ihs = il.getInstructionHandles();

				for (InstructionHandle ih : ihs) {
					if (ih.getInstruction() instanceof SIPUSH
							&& ih.getNext() != null
							&& ih.getNext().getInstruction() instanceof ICONST
							&& ih.getNext().getNext().getInstruction() instanceof GETSTATIC) {
						SIPUSH instr = (SIPUSH) ih.getInstruction();
						ICONST iconst = (ICONST) ih.getNext().getInstruction();
						
						//Log.log.info("Version " + instr.getValue().shortValue() + " " + iconst.getValue().intValue());

						Client.get.setVersionInfo(instr.getValue().shortValue(), iconst.getValue().intValue());
						
						found = true;
						//Log.log.info(cg.getClassName() + "." + mg.getName() + "() - pos: " + instr.getPosition());
					}
				}
			}
		}
	
		return found;
	}*/
	
	private boolean findMenuPositionFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(-1),
						new PI_SIPUSH(cpg).adjustValue(-256)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					if (matches1[0].length != 4)
						continue;
					
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle prevCheckcastIh = searcher.getPrev(false, CHECKCAST.class);
						
						if (prevCheckcastIh != null && ((CHECKCAST) prevCheckcastIh.getInstruction()).getType(cpg).toString().equals(MenuItemNode.get.getInternalName())) {
							searcher.getPrev(true, GETSTATIC.class);
							searcher.getPrev(true, GETSTATIC.class);
							InstructionHandle prevGetStaticIh = searcher.getPrev(true, GETSTATIC.class);
							InstructionHandle prev2GetStaticIh = searcher.getPrev(true, GETSTATIC.class);
							InstructionHandle prev3GetStaticIh = searcher.getPrev(true, GETSTATIC.class);
							InstructionHandle prev4GetStaticIh = searcher.getPrev(true, GETSTATIC.class);
							
							if (prevGetStaticIh != null && prev2GetStaticIh != null && prev3GetStaticIh != null && prev4GetStaticIh != null) {
								GETSTATIC prevGetStaticInstr = (GETSTATIC) prevGetStaticIh.getInstruction();
								GETSTATIC prev2GetStaticInstr = (GETSTATIC) prev2GetStaticIh.getInstruction();
								GETSTATIC prev3GetStaticInstr = (GETSTATIC) prev3GetStaticIh.getInstruction();
								GETSTATIC prev4GetStaticInstr = (GETSTATIC) prev4GetStaticIh.getInstruction();
								
								if (prevGetStaticInstr.getType(cpg).equals(Type.INT) && prev2GetStaticInstr.getType(cpg).equals(Type.INT) && prev3GetStaticInstr.getType(cpg).equals(Type.INT) && prev4GetStaticInstr.getType(cpg).equals(Type.INT)) {
									// TODO: add multi-pattern and pattern for reverse aconst_null/getstatic
									InstructionHandle ifInstr = searcher.searchPrev(false, CompareType.Opcode, new InstructionClass(GETSTATIC.class), new InstructionClass(IfInstruction.class));
									if (ifInstr != null) ifInstr = ifInstr.getNext();
									
									InstructionHandle aconst_nullCheck = ifInstr.getPrev().getPrev();
									boolean hasAconstNull = false;
									if (aconst_nullCheck != null && aconst_nullCheck.getInstruction() instanceof ACONST_NULL)
										hasAconstNull = true;
									
									if (ifInstr != null && (hasAconstNull || ifInstr.getInstruction() instanceof IFNULL || ifInstr.getInstruction() instanceof IFNONNULL)) {
										GETSTATIC instr = prev4GetStaticInstr;
										Client.get.subMenuX.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
										
										instr = prev3GetStaticInstr;
										Client.get.subMenuY.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));			
										
										instr = prev2GetStaticInstr;
										Client.get.subMenuWidth.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));			
										
										instr = prevGetStaticInstr;
										Client.get.subMenuHeight.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));			
										
										found = true;
										
										findMenuPositionFields_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
										
										
										searcher.gotoStart();
										Client.get.subMenuX.set(searcher.findNextGetMultiplier(false, true, Client.get.subMenuX, cpg));
										Client.get.subMenuY.set(searcher.findNextGetMultiplier(false, true, Client.get.subMenuY, cpg));
										Client.get.subMenuWidth.set(searcher.findNextGetMultiplier(false, true, Client.get.subMenuWidth, cpg));
										Client.get.subMenuHeight.set(searcher.findNextGetMultiplier(false, true, Client.get.subMenuHeight, cpg));
									} else {
										GETSTATIC instr = prev4GetStaticInstr;
										Client.get.menuX.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
										
										instr = prev3GetStaticInstr;
										Client.get.menuY.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));			
										
										instr = prev2GetStaticInstr;
										Client.get.menuWidth.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));			
										
										instr = prevGetStaticInstr;
										Client.get.menuHeight.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));			
										
										found = true;
										
										findMenuPositionFields_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
										
										
										searcher.gotoStart();
										Client.get.menuX.set(searcher.findNextGetMultiplier(false, true, Client.get.menuX, cpg));
										Client.get.menuY.set(searcher.findNextGetMultiplier(false, true, Client.get.menuY, cpg));
										Client.get.menuWidth.set(searcher.findNextGetMultiplier(false, true, Client.get.menuWidth, cpg));
										Client.get.menuHeight.set(searcher.findNextGetMultiplier(false, true, Client.get.menuHeight, cpg));
									}
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findMenuFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] verifyPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName())
				);
				
				List<CompareInstruction[]> verifyPatterns1 = new ArrayList<>();
				verifyPatterns1.add(verifyPattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] verifyMatches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, verifyPatterns1);
				
				if (verifyMatches1 == null)
					continue;
				
				final CompareInstruction[] pattern1_1partial = CompareInstruction.Methods.toCompareInstructions(
						ACONST_NULL.class,
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName()),
						IfInstruction.class,
						new SynonymInstruction(CompareType.Opcode, LDC.class, LDC_W.class),
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName()),
						new PI_GETFIELD(cpg).adjustType(Type.INT),
						IMUL.class,
						IRETURN.class
				);
				final CompareInstruction[] pattern1_1 = CompareInstruction.Methods.toCompareInstructions( pattern1_1partial, pattern1_1partial );
				
				// multiplier switch from pattern1_1
				final CompareInstruction[] pattern1_2partial = CompareInstruction.Methods.toCompareInstructions(
						ACONST_NULL.class,
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName()),
						IfInstruction.class,
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName()),
						new PI_GETFIELD(cpg).adjustType(Type.INT),
						new SynonymInstruction(CompareType.Opcode, LDC.class, LDC_W.class),
						IMUL.class,
						IRETURN.class
				);
				final CompareInstruction[] pattern1_2 = CompareInstruction.Methods.toCompareInstructions( pattern1_2partial, pattern1_2partial );
				
				final CompareInstruction[] pattern1_3 = CompareInstruction.Methods.toCompareInstructions( pattern1_1partial, pattern1_2partial );
				final CompareInstruction[] pattern1_4 = CompareInstruction.Methods.toCompareInstructions( pattern1_2partial, pattern1_1partial );
				
				
				// different null check structure from pattern1
				final CompareInstruction[] pattern2_1partial = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName()),
						new SynonymInstruction(CompareType.Opcode, IFNULL.class, IFNONNULL.class),
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName()),
						new PI_GETFIELD(cpg).adjustType(Type.INT),
						new SynonymInstruction(CompareType.Opcode, LDC.class, LDC_W.class),
						IMUL.class,
						IRETURN.class
				);
				final CompareInstruction[] pattern2_1 = CompareInstruction.Methods.toCompareInstructions( pattern2_1partial, pattern2_1partial );
				
				// multiplier switch from pattern2_1
				final CompareInstruction[] pattern2_2partial = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName()),
						new SynonymInstruction(CompareType.Opcode, IFNULL.class, IFNONNULL.class),
						new SynonymInstruction(CompareType.Opcode, LDC.class, LDC_W.class),
						new PI_GETSTATIC(cpg).adjustTypeString(false, MenuItemNode.get.getInternalName()),
						new PI_GETFIELD(cpg).adjustType(Type.INT),
						IMUL.class,
						IRETURN.class
				);
				final CompareInstruction[] pattern2_2 = CompareInstruction.Methods.toCompareInstructions( pattern2_2partial, pattern2_2partial );
				
				final CompareInstruction[] pattern2_3 = CompareInstruction.Methods.toCompareInstructions( pattern2_1partial, pattern2_2partial );
				final CompareInstruction[] pattern2_4 = CompareInstruction.Methods.toCompareInstructions( pattern2_2partial, pattern2_1partial );
				
				
				final CompareInstruction[] pattern1n2_1 = CompareInstruction.Methods.toCompareInstructions( pattern1_1partial, pattern2_1partial );
				final CompareInstruction[] pattern1n2_2 = CompareInstruction.Methods.toCompareInstructions( pattern1_1partial, pattern2_2partial );
				final CompareInstruction[] pattern1n2_3 = CompareInstruction.Methods.toCompareInstructions( pattern1_2partial, pattern2_1partial );
				final CompareInstruction[] pattern1n2_4 = CompareInstruction.Methods.toCompareInstructions( pattern1_2partial, pattern2_2partial );
				
				final CompareInstruction[] pattern1n2_5 = CompareInstruction.Methods.toCompareInstructions( pattern2_1partial, pattern1_1partial );
				final CompareInstruction[] pattern1n2_6 = CompareInstruction.Methods.toCompareInstructions( pattern2_1partial, pattern1_2partial );
				final CompareInstruction[] pattern1n2_7 = CompareInstruction.Methods.toCompareInstructions( pattern2_2partial, pattern1_1partial );
				final CompareInstruction[] pattern1n2_8 = CompareInstruction.Methods.toCompareInstructions( pattern2_2partial, pattern1_2partial );
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1_1);
				patterns1.add(pattern1_2);
				patterns1.add(pattern1_3);
				patterns1.add(pattern1_4);
				patterns1.add(pattern2_1);
				patterns1.add(pattern2_2);
				patterns1.add(pattern2_3);
				patterns1.add(pattern2_4);
				patterns1.add(pattern1n2_1);
				patterns1.add(pattern1n2_2);
				patterns1.add(pattern1n2_3);
				patterns1.add(pattern1n2_4);
				patterns1.add(pattern1n2_5);
				patterns1.add(pattern1n2_6);
				patterns1.add(pattern1n2_7);
				patterns1.add(pattern1n2_8);
				
				//Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns1);
				InstructionHandle[] matches1Combined = (matches1 != null) ? Util.combine(matches1) : null;

				if (matches1Combined != null && matches1Combined.length == 1) {
					for (InstructionHandle ih : matches1Combined) {
						searcher.gotoHandle(ih);
						InstructionHandle firstStaticFieldIh = searcher.getNext(true, GETSTATIC.class);
						InstructionHandle secondStaticFieldIh = searcher.getNext(true, GETSTATIC.class);
						secondStaticFieldIh = searcher.getNext(true, GETSTATIC.class);
						
						if (firstStaticFieldIh != null && secondStaticFieldIh != null) {
							searcher.gotoHandle(ih);
							
							GETSTATIC instr = (GETSTATIC) firstStaticFieldIh.getInstruction();
							Client.get.secondMenuItem.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							
							instr = (GETSTATIC) secondStaticFieldIh.getInstruction();
							Client.get.firstMenuItem.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							
							
							InstructionHandle prevStaticBooleanFieldIh = searcher.searchPrev(false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.BOOLEAN));
							InstructionHandle nextStaticBooleanFieldIh = searcher.searchNext(false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.BOOLEAN));
							
							if (prevStaticBooleanFieldIh != null && nextStaticBooleanFieldIh != null) {
								instr = (GETSTATIC) prevStaticBooleanFieldIh.getInstruction();
								Client.get.isMenuOpen.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								
								instr = (GETSTATIC) nextStaticBooleanFieldIh.getInstruction();
								Client.get.isMenuCollapsed.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
								
								
								final CompareInstruction[] menuItemsPattern = CompareInstruction.Methods.toCompareInstructions(
										NEW.class,
										DUP.class,
										new PI_GETSTATIC(cpg).adjustTypeString(false, NodeDeque.get.getInternalName()),
										INVOKESPECIAL.class,
										ASTORE.class
								);
								
								InstructionHandle[] menuItemsMatches = searcher.searchAllFromStart(false, CompareType.Opcode, menuItemsPattern);
								if (menuItemsMatches != null) {
									for (InstructionHandle ih2 : menuItemsMatches) {
										instr = (GETSTATIC) ih2.getNext().getNext().getInstruction();
										Client.get.menuItems.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									}
								}
								
								
								final CompareInstruction[] collapsedMenuItemsPattern = CompareInstruction.Methods.toCompareInstructions(
										NEW.class,
										DUP.class,
										new PI_GETSTATIC(cpg).adjustTypeString(false, NodeSubQueue.get.getInternalName()),
										INVOKESPECIAL.class,
										ASTORE.class
								);
								
								InstructionHandle[] collapsedMenuItemsMatches = searcher.searchAllFromStart(false, CompareType.Opcode, collapsedMenuItemsPattern);
								if (collapsedMenuItemsMatches != null) {
									for (InstructionHandle ih2 : collapsedMenuItemsMatches) {
										instr = (GETSTATIC) ih2.getNext().getNext().getInstruction();
										Client.get.collapsedMenuItems.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									}
								}
								
								
								final CompareInstruction[] currentMenuGroupNodePattern = CompareInstruction.Methods.toCompareInstructions(
										NEW.class,
										DUP.class,
										new PI_GETSTATIC(cpg).adjustTypeString(false, MenuGroupNode.get.getInternalName()),
										new PI_GETFIELD(cpg).adjustClassName(MenuGroupNode.get.items.getInternalClassName()).adjustFieldName(MenuGroupNode.get.items.getInternalName()).adjustSignature(MenuGroupNode.get.items.getSignature()),
										INVOKESPECIAL.class,
										ASTORE.class
								);
								
								InstructionHandle[] currentMenuGroupNodeMatches = searcher.searchAllFromStart(false, CompareType.Opcode, currentMenuGroupNodePattern);
								if (currentMenuGroupNodeMatches != null) {
									for (InstructionHandle ih2 : currentMenuGroupNodeMatches) {
										instr = (GETSTATIC) ih2.getNext().getNext().getInstruction();
										Client.get.currentMenuGroupNode.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									}
								}
								
								
								if (menuItemsMatches != null && collapsedMenuItemsMatches != null && currentMenuGroupNodeMatches != null) {
									found = true;
									
									findMenuFields_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());	
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	/**
	 * lastSelectedUsableComponentAction, lastSelectedUsableComponentId, isUsableComponentSelected
	 * @param cg
	 * @return
	 */
	private boolean findUsableComponentFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						ILOAD.class,
						new PI_BIPUSH(cpg).adjustValue(32),
						ISHL.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(8),
						ISHL.class
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(9),
						ISHR.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle prevNew = searcher.searchPrev(true, true, CompareType.Opcode, new PI_NEW(cpg).adjustTypeString(false, "java.lang.StringBuilder"));
						
						if (prevNew != null) {
							//
							// lastSelectedUsableComponentAction
							//
							InstructionHandle fieldIh = searcher.searchPrev(true, true, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.STRING));
							
							if (fieldIh != null) {
								GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
								Client.get.lastSelectedUsableComponentAction.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								//found = true;
								
								findLastSelectedUsableComponentAction_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
								
								
								//
								// lastSelectedUsableComponentId
								//
								fieldIh = searcher.searchPrev(true, false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.INT));
								
								if (fieldIh != null) {
									instr = (GETSTATIC) fieldIh.getInstruction();
									Client.get.lastSelectedUsableComponentId.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									//found = true;
									
									findLastSelectedUsableComponentId_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
									
									searcher.gotoStart();
									Client.get.lastSelectedUsableComponentId.set(searcher.findNextGetMultiplier(false, true, Client.get.lastSelectedUsableComponentId, cpg));
									searcher.gotoHandle(fieldIh);
									
									
									//
									// isUsableComponentSelected
									//
									fieldIh = searcher.searchPrev(false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.BOOLEAN));
									
									if (fieldIh != null) {
										instr = (GETSTATIC) fieldIh.getInstruction();
										Client.get.isUsableComponentSelected.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
										found = true;
										
										findIsUsableComponentSelected_Count.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
									}
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findLastSelectedUsableComponentOption(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				// is very short method (~70 instructions)
				if (il.getLength() > 100)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						GETSTATIC.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					if (matches1[0].length == 5) {
						int isUsableComponentSelectedCount = 0;
						int lastSelectedUsableComponentActionCount = 0;
						List<FieldData> otherFields = new ArrayList<>();
						
						for (InstructionHandle ih : matches1[0]) {
							GETSTATIC instr = (GETSTATIC) ih.getInstruction();
							
							if (instr.getType(cpg).equals(Type.BOOLEAN)) {
								if (instr.getReferenceType(cpg).toString().equals(Client.get.isUsableComponentSelected.getInternalClassName())
										&& instr.getFieldName(cpg).equals(Client.get.isUsableComponentSelected.getInternalName()))
									isUsableComponentSelectedCount++;
							} else if (instr.getType(cpg).equals(Type.STRING)) {
								if (instr.getReferenceType(cpg).toString().equals(Client.get.lastSelectedUsableComponentAction.getInternalClassName())
										&& instr.getFieldName(cpg).equals(Client.get.lastSelectedUsableComponentAction.getInternalName()))
									lastSelectedUsableComponentActionCount++;
								else {
									FieldData fd = FieldData.createTemp();
									fd.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
									otherFields.add(fd);
								}
							}
						}
						
						if (isUsableComponentSelectedCount == 1 
								&& lastSelectedUsableComponentActionCount == 2 
								&& otherFields.size() == 2
								&& otherFields.get(0).compareData(otherFields.get(1))
								) {
							
							Client.get.lastSelectedUsableComponentOption.setData(otherFields.get(0));					
							found = true;
							
							findLastSelectedUsableComponentOption_Count.add(cg.getClassName() + "." + mg.getName(), -1);
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findMinimapFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC2_W(cpg).adjustValue(2607.5945876176133D),
						DMUL.class
				);
				
				final CompareInstruction[] pattern2_1 = CompareInstruction.Methods.toCompareInstructions(
						SynonymInstruction.Load_OR_Get,
						new PI_SIPUSH(cpg).adjustValue(128),
						IDIV.class,
						new PI_BIPUSH(cpg).adjustValue(48),
						IADD.class
				);
				
				// BIPUSH for IADD switched
				final CompareInstruction[] pattern2_2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(48),
						SynonymInstruction.Load_OR_Get,
						new PI_SIPUSH(cpg).adjustValue(128),
						IDIV.class,
						IADD.class
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(0x3FFF),
						IAND.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern3);
				
				List<CompareInstruction[]> patterns2 = new ArrayList<>();
				patterns2.add(pattern2_1);
				patterns2.add(pattern2_2);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				InstructionHandle[][] matches2 = null;
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					matches2 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns2);
					
					if (matches2 != null) {
						for (InstructionHandle ih : matches1[0]) {
							searcher.gotoHandle(ih);
							
							// just used for multiplier searcher guidance and searching between for ConstantPushInstruction for minimapSetting
							InstructionHandle prevStore = searcher.searchPrev(false, CompareType.Opcode, SynonymInstruction.Store_OR_Put);
							if (prevStore == null)
								continue;
							
							
							InstructionHandle prevIf = searcher.searchPrev(true, CompareType.Opcode, new InstructionClass(IfInstruction.class));
							
							if (prevIf != null) {
								InstructionHandle cpiIh = searcher.search(false, false, prevStore, prevIf, CompareType.Opcode, new InstructionClass(ConstantPushInstruction.class));
								
								if (cpiIh != null) {
									InstructionHandle fieldIh = searcher.searchPrev(true, false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.INT));
									
									if (fieldIh != null) {
										GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
										Client.get.minimapSetting.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
										found = true;
										
										findMinimapSetting_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
										
										searcher.gotoHandle(prevStore);
										Client.get.minimapSetting.set(searcher.findNextGetMultiplier(false, true, Client.get.minimapSetting, cpg));
										
										
										// get minimapSetting check
										ConstantPushInstruction cpi = (ConstantPushInstruction) cpiIh.getInstruction();
										Client.get.minimapSetting_check.set("CONST", "CONST", cpi.getType(cpg).getSignature());
										Client.get.minimapSetting_check.set(cpi.getValue());
									}
								}
							} else
								continue;
							
							searcher.gotoHandle(ih);
							
							InstructionHandle nextStore = searcher.searchNext(true, CompareType.Opcode, SynonymInstruction.Store_OR_Put);
							
							if (nextStore != null) {
								InstructionHandle fieldIh = searcher.searchPrev(true, false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.INT));
								
								if (fieldIh != null) {
									GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
									Client.get.minimapOffset.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									found = true;
									
									findMinimapOffset_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
									
									searcher.gotoHandle(prevStore);
									Client.get.minimapOffset.set(searcher.findNextGetMultiplier(false, true, Client.get.minimapOffset, cpg));
								}
								
								searcher.gotoHandle(nextStore);
								InstructionHandle next3FFF = searcher.searchNext(true, CompareType.Opcode, new PI_SIPUSH(cpg).adjustValue(0x3FFF), new InstructionClass(IAND.class));
								
								if (next3FFF != null) {
									nextStore = searcher.searchNext(true, CompareType.Opcode, SynonymInstruction.Store_OR_Put);
									
									if (nextStore != null) {
										fieldIh = searcher.searchPrev(false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.FLOAT));
										
										if (fieldIh != null) {
											GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
											Client.get.minimapAngle.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
											found = true;
											
											findMinimapAngle_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
										}
									}
								}
								
								searcher.gotoHandle(nextStore);
								InstructionHandle next4096 = searcher.searchNext(true, CompareType.Opcode, new PI_SIPUSH(cpg).adjustValue(4096));
								
								if (next4096 != null) {
									nextStore = searcher.searchNext(true, CompareType.Opcode, SynonymInstruction.Store_OR_Put);
									
									if (nextStore != null) {
										fieldIh = searcher.searchPrev(false, CompareType.Opcode, new PI_GETSTATIC(cpg).adjustType(Type.INT));
										
										if (fieldIh != null) {
											GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
											Client.get.minimapScale.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
											found = true;
											
											findMinimapScale_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
											
											// multiplier here is not correct, search next pass
											//searcher.gotoHandle(prevStore);
											//Client.get.minimapScale.set(searcher.findNextGetMultiplier(false, true, Client.get.minimapScale, cpg).intValue());
										}
									}
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findMinimapScaleMultiplier(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] exclusionPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(4096)
				);
				
				List<CompareInstruction[]> exclusionPatterns1 = new ArrayList<>();
				exclusionPatterns1.add(exclusionPattern1);
				
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustClassName(Client.get.minimapScale.getInternalClassName()).adjustFieldName(Client.get.minimapScale.getInternalName()).adjustSignature(Client.get.minimapScale.getSignature())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] exclusionMatches1 = searcher.searchAllFromStart(false, false, CompareType.Opcode, exclusionPatterns1);
				if (exclusionMatches1 != null)
					continue;
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					searcher.gotoStart();
					Client.get.minimapScale.set(searcher.findNextGetMultiplier(false, true, Client.get.minimapScale, cpg).intValue());
				}
			}
		//}
	
		return found;
	}
	
	private boolean findDestinationFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(9),
						ISHR.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(256)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle prevStore = searcher.searchPrev(false, CompareType.Opcode, SynonymInstruction.Store_OR_Put);
						InstructionHandle nextIf = searcher.searchNext(false, CompareType.Opcode, new InstructionClass(IfInstruction.class));
						
						if (prevStore != null && nextIf != null) {
							searcher.gotoHandle(prevStore);
							InstructionHandle sipush256 = searcher.search(false, true, prevStore, nextIf, CompareType.Opcode, new PI_SIPUSH(cpg).adjustValue(256));
							
							if (sipush256 != null) {
								searcher.gotoHandle(nextIf.getNext());
								InstructionHandle nextNextIf = searcher.searchNext(false, CompareType.Opcode, new InstructionClass(IfInstruction.class));
								
								if (nextNextIf != null) {
									InstructionHandle[] fieldIhs = searcher.searchAll(false, true, nextIf, nextNextIf, CompareType.Opcode, new PI_PUTSTATIC(cpg).adjustType(Type.INT));
									
									if (fieldIhs != null && fieldIhs.length == 2) {
										InstructionHandle fieldIh = fieldIhs[0];
										
										if (fieldIh != null) {
											PUTSTATIC instr = (PUTSTATIC) fieldIh.getInstruction();
											Client.get.destX.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
											found = true;
											
											findDestX_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
											
											searcher.gotoHandle(prevStore);
											Client.get.destX.set(searcher.findNextGetMultiplier(false, true, Client.get.destX, cpg));
										}
										
										fieldIh = fieldIhs[1];
										
										if (fieldIh != null) {
											PUTSTATIC instr = (PUTSTATIC) fieldIh.getInstruction();
											Client.get.destY.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
											found = true;
											
											findDestY_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
											
											searcher.gotoHandle(prevStore);
											Client.get.destY.set(searcher.findNextGetMultiplier(false, true, Client.get.destY, cpg));
										}
									}
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	// TODO: this just finds any GETSTATIC use; make it more accurate
	private boolean findSettingData(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, SettingData.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.settingData.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findSettingData_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findItemTables(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, HashTable.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_PUTFIELD(cpg).adjustClassName(ItemTable.get.itemIds.getInternalClassName()).adjustFieldName(ItemTable.get.itemIds.getInternalName()).adjustSignature(ItemTable.get.itemIds.getSignature())
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_PUTFIELD(cpg).adjustClassName(ItemTable.get.itemStackSizes.getInternalClassName()).adjustFieldName(ItemTable.get.itemStackSizes.getInternalName()).adjustSignature(ItemTable.get.itemStackSizes.getSignature())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.itemTables.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findItemTables_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findEventQueue(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (Field f : cg.getFields()) {
				if (!f.isStatic())
					continue;
				
				if (!f.getSignature().equals("Ljava/awt/EventQueue;"))
					continue;
				
				Client.get.eventQueue.set(cg.getClassName(), f.getName(), f.getSignature());					
				found = true;
				
				findEventQueue_Count.add(cg.getClassName(), 0);
			}
		//}
	
		return found;
	}
	
	private boolean findItemNodeCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.isStatic())
					continue;
				
				Type[] args = mg.getArgumentTypes();
				if (args == null || args.length < 4)
					continue;
				
				boolean foundItemNodeArg = false;
				for (Type arg : args) {
					if (arg.toString().equals(ItemNode.get.getInternalName())) {
						foundItemNodeArg = true;
						break;
					}
				}
				
				if (!foundItemNodeArg)
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, HashTable.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(ItemNode.get.id.getInternalClassName()).adjustFieldName(ItemNode.get.id.getInternalName()).adjustSignature(ItemNode.get.id.getSignature())
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(ItemNode.get.stackSize.getInternalClassName()).adjustFieldName(ItemNode.get.stackSize.getInternalName()).adjustSignature(ItemNode.get.stackSize.getSignature())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.itemNodeCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findItemNodeCache_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findItemDefLoader(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, ItemDefLoader.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.itemDefLoader.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findItemDefLoader_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findGeOffers(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, GEOffer.get.getInternalName() + "[][]")
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.geOffers.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findGeOffers_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findWorldInfo(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, WorldInfo.get.getInternalName()),
						new PI_PUTSTATIC(cpg).adjustTypeString(false, WorldInfo.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, WorldInfo.get.getInternalName()),
						new PI_GETSTATIC(cpg).adjustTypeString(false, "java.applet.Applet"),
						new PI_INVOKEVIRTUAL(cpg).adjustMethodName("getCodeBase"),
						new PI_INVOKEVIRTUAL(cpg).adjustMethodName("getHost"),
						new PI_PUTFIELD(cpg).adjustClassName(WorldInfo.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih.getNext();
						
						if (fieldIh != null) {
							PUTSTATIC instr = (PUTSTATIC) fieldIh.getInstruction();
							Client.get.worldInfo.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findWorldInfo_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findGraphicsSettingsContainer(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, GraphicsSettingsContainer.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETSTATIC instr = (GETSTATIC) fieldIh.getInstruction();
							Client.get.graphicsSettingsContainer.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findGraphicsSettingsContainer_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	/*private boolean findWorldInfo(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Client.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_NEW(cpg).adjustTypeString(false, WorldInfo.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_PUTSTATIC(cpg).adjustTypeString(false, WorldInfo.get.getInternalName())
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						GETSTATIC.class,
						GETSTATIC.class,
						new SynonymInstruction(IF_ACMPEQ.class, IF_ACMPNE.class)
				);
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						IfInstruction.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				patterns1.add(pattern4);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()
						&& matches1[0].length == 1 && matches1[1].length == 1) {
					
					boolean sameTypesCompared = false;
					for (InstructionHandle ih : matches1[2]) {
						if (((GETSTATIC) ih.getInstruction()).getSignature(cpg).equals(((GETSTATIC) ih.getNext().getInstruction()).getSignature(cpg)))
							sameTypesCompared = true;
					}
					
					if (sameTypesCompared) {
						for (InstructionHandle ih : matches1[1]) {
							InstructionHandle fieldIh = ih;
							
							if (fieldIh != null) {
								PUTSTATIC instr = (PUTSTATIC) fieldIh.getInstruction();
								Client.get.worldInfo.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								found = true;
								
								findWorldInfo_Count.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());	
							}
						}
					}
				}
			}
		//}
	
		return found;
	}*/

	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Client.get.setInternalName(cg.getClassName());
			
			BuildNumberAnalyzer.findVersion(cg);
			
			//findVersion(cg);
			findMyPlayer(cg);
		}
		
		findCanvasSizeFields(cg);
		findCameraYaw(cg);
		findCameraPitch(cg);
		findCanvas(cg);
		findGameInfo(cg);
		findRender(cg);
		findApplets(cg);
		findUsableComponentFields(cg);
		findMinimapFields(cg);
		findDestinationFields(cg);
		findEventQueue(cg);
	}
	
	public void analyze_pass2(ClassGen cg) {
		if (Camera.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Camera internal class name is null!");
			//return;
		} else
			findCamera(cg);
		
		if (GroundBytes.get.getInternalName() == null || GroundBytes.get.bytes.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: GroundBytes/bytes internal class name is null!");
			//return;
		} else
			findPlane(cg);
		
		
		if (Graphics.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Graphics internal class name is null!");
			//return;
		} else
			findGraphics(cg);
		
		
		if (Location.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Location internal class name is null!");
			//return;
		} else
			findLocationArray(cg);
		
		
		if (ItemTable.get.getInternalName() == null || ItemTable.get.itemIds.getInternalName() == null || ItemTable.get.itemStackSizes.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: ItemTable/itemIds/itemStackSizes internal class name is null!");
			//return;
		} else
			findItemTables(cg);
		
		
		if (Client.get.minimapScale.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Client.minimapScale internal field name is null!");
			//return;
		} else
			findMinimapScaleMultiplier(cg);
		
		if (Player.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Player internal class name is null!");
			//return;
		} else
			findPlayers(cg);
		
		
		if (GEOffer.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: GEOffer internal class name is null!");
			//return;
		} else
			findGeOffers(cg);
		
		
		if (WorldInfo.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: WorldInfo internal class name is null!");
			//return;
		} else
			findWorldInfo(cg);
		
		
		if (Client.get.isUsableComponentSelected.getInternalName() == null
				|| Client.get.lastSelectedUsableComponentAction.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Client.isUsableComponentSelected or Client.lastSelectedUsableComponentAction internal field name is null!");
			//return;
		} else
			findLastSelectedUsableComponentOption(cg);
	}
	
	public void analyze_pass3(ClassGen cg) {
		if (SettingData.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: SettingData internal class name is null!");
			//return;
		} else
			findSettingData(cg);
		
		
		if (Component.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Component internal class name is null!");
			//return;
		} else
			findComponentBoundsArray(cg);
		
		
		if (HashTable.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: HashTable internal class name is null!");
			//return;
		} else if (Component.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Component internal class name is null!");
			//return;
		} else
			findComponentNodeCache(cg);
		
		
		if (HashTable.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: HashTable internal class name is null!");
			//return;
		} else if (NpcNode.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: NpcNode internal class name is null!");
			//return;
		} else if (Node.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Node internal class name is null!");
			//return;
		} else
			findNpcNodeCacheAndIndexArray(cg);
		
		
		if (ItemNode.get.getInternalName() == null || ItemNode.get.id.getInternalName() == null || ItemNode.get.stackSize.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: ItemNode/id/stackSize internal class name is null!");
			//return;
		} else
			findItemNodeCache(cg);
		
		
		if (GraphicsSettingsContainer.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: GraphicsSettingsContainer internal class name is null!");
			//return;
		} else
			findGraphicsSettingsContainer(cg);
	}
	
	public void analyze_pass4(ClassGen cg) {
		if (NpcNode.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: NpcNode internal class name is null!");
			//return;
		} else if (NpcNode.get.npc.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: NpcNode field 'npc' internal name is null!");
			//return;
		} else if (Npc.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Npc internal class name is null!");
			//return;
		} else
			findNpcNodeArray(cg);
		
		
		if (Widget.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Widget internal class name is null!");
			//return;
		} else if (Component.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Component internal class name is null!");
			//return;
		} else if (Component.get.components.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Component.components internal field name is null!");
			//return;
		} else
			findWidgetCache(cg);
		
		
		if (MenuItemNode.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: MenuItemNode internal class name is null!");
			//return;
		} else
			findMenuPositionFields(cg);
	}
	
	public void analyze_pass5(ClassGen cg) {
		if (Cache.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Cache internal class name is null!");
			//return;
		} else if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: Model internal class name is null!");
			//return;
		} else
			findPlayerModelCache(cg);
		
		
		if (Client.get.widgetCache.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: widgetCache internal field name is null!");
			//return;
		} else
			findValidWidgetArray(cg);
		
		
		if (MenuItemNode.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: MenuItemNode internal class name is null!");
			//return;
		} else if (NodeDeque.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: NodeDeque internal class name is null!");
			//return;
		} else if (NodeSubQueue.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: NodeSubQueue internal class name is null!");
			//return;
		} else if (MenuGroupNode.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: MenuGroupNode internal class name is null!");
			//return;
		} else if (MenuGroupNode.get.items.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: MenuGroupNode.items internal field name is null!");
			//return;	
		} else
			findMenuFields(cg);
	}
	
	public void analyze_ItemDefLoader(ClassGen cg) {
		if (ItemDefLoader.get.getInternalName() == null) {
			System.out.println("ERROR: ClientAnalyzer: ItemDefLoader internal class name is null!");
			//return;
		} else
			findItemDefLoader(cg);
	}

}
