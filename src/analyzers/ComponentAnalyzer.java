package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ANEWARRAY;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FieldInstruction;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC_W;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.LUSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.SIPUSH;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.InstructionClass;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_FieldInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_GETSTATIC;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import searcher.pi.PI_PUTSTATIC;
import searcher.pi.PI_SIPUSH;
import searcher.pi.PrecisionInstruction;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.Client;
import data.Component;
import data.FieldData;
import data.GameInfo;
import data.Model;
import data.ObjectCacheLoader;
import data.Player;

public class ComponentAnalyzer extends Analyzer {
	
	public static ComponentAnalyzer get = new ComponentAnalyzer();

	//CountInfoItem findX = new CountInfoItem("findX");
	//CountInfoItem findY = new CountInfoItem("findY");
	
	public ComponentAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findX, findY);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			if (!mg.getReturnType().toString().equals(Model.get.getInternalName()))
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(8),
					LUSHR.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustValue(0xFFL),
					LAND.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				if (matches1[0].length == 8 && matches1[1].length == 8)
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
		}
	
		return found;
	}
	
	private boolean findComponents(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						ANEWARRAY.class,
						new PI_PUTFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustTypeString(false, Component.get.getInternalName() + "[]"),
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustTypeString(false, Component.get.getInternalName() + "[]"),
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustTypeString(false, Component.get.getInternalName() + "[]")
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						//searcher.gotoHandle(ih);
						//InstructionHandle xIh = searcher.getNext(false, GETFIELD.class);
						
						FieldInstruction fieldInstruction1 = (FieldInstruction) ih.getNext().getInstruction();
						String field1name = fieldInstruction1.getFieldName(cpg);
						FieldInstruction fieldInstruction2 = (FieldInstruction) ih.getNext().getNext().getNext().getInstruction();
						String field2name = fieldInstruction2.getFieldName(cpg);
						FieldInstruction fieldInstruction3 = (FieldInstruction) ih.getNext().getNext().getNext().getNext().getNext().getInstruction();
						String field3name = fieldInstruction3.getFieldName(cpg);
						
						if (field1name.equals(field2name) && field2name.equals(field3name)) {
							InstructionHandle fieldIh = ih.getNext();
							FieldInstruction instr = (FieldInstruction) fieldIh.getInstruction();
							Component.get.components.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							//findComponents.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	
	private boolean findBoundsArrayIndex(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, Component.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustTypeString(false, Component.get.getInternalName() + "[]")
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_PUTSTATIC(cpg).adjustTypeString(false, Component.get.getInternalName() + "[]")
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : searcher.searchAllFromStart(false, CompareType.Opcode, new InstructionClass(INVOKESTATIC.class))) {
						INVOKESTATIC instr = ((INVOKESTATIC) ih.getInstruction());
						
						boolean foundMethodUsage = false; // component[] as argument in invokestatic
						for (Type type : instr.getArgumentTypes(cpg)) {
							if (type.toString().equals(Component.get.getInternalName() + "[]")) {
								foundMethodUsage = true;
								break;
							}
						}
						
						if (foundMethodUsage) {
							searcher.gotoHandle(ih);
							
							InstructionHandle fieldIh = searcher.searchPrev(true, CompareType.Opcode, 
									new PI_GETSTATIC(cpg).adjustTypeString(false, Component.get.getInternalName()), 
									new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT)
									);
							
							if (fieldIh != null) {
								fieldIh = fieldIh.getNext();
								FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
								Component.get.boundsArrayIndex.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
								found = true;
								
								//findBoundsArrayIndex.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
								
								searcher.gotoHandle(ih);
								Component.get.boundsArrayIndex.set(searcher.findPrevGetMultiplier(false, false, Component.get.boundsArrayIndex, cpg));
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findParentIdAndId(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(0xFFFF), new PI_LDC_W(cpg).adjustValue(0xFFFF))
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(0xFFFF0000), new PI_LDC_W(cpg).adjustValue(0xFFFF0000))
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[1]) {
						searcher.gotoHandle(ih);
						ih = searcher.searchPrev(true, CompareType.Opcode, new SynonymInstruction(new PI_LDC(cpg).adjustValue(0xFFFF), new PI_LDC_W(cpg).adjustValue(0xFFFF)));
						InstructionHandle fieldIh = searcher.searchNext(false, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT));
						
						if (fieldIh != null) {
							FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
							Component.get.parentId.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
							found = true;
							
							//findParentId.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
							
							searcher.gotoHandle(ih);
							Component.get.parentId.set(searcher.findNextGetMultiplier(false, false, Component.get.parentId, cpg));
						}
						
						searcher.gotoHandle(ih);
						while ((fieldIh = searcher.searchNext(true, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT))) != null) {
							//if (fieldIh != null) {
								FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
								
								FieldData temp = FieldData.createTemp("tempId");
								temp.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));	
								
								if (!Component.get.parentId.compareData(temp)) {
									Component.get.id.setData(temp);		
									found = true;
									
									//findId.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
									
									searcher.gotoHandle(ih);
									Component.get.id.set(searcher.findNextGetMultiplier(false, false, Component.get.id, cpg));
									break;
								}
							//}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findXYAndScrollbarThumbSizes(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETSTATIC(cpg).adjustClassName(Client.get.componentBoundsArray.getInternalClassName()).adjustFieldName(Client.get.componentBoundsArray.getInternalName()).adjustSignature(Client.get.componentBoundsArray.getSignature())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle setBoundsIh = searcher.searchNext(false, CompareType.Opcode, new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.awt.Rectangle").adjustMethodName("setBounds"));
						
						if (setBoundsIh != null) {
							InstructionHandle[] getFields = searcher.searchAll(false, true, ih, setBoundsIh, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()));
							
							if (getFields != null && getFields.length == 4) {
								InstructionHandle fieldIh = getFields[0];
								
								FieldInstruction instr = (FieldInstruction) fieldIh.getInstruction();
								Component.get.x.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								
								//findX.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
								
								searcher.gotoHandle(ih);
								Component.get.x.set(searcher.findNextGetMultiplier(false, false, Component.get.x, cpg));
								
								
								fieldIh = getFields[1];
								
								instr = (FieldInstruction) fieldIh.getInstruction();
								Component.get.y.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));	
								
								//findY.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
								
								searcher.gotoHandle(ih);
								Component.get.y.set(searcher.findNextGetMultiplier(false, false, Component.get.y, cpg));

								
								fieldIh = getFields[2];
								
								instr = (FieldInstruction) fieldIh.getInstruction();
								Component.get.horizontalScrollbarThumbSize.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));	
								
								//findHorizontalScrollbarThumbSize.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
								
								searcher.gotoHandle(ih);
								Component.get.horizontalScrollbarThumbSize.set(searcher.findNextGetMultiplier(false, false, Component.get.horizontalScrollbarThumbSize, cpg));
								
								
								fieldIh = getFields[3];
								
								instr = (FieldInstruction) fieldIh.getInstruction();
								Component.get.verticalScrollbarThumbSize.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));	
								
								//findVerticalScrollbarThumbSize.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
								
								searcher.gotoHandle(ih);
								Component.get.verticalScrollbarThumbSize.set(searcher.findNextGetMultiplier(false, false, Component.get.verticalScrollbarThumbSize, cpg));
								
								
								found = true;
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findWidthHeight(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				boolean foundComponentArgument = false;
				for (Type type : mg.getArgumentTypes()) {
					if (type.toString().equals(Component.get.getInternalName())) {
						foundComponentArgument = true;
						break;
					}
				}
				
				if (!foundComponentArgument)
					continue;
				
				//if (mg.getArgumentTypes().length != 0)
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(14),
						ISHR.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle putField = searcher.searchNext(true, CompareType.Opcode, new PI_PUTFIELD(cpg));
						
						if (putField != null) {
							if (new PI_PUTFIELD(cpg).adjustClassName(Component.get.horizontalScrollbarThumbSize.getInternalClassName()).adjustFieldName(Component.get.horizontalScrollbarThumbSize.getInternalName()).adjustSignature(Component.get.horizontalScrollbarThumbSize.getSignature()).compare(putField.getInstruction())) {
								InstructionHandle fieldIh = searcher.searchPrev(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT));
								
								if (fieldIh != null) {
									FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
									Component.get.width.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
									found = true;
									
									//findWidth.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
									
									searcher.gotoHandle(ih);
									Component.get.width.set(searcher.findPrevGetMultiplier(false, false, Component.get.width, cpg));
								}
							} else if (new PI_PUTFIELD(cpg).adjustClassName(Component.get.verticalScrollbarThumbSize.getInternalClassName()).adjustFieldName(Component.get.verticalScrollbarThumbSize.getInternalName()).adjustSignature(Component.get.verticalScrollbarThumbSize.getSignature()).compare(putField.getInstruction())) {
								InstructionHandle fieldIh = searcher.searchPrev(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT));
								
								if (fieldIh != null) {
									FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
									Component.get.height.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
									found = true;
									
									//findHeight.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
									
									searcher.gotoHandle(ih);
									Component.get.height.set(searcher.findPrevGetMultiplier(false, false, Component.get.height, cpg));
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findType(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(0x7F),
						IAND.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle prevSipush = searcher.getPrev(false, SIPUSH.class);
						
						if (prevSipush != null 
								&& prevSipush.getNext() != null && prevSipush.getNext().getInstruction() instanceof IAND
								&& new PI_SIPUSH(cpg).adjustValue(0x80).compare(prevSipush.getInstruction())) {
							
							InstructionHandle fieldIh = searcher.getNext(false, PUTFIELD.class);
							
							if (fieldIh != null && new PI_PUTFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT).compare(fieldIh.getInstruction())) {
								FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
								Component.get.type.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
								found = true;
								
								//findType.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
								
								searcher.gotoHandle(ih);
								Component.get.type.set(searcher.findPrevGetMultiplier(false, false, Component.get.type, cpg));
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findHiddenAndVisible(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(0xFFFF), new PI_LDC_W(cpg).adjustValue(0xFFFF))
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(0xFFFF0000), new PI_LDC_W(cpg).adjustValue(0xFFFF0000))
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(1),
						IAND.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[2]) {
						searcher.gotoHandle(ih);
						InstructionHandle prevPutField = searcher.getPrev(false, PUTFIELD.class);
						InstructionHandle nextGetField = searcher.getNext(false, GETFIELD.class);
						
						if (prevPutField != null && nextGetField != null) {
							if (new PI_PUTFIELD(cpg).adjustClassName(Component.get.parentId.getInternalClassName()).adjustFieldName(Component.get.parentId.getInternalName()).adjustSignature(Component.get.parentId.getSignature()).compare(prevPutField.getInstruction())
									&& new PI_GETFIELD(cpg).adjustClassName(Component.get.type.getInternalClassName()).adjustFieldName(Component.get.type.getInternalName()).adjustSignature(Component.get.type.getSignature()).compare(nextGetField.getInstruction())) {
								
								InstructionHandle[] fieldIhs = searcher.searchAll(false, true, prevPutField.getNext(), nextGetField, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.BOOLEAN));
								if (fieldIhs != null && fieldIhs.length == 2) {
									InstructionHandle fieldIh = fieldIhs[0];
									
									if (fieldIh != null) {
										FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
										Component.get.hidden.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
										found = true;
										
										//findHidden.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
									}
									
									fieldIh = fieldIhs[1];
									
									if (fieldIh != null) {
										FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
										Component.get.visible.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
										found = true;
										
										//findVisible.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
									}
								}
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findScrollbarSizesPositions(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				boolean foundComponentArrayArgument = false;
				for (Type type : mg.getArgumentTypes()) {
					if (type.toString().equals(Component.get.getInternalName() + "[]")) {
						foundComponentArrayArgument = true;
						break;
					}
				}
				
				if (!foundComponentArrayArgument)
					continue;
				
				//if (mg.getArgumentTypes().length != 0)
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Component.get.type.getInternalClassName()).adjustFieldName(Component.get.type.getInternalName()).adjustSignature(Component.get.type.getSignature())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Component.get.verticalScrollbarThumbSize.getInternalClassName()).adjustFieldName(Component.get.verticalScrollbarThumbSize.getInternalName()).adjustSignature(Component.get.verticalScrollbarThumbSize.getSignature())
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Component.get.horizontalScrollbarThumbSize.getInternalClassName()).adjustFieldName(Component.get.horizontalScrollbarThumbSize.getInternalName()).adjustSignature(Component.get.horizontalScrollbarThumbSize.getSignature())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					if (matches1[0].length == 1) {
						searcher.gotoHandle(matches1[0][0]);
						
						// verify Component.type is last getfield in instruction list
						if (searcher.getNext(false, GETFIELD.class) == null) {
							for (InstructionHandle ih : matches1[1]) {
								searcher.gotoHandle(ih);
								
								InstructionHandle prevGetField = searcher.getPrev(false, GETFIELD.class);
								InstructionHandle nextGetField = searcher.getNext(true, GETFIELD.class);
								if (nextGetField == null) continue;
								searcher.gotoHandle(nextGetField.getNext());
								InstructionHandle nextNextGetField = searcher.getNext(false, GETFIELD.class);
								InstructionHandle nextPutField = searcher.getNext(true, PUTFIELD.class);
								
								if (prevGetField != null && nextGetField != null && nextNextGetField != null && nextPutField != null) {
									PrecisionInstruction<?> piGetVerticalThumb = new PI_GETFIELD(cpg).adjustClassName(Component.get.verticalScrollbarThumbSize.getInternalClassName()).adjustFieldName(Component.get.verticalScrollbarThumbSize.getInternalName()).adjustSignature(Component.get.verticalScrollbarThumbSize.getSignature());
									PrecisionInstruction<?> piGetComponentIntField = new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT);
									PrecisionInstruction<?> piPutComponentIntField = new PI_PUTFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT);
									
									if (piGetComponentIntField.compare(prevGetField.getInstruction())
											&& piGetComponentIntField.compare(nextGetField.getInstruction())
											&& piGetVerticalThumb.compare(nextNextGetField.getInstruction())
											&& piPutComponentIntField.compare(nextPutField.getInstruction())) {
										
										GETFIELD prevGetInstr = (GETFIELD) prevGetField.getInstruction();
										GETFIELD nextGetInstr = (GETFIELD) nextGetField.getInstruction();
										PUTFIELD nextPutInstr = (PUTFIELD) nextPutField.getInstruction();
										
										if (prevGetInstr.getFieldName(cpg).equals(nextGetInstr.getFieldName(cpg))) {
											InstructionHandle fieldIh = nextGetField;
											
											if (fieldIh != null) {
												FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
												Component.get.verticalScrollbarSize.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
												//found = true;
												
												//findVerticalScrollbarSize.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
												
												searcher.gotoStart();
												// TODO: go9090gos find other multiplier, verify if it is first or second
												/* localgx.bo is Component.verticalScrollbarSize
										        if (localgx.bh * -1777210373 > -596975315 * localgx.bo - 735680013 * localgx.ar) {
										            localgx.bh = (localgx.bo * -2054686985 - 282861463 * localgx.ar);
										        */
												Component.get.verticalScrollbarSize.set(searcher.findNextGetMultiplier(false, false, Component.get.verticalScrollbarSize, cpg));
											}
											
											if (!nextGetInstr.getFieldName(cpg).equals(nextPutInstr.getFieldName(cpg))) {
												fieldIh = nextPutField;
												
												if (fieldIh != null) {
													FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
													Component.get.verticalScrollbarPosition.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
													found = true;
													
													//findVerticalScrollbarPosition.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
													
													searcher.gotoStart();
													Component.get.verticalScrollbarPosition.set(searcher.findNextGetMultiplier(false, false, Component.get.verticalScrollbarPosition, cpg));
												}
											}
										}
									}
								}
							}
							
							for (InstructionHandle ih : matches1[2]) {
								searcher.gotoHandle(ih);
								
								InstructionHandle prevGetField = searcher.getPrev(false, GETFIELD.class);
								InstructionHandle nextGetField = searcher.getNext(true, GETFIELD.class);
								if (nextGetField == null) continue;
								searcher.gotoHandle(nextGetField.getNext());
								InstructionHandle nextNextGetField = searcher.getNext(false, GETFIELD.class);
								InstructionHandle nextPutField = searcher.getNext(true, PUTFIELD.class);
								
								if (prevGetField != null && nextGetField != null && nextNextGetField != null && nextPutField != null) {
									PrecisionInstruction<?> piGetHorizontalThumb = new PI_GETFIELD(cpg).adjustClassName(Component.get.horizontalScrollbarThumbSize.getInternalClassName()).adjustFieldName(Component.get.horizontalScrollbarThumbSize.getInternalName()).adjustSignature(Component.get.horizontalScrollbarThumbSize.getSignature());
									PrecisionInstruction<?> piGetComponentIntField = new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT);
									PrecisionInstruction<?> piPutComponentIntField = new PI_PUTFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT);
									
									if (piGetComponentIntField.compare(prevGetField.getInstruction())
											&& piGetComponentIntField.compare(nextGetField.getInstruction())
											&& piGetHorizontalThumb.compare(nextNextGetField.getInstruction())
											&& piPutComponentIntField.compare(nextPutField.getInstruction())) {
										
										GETFIELD prevGetInstr = (GETFIELD) prevGetField.getInstruction();
										GETFIELD nextGetInstr = (GETFIELD) nextGetField.getInstruction();
										PUTFIELD nextPutInstr = (PUTFIELD) nextPutField.getInstruction();
										
										if (prevGetInstr.getFieldName(cpg).equals(nextGetInstr.getFieldName(cpg))) {
											InstructionHandle fieldIh = nextGetField;
											
											if (fieldIh != null) {
												FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
												Component.get.horizontalScrollbarSize.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
												//found = true;
												
												//findHorizontalScrollbarSize.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
												
												searcher.gotoStart();
												// TODO: the verticalscrollbarsize field multiplier is different from go9090gos log, horizontal may also be affected considering the calculation is similar
												Component.get.horizontalScrollbarSize.set(searcher.findNextGetMultiplier(false, false, Component.get.horizontalScrollbarSize, cpg));
											}
											
											if (!nextGetInstr.getFieldName(cpg).equals(nextPutInstr.getFieldName(cpg))) {
												fieldIh = nextPutField;
												
												if (fieldIh != null) {
													FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
													Component.get.horizontalScrollbarPosition.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
													found = true;
													
													//findHorizontalScrollbarPosition.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
													
													searcher.gotoStart();
													Component.get.horizontalScrollbarPosition.set(searcher.findNextGetMultiplier(false, false, Component.get.horizontalScrollbarPosition, cpg));
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findComponentId(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern11 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.INT)
				);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(1)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(1)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(2)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(2)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(3)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(3)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(4)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(4)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(5)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new ICONST(5)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern6 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(6)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(6)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern7 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(7)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(7)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern8 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(8)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(8)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern9 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(9)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(9)),
						IfInstruction.class
				);
				
				final CompareInstruction[] pattern10 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(10)),
						new SynonymInstruction(CompareType.Opcode, ILOAD.class, new PI_BIPUSH(cpg).adjustValue(10)),
						IfInstruction.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern11);
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				patterns1.add(pattern3);
				patterns1.add(pattern4);
				patterns1.add(pattern5);
				patterns1.add(pattern6);
				patterns1.add(pattern7);
				patterns1.add(pattern8);
				patterns1.add(pattern9);
				patterns1.add(pattern10);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					
					// verify the patterns were found (must have constant push instruction)
					boolean patternNotFound = false;
					for (int i = 1; i < 11; i++) {
						boolean hasConstantPush = false;
						for (InstructionHandle ih : matches1[i]) {
							if (ih.getInstruction() instanceof ICONST || ih.getInstruction() instanceof BIPUSH) {
								hasConstantPush = true;
								break;
							}
							else if (ih.getNext() != null 
									&& (ih.getNext().getInstruction() instanceof ICONST || ih.getNext().getInstruction() instanceof BIPUSH)) {
								hasConstantPush = true;
								break;
							}
						}
						
						if (!hasConstantPush) {
							patternNotFound = true;
							break;
						}
					}
					
					if (patternNotFound)
						continue;
					
					// verify all getfields are the same variable
					boolean allSame = true;
					String fieldName = null;
					for (InstructionHandle ih : matches1[0]) {
						if (fieldName == null)
							fieldName = ((GETFIELD) ih.getInstruction()).getFieldName(cpg);
						else {
							if (!fieldName.equals(((GETFIELD) ih.getInstruction()).getFieldName(cpg))) {
								allSame = false;
								break;
							}
						}
					}
					
					if (!allSame)
						continue;
					
					
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
							
						if (fieldIh != null) {
							FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
							Component.get.componentId.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
							found = true;
							
							//findComponentId.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
							
							searcher.gotoStart();
							Component.get.componentId.set(searcher.findNextGetMultiplier(false, false, Component.get.componentId, cpg));
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findTooltip(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.STRING),
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(""), new PI_LDC_W(cpg).adjustValue("")),
						new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.lang.String").adjustMethodName("equals")
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
							Component.get.tooltip.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
							found = true;
							
							//findTooltip.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findComponentName(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						SynonymInstruction.Load_OR_Get,
						new PI_BIPUSH(cpg).adjustValue(0xF),
						IAND.class,
						SynonymInstruction.Store_OR_Put,
						SynonymInstruction.Load_OR_Get,
						new ICONST(4),
						ISHR.class,
						SynonymInstruction.Store_OR_Put
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.searchPrev(true, false, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.STRING));
						
						if (fieldIh != null) {
							FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
							Component.get.componentName.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
							found = true;
							
							//findComponentName.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findText(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1_1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(LDC.class, LDC_W.class),
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustClassName(Component.get.type.getInternalClassName()).adjustFieldName(Component.get.type.getInternalName()).adjustSignature(Component.get.type.getSignature()),
						IMUL.class,
						new ICONST(4),
						IfInstruction.class
				);
				
				// multiplier flipped
				final CompareInstruction[] pattern1_2 = CompareInstruction.Methods.toCompareInstructions(
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustClassName(Component.get.type.getInternalClassName()).adjustFieldName(Component.get.type.getInternalName()).adjustSignature(Component.get.type.getSignature()),
						new SynonymInstruction(LDC.class, LDC_W.class),
						IMUL.class,
						new ICONST(4),
						IfInstruction.class
				);
				
				// iconst flipped
				final CompareInstruction[] pattern1_3 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(4),
						new SynonymInstruction(LDC.class, LDC_W.class),
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustClassName(Component.get.type.getInternalClassName()).adjustFieldName(Component.get.type.getInternalName()).adjustSignature(Component.get.type.getSignature()),
						IMUL.class,
						IfInstruction.class
				);
				
				// iconst flipped, multiplier flipped
				final CompareInstruction[] pattern1_4 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(4),
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustClassName(Component.get.type.getInternalClassName()).adjustFieldName(Component.get.type.getInternalName()).adjustSignature(Component.get.type.getSignature()),
						new SynonymInstruction(LDC.class, LDC_W.class),
						IMUL.class,
						IfInstruction.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1_1);
				patterns1.add(pattern1_2);
				patterns1.add(pattern1_3);
				patterns1.add(pattern1_4);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns1);
				
				if (matches1 != null) {
					InstructionHandle[] matches1combined = Util.combine(matches1);
					
					for (InstructionHandle ih : matches1combined) {
						searcher.gotoHandle(ih);
						
						// goto end of pattern
						searcher.searchNext(true, CompareType.Opcode, new InstructionClass(IfInstruction.class));
						
						InstructionHandle fieldIh = searcher.searchNext(false, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustClassName(Component.get.getInternalName()).adjustType(Type.STRING));
						
						if (fieldIh != null) {
							FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
							Component.get.text.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
							found = true;
							
							//findText.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Component.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_FieldInstruction<>(cpg).adjustClassName(Component.get.getInternalName()).adjustTypeString(false, "java.lang.String[]")
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							FieldInstruction fieldInstr = (FieldInstruction) fieldIh.getInstruction();
							Component.get.actions.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));					
							found = true;
							
							//findActions.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: ComponentAnalyzer: Model internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			Component.get.setInternalName(cg.getClassName());
			
			findFields(cg);
			findParentIdAndId(cg);
			findType(cg);
			findTooltip(cg);
			findComponentName(cg);
		}
	}
	
	public void analyze_pass3(ClassGen cg) {
		if (Component.get.getInternalName() == null) {
			System.out.println("ERROR: ComponentAnalyzer: Component internal class name is null!");
			return;
		}
		
		findComponents(cg);
		findBoundsArrayIndex(cg);
		findComponentId(cg);
		
		if (Component.get.type.getInternalName() == null) {
			System.out.println("ERROR: ComponentAnalyzer: type internal field name is null!");
			return;
		}
		
		findText(cg);
		
		if (Component.get.parentId.getInternalName() == null) {
			System.out.println("ERROR: ComponentAnalyzer: parentId internal field name is null!");
			return;
		}
		// also needs type; which the previous find also does
		
		findHiddenAndVisible(cg);
	}
	
	public void analyze_pass4(ClassGen cg) {
		if (Client.get.componentBoundsArray.getInternalName() == null) {
			System.out.println("ERROR: ComponentAnalyzer: client.componentBoundsArray internal field name is null!");
			return;
		}
		
		findXYAndScrollbarThumbSizes(cg);
	}
	
	public void analyze_pass5(ClassGen cg) {
		if (Component.get.getInternalName() == null) {
			System.out.println("ERROR: ComponentAnalyzer: Component internal class name is null!");
			return;
		}
		
		if (Component.get.horizontalScrollbarThumbSize.getInternalName() == null) {
			System.out.println("ERROR: ComponentAnalyzer: Component.horizontalScrollbarThumbSize internal field name is null!");
			return;
		}
		
		if (Component.get.verticalScrollbarThumbSize.getInternalName() == null) {
			System.out.println("ERROR: ComponentAnalyzer: Component.verticalScrollbarThumbSize internal field name is null!");
			return;
		}
		
		findWidthHeight(cg);
		findScrollbarSizesPositions(cg);
	}

}
