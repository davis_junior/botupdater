package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FieldInstruction;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.InstructionClass;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_GETSTATIC;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.Client;
import data.Component;
import data.ComponentNode;
import data.GameInfo;
import data.Model;
import data.Player;

public class ComponentNodeAnalyzer extends Analyzer {
	
	public static ComponentNodeAnalyzer get = new ComponentNodeAnalyzer();

	CountInfoItem findMainId = new CountInfoItem("findMainId");
	
	public ComponentNodeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findMainId);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		return false;
	}
	
	protected boolean findClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_GETSTATIC(cpg).adjustClassName(Client.get.componentNodeCache.getInternalClassName()).adjustFieldName(Client.get.componentNodeCache.getInternalName()).adjustSignature(Client.get.componentNodeCache.getSignature())
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				for (InstructionHandle ih : matches1[0]) {
					searcher.gotoHandle(ih);
					InstructionHandle nextGetField = searcher.searchNext(false, CompareType.Opcode, new InstructionClass(GETFIELD.class));
					InstructionHandle nextGetStatic = searcher.searchNext(false, CompareType.Opcode, new InstructionClass(GETSTATIC.class));
					
					if (nextGetField != null && nextGetStatic != null) {
						if (new PI_GETFIELD(cpg).adjustClassName(Component.get.getInternalName()).compare(nextGetField.getInstruction())
								&& new PI_GETSTATIC(cpg).adjustTypeString(false, Component.get.getInternalName()).compare(nextGetStatic.getInstruction())) {
							
							searcher.gotoHandle(nextGetStatic);
							InstructionHandle fieldIh = searcher.getPrev(false, GETFIELD.class);
							
							FieldInstruction instr = (FieldInstruction) fieldIh.getInstruction();
							ComponentNode.get.mainId.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findMainId.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoHandle(ih);
							ComponentNode.get.mainId.set(searcher.findNextGetMultiplier(false, false, ComponentNode.get.mainId, cpg));
							
							
							ComponentNode.get.setInternalName(ComponentNode.get.mainId.getInternalClassName());
						}
					}
				}
			}
			//}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Component.get.getInternalName() == null) {
			System.out.println("ERROR: ComponentNodeAnalyzer: Component internal class name is null!");
			return;
		}
		
		if (Client.get.componentNodeCache.getInternalName() == null) {
			System.out.println("ERROR: ComponentNodeAnalyzer: Client.componentNodeCache internal field name is null!");
			return;
		}
		
		findClass(cg);
	}

}
