package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import searcher.Searcher;
import data.AbstractDefinition;
import data.Cache;
import data.DefLoader;
import data.Model;
import data.ObjectComposite;
import data.ObjectCacheLoader;
import data.ObjectDefinition;

public class DefLoaderAnalyzer extends Analyzer {
	
	public static DefLoaderAnalyzer get = new DefLoaderAnalyzer();

	CountInfoItem findId = new CountInfoItem("findId");
	CountInfoItem findDefinitionCache = new CountInfoItem("findDefinitionCache");
	
	public DefLoaderAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findId, findDefinitionCache);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (cg.getInterfaces() == null || cg.getInterfaces().length == 0)
			return false;
		
		if (!cg.getSuperclassName().equals(java.lang.Object.class.getName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		boolean found1 = false;
		boolean found2 = false;
		
		int cacheFieldCount = 0;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				if (f.getType().getSignature().equals("L" + Cache.get.getInternalName() + ";"))
					cacheFieldCount++;
			}
		}
		
		if (cacheFieldCount == 1)
			found1 = true;
		
		int abstractDefinitionMethodReturnCount = 0;
		
		for (Method m : cg.getMethods()) {
			if (!m.isStatic()) {
				if (m.getReturnType().getSignature().equals("L" + AbstractDefinition.get.getInternalName() + ";"))
					abstractDefinitionMethodReturnCount++;
			}
		}
		
		if (abstractDefinitionMethodReturnCount >= 2) {
			found2 = true;
		}
		
		if (found1 && found2)
			found = true;
		
		return found;
	}
	
	private boolean findDefinitionCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(DefLoader.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				if (!mg.getReturnType().getSignature().equals("L" + AbstractDefinition.get.getInternalName() + ";"))
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustTypeString(false, Cache.get.getInternalName()).adjustClassName(cg.getClassName())
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						DefLoader.get.definitionCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findDefinitionCache.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
						found = true;
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Cache.get.getInternalName() == null) {
			System.out.println("ERROR: DefLoaderAnalyzer: Cache internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			DefLoader.get.setInternalName(cg.getClassName());
			
			if (Cache.get.getInternalName() == null) {
				System.out.println("ERROR: DefLoaderAnalyzer: Cache internal class name is null!");
				//return;
			} else if (AbstractDefinition.get.getInternalName() == null) {
				System.out.println("ERROR: DefLoaderAnalyzer: AbstractDefinition internal class name is null!");
				//return;
			} else
				findDefinitionCache(cg);
		}
	}

}
