package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.DescendingVisitor;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ARETURN;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IFNULL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import util.DependencyEmitter;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.EntityNode;
import data.InteractableData;

public class EntityNodeAnalyzer extends Analyzer {
	
	public static EntityNodeAnalyzer get = new EntityNodeAnalyzer();

	CountInfoItem findData = new CountInfoItem("findData");
	CountInfoItem findDisposed = new CountInfoItem("findDisposed");
	
	public EntityNodeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findData, findDisposed);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (!Util.extendsDefault(cg))
			return false;
		
	    DependencyEmitter visitor = new DependencyEmitter(cg.getJavaClass());
	    DescendingVisitor classWalker = new DescendingVisitor(cg.getJavaClass(), visitor);
	    classWalker.visit();
	    
	    //visitor.printDependencies();
	    
	    for (String dependency : visitor.getDependencies()) {
	    	if (dependency.equals("jaggl/OpenGL"))
	    		return false;
	    }
		
	    for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			Object[] pattern = new Object[]{
					ALOAD.class,
					new ICONST(1),
					PUTFIELD.class
					};
			
			//if (mg.getArgumentTypes().length == 2) {
			InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
					pattern,
					pattern,
					pattern,
					pattern
					));

			if (matches != null) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findData(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		if (cg.getClassName().equals(EntityNode.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.getArgumentTypes().length != 0)
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il.getLength() < 5) // other InteractableData is in a similar method but a very short instruction list, 3 to be exact
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustTypeString(false, InteractableData.get.getInternalName()),
						ARETURN.class
						));
				
				if (matches != null) {
					for (InstructionHandle ih : matches) {
						GETFIELD instr2 = (GETFIELD) ih.getNext().getInstruction();
						
						EntityNode.get.data.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
						findData.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
						
						found = true;
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findDisposed(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(EntityNode.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.BOOLEAN)
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(ACONST_NULL.class, IFNONNULL.class, IFNULL.class)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih.getNext();
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							EntityNode.get.disposed.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findDisposed.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			EntityNode.get.setInternalName(cg.getClassName());
			
			findDisposed(cg);
			
			if (InteractableData.get.getInternalName() == null) {
				System.out.println("ERROR: EntityNodeAnalyzer: data: InteractableData internal class name is null!");
				return;
			}
			
			findData(cg);
		}
	}

}
