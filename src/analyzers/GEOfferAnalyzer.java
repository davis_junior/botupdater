package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ARRAYLENGTH;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.LSTORE;
import org.apache.bcel.generic.LUSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GEOffer;
import data.GameInfo;
import data.ItemTable;
import data.Player;

public class GEOfferAnalyzer extends Analyzer {
	
	public static GEOfferAnalyzer get = new GEOfferAnalyzer();

	//CountInfoItem findItemIds = new CountInfoItem("findItemIds");
	//CountInfoItem findItemStackSizes = new CountInfoItem("findItemStackSizes");
	
	public GEOfferAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findItemIds, findItemStackSizes);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			if (!mg.getReturnType().equals(Type.INT))
				continue;
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(8)
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(8), // also 7 in a different method, though only 1 occurence
					IAND.class
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustType(Type.BYTE)
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			patterns1.add(pattern3);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				if (matches1[0].length == 2 && matches1[1].length == 1 && matches1[2].length == 1) {
					for (InstructionHandle ih : matches1[2]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							GEOffer.get.status.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							//findStatus.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(GEOffer.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				if (!mg.getName().equals("<init>"))
					continue;
				
				if (mg.getArgumentTypes().length == 0)
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_PUTFIELD(cpg).adjustClassName(GEOffer.get.getInternalName()).adjustType(Type.INT)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					if (matches1[0].length == 5) {
						found = true;
						
						for (int i = 0; i < matches1[0].length; i++) {
							InstructionHandle fieldIh = matches1[0][i];
							
							if (fieldIh != null) {
								PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
								switch(i) {
								case 0:
									GEOffer.get.id.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									//findId.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
									
									GEOffer.get.id.set(searcher.findNextPutMultiplier(false, false, GEOffer.get.id, cpg));
									break;
								case 1:
									GEOffer.get.price.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									//findPrice.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
									
									GEOffer.get.price.set(searcher.findNextPutMultiplier(false, false, GEOffer.get.price, cpg));
									break;
								case 2:
									GEOffer.get.total.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									//findTotal.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
									
									GEOffer.get.total.set(searcher.findNextPutMultiplier(false, false, GEOffer.get.total, cpg));
									break;
								case 3:
									GEOffer.get.transfered.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									//findTransfered.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
									
									GEOffer.get.transfered.set(searcher.findNextPutMultiplier(false, false, GEOffer.get.transfered, cpg));
									break;
								case 4:
									GEOffer.get.spend.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
									//findSpend.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
									
									GEOffer.get.spend.set(searcher.findNextPutMultiplier(false, false, GEOffer.get.spend, cpg));
									break;
								}
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			GEOffer.get.setInternalName(cg.getClassName());

			findFields(cg);
		}
	}

}
