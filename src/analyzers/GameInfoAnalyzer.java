package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.DUP;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.INVOKESPECIAL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.NEW;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Character;
import data.Client;
import data.GameInfo;
import data.Model;
import data.ObjectCacheLoader;
import data.ObjectDefLoader;
import data.Player;

public class GameInfoAnalyzer extends Analyzer {
	
	public static GameInfoAnalyzer get = new GameInfoAnalyzer();

	CountInfoItem findBaseInfo = new CountInfoItem("findBaseInfo");
	CountInfoItem findGroundInfo = new CountInfoItem("findGroundInfo");
	CountInfoItem findGroundBytes = new CountInfoItem("findGroundBytes");
	CountInfoItem findObjectDefLoader = new CountInfoItem("findObjectDefLoader");
	
	public GameInfoAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findBaseInfo, findGroundInfo, findGroundBytes, findObjectDefLoader);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
					new SynonymInstruction(new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(-0.05F), new PI_LDC_W(cpg).adjustType(Type.FLOAT).adjustValue(-0.05F)),
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("random"),
					new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(10.0D)
					);

			if (matches != null) {
				// find GroundInfo field
				for (InstructionHandle ih : matches) {
					searcher.gotoHandle(ih);
					InstructionHandle groundInfoIh = searcher.searchNext(true, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(NEW.class, DUP.class, new ALOAD(0), GETFIELD.class));
					
					if (groundInfoIh != null) {
						groundInfoIh = groundInfoIh.getNext().getNext().getNext();
						
						GETFIELD groundInfoInstr = (GETFIELD) groundInfoIh.getInstruction();
						GameInfo.get.groundInfo.set(groundInfoInstr.getReferenceType(cpg).toString(), groundInfoInstr.getFieldName(cpg), groundInfoInstr.getSignature(cpg));					
						found = true;
						
						findGroundInfo.add(cg.getClassName() + "." + mg.getName(), groundInfoIh.getPosition());
						
						
						InstructionHandle groundBytesIh = searcher.searchNext(true, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(INVOKESPECIAL.class, PUTFIELD.class));
						
						if (groundBytesIh != null
								&& (groundBytesIh = searcher.searchPrev(true, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(ICONST.class, ALOAD.class, GETFIELD.class))) != null) {
							
							groundBytesIh = groundBytesIh.getNext().getNext();
							
							GETFIELD groundBytesInstr = (GETFIELD) groundBytesIh.getInstruction();
							GameInfo.get.groundBytes.set(groundBytesInstr.getReferenceType(cpg).toString(), groundBytesInstr.getFieldName(cpg), groundBytesInstr.getSignature(cpg));					
							found = true;
							
							findGroundBytes.add(cg.getClassName() + "." + mg.getName(), groundBytesIh.getPosition());
						}
					}
					
					found = true;
				}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findBaseInfo(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(GameInfo.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(14),
						LSHR.class,
						new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(16383L),
						LAND.class,
						L2I.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(28),
						LSHR.class,
						new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(3L),
						LAND.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						//InstructionHandle fieldIh = searcher.getNext(true, GETFIELD.class);
						InstructionHandle fieldIh = searcher.searchNext(true, CompareType.Opcode, new PI_GETFIELD(cpg));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							GameInfo.get.baseInfo.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findBaseInfo.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());						
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findObjectDefLoader(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(GameInfo.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustTypeString(false, ObjectDefLoader.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih.getNext();
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							GameInfo.get.objectDefLoader.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findObjectDefLoader.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());						
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			GameInfo.get.setInternalName(cg.getClassName());

			findBaseInfo(cg);
		}
	}
	
	public void analyze_ObjectDefLoader(ClassGen cg) {
		if (GameInfo.get.getInternalName() != null) {
			if (ObjectDefLoader.get.getInternalName() == null) {
				System.out.println("ERROR: GameInfoAnalyzer: ObjectDefLoader internal class name is null!");
				//return;
			} else
				findObjectDefLoader(cg);
		}
	}
}
