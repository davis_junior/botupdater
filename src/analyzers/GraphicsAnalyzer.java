package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import searcher.Comparator.CompareType;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Character;
import data.GameInfo;
import data.Graphics;
import data.GroundBytes;
import data.GroundInfo;
import data.Player;

public class GraphicsAnalyzer extends Analyzer {
	
	public static GraphicsAnalyzer get = new GraphicsAnalyzer();

	CountInfoItem findBytes = new CountInfoItem("findBytes");
	
	public GraphicsAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findBytes);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean foundGraphicsDevice = false;
		boolean foundDisplayMode = false;
		
		for (Field field : cg.getFields()) {
			if (field.getType().equals(Type.getType(java.awt.GraphicsDevice.class))) {
				foundGraphicsDevice = true;
				Graphics.get.graphicsDevice.set(cg.getClassName(), field.getName(), field.getSignature());	
			} else if (field.getType().equals(Type.getType(java.awt.DisplayMode.class))) {
				foundDisplayMode = true;
				Graphics.get.displayMode.set(cg.getClassName(), field.getName(), field.getSignature());	
			}
		}
	
		if (foundGraphicsDevice && foundDisplayMode)
			found = true;
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Graphics.get.setInternalName(cg.getClassName());
		}
	}

}
