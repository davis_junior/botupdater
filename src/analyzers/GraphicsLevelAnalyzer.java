package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.IRETURN;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.GraphicsLevel;
import data.AbstractGraphicsSetting;
import data.Player;

public class GraphicsLevelAnalyzer extends Analyzer {
	
	public static GraphicsLevelAnalyzer get = new GraphicsLevelAnalyzer();

	//CountInfoItem findX = new CountInfoItem("findX");
	//CountInfoItem findY = new CountInfoItem("findY");
	
	public GraphicsLevelAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findX, findY);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(AbstractGraphicsSetting.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			if (mg.getReturnType().equals(Type.BOOLEAN))
				return false;
		}
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] exclusionPattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new ICONST(2), new ICONST(3), new ICONST(4)),
					IRETURN.class
			);
			
			List<CompareInstruction[]> exclusionPatterns1 = new ArrayList<>();
			exclusionPatterns1.add(exclusionPattern1);
			
			InstructionHandle[][] exclusionMatches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, exclusionPatterns1);
			
			if (exclusionMatches1 != null)
				return false;
			
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new ICONST(4),
					IfInstruction.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()).adjustFieldName(AbstractGraphicsSetting.get.value.getInternalName())
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				//for (InstructionHandle ih : matches1[0]) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (AbstractGraphicsSetting.get.getInternalName() == null) {
			System.out.println("ERROR: GraphicsLevelAnalyzer: GraphicsSetting internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			GraphicsLevel.get.setInternalName(cg.getClassName());
		}
	}
}
