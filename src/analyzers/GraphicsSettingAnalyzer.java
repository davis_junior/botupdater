package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC_W;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.InstructionObject;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Character;
import data.FieldData;
import data.GameInfo;
import data.Graphics;
import data.AbstractGraphicsSetting;
import data.GroundBytes;
import data.GroundInfo;
import data.MessageData;
import data.Player;

public class GraphicsSettingAnalyzer extends Analyzer {
	
	public static GraphicsSettingAnalyzer get = new GraphicsSettingAnalyzer();

	CountInfoItem findValue = new CountInfoItem("findValue");
	
	public GraphicsSettingAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findValue);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		if (!cg.isAbstract())
			return false;
		
		boolean found = false;
		
		int intCount = 0;
		int objectCount = 0;
		FieldData tempValueData = FieldData.createTemp("value");
		
		for (Field field : cg.getFields()) {
			if (field.isStatic())
				continue;
			
			if (!field.isProtected())
				return false;
			
			if (field.getType().equals(Type.INT)) {
				tempValueData.set(cg.getClassName(), field.getName(), field.getSignature());
				intCount++;
			} else if (field.getType() instanceof ReferenceType) {
				//ReferenceType refType = (ReferenceType) field.getType();
				objectCount++;
			} else
				return false;
		}
		
		if (intCount == 1 && objectCount == 1) {
			AbstractGraphicsSetting.get.value.setData(tempValueData);
			found = true;
			findValue.add(cg.getClassName() + ".<FIELDS>", -1);
			
			// find multiplier
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.getName().equals("<init>"))
					continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				//if (mg.getArgumentTypes().length == 0) {
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_PUTFIELD(cpg).adjustClassName(AbstractGraphicsSetting.get.getInternalName()).adjustType(Type.INT)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						
						InstructionHandle ldcIh = searcher.searchPrev(false, CompareType.Opcode, new SynonymInstruction(LDC.class, LDC_W.class));
						if (ldcIh != null) {
							LDC instr = (LDC) ldcIh.getInstruction();
							int multi = MultiplierFinder.findInverse((int)instr.getValue(cpg));
							AbstractGraphicsSetting.get.value.set(multi);
						}
					}
				}
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			AbstractGraphicsSetting.get.setInternalName(cg.getClassName());
		}
	}

}
