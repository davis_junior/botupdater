package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.Repository;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC_W;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.InstructionObject;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AnimatedObject;
import data.Animator;
import data.Character;
import data.FieldData;
import data.GameInfo;
import data.Graphics;
import data.GraphicsCpuUsage;
import data.GraphicsLevel;
import data.AbstractGraphicsSetting;
import data.GraphicsSettingsContainer;
import data.GroundBytes;
import data.GroundInfo;
import data.Interactable;
import data.MessageData;
import data.Player;

public class GraphicsSettingsContainerAnalyzer extends Analyzer {
	
	public static GraphicsSettingsContainerAnalyzer get = new GraphicsSettingsContainerAnalyzer();

	CountInfoItem findGraphicsLevel = new CountInfoItem("findGraphicsLevel");
	
	public GraphicsSettingsContainerAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findGraphicsLevel);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		int graphicsSettingCount = 0;
		
		for (Field field : cg.getFields()) {
			if (field.isStatic())
				continue;
			
			if (field.getType() instanceof ReferenceType) {
				if (field.getType().toString().contains("["))
					continue;
				
				try {
					for (JavaClass jc : Repository.getSuperClasses(field.getType().toString())) {
						if (jc.getClassName().equals(AbstractGraphicsSetting.get.getInternalName())) {
							graphicsSettingCount++;
							break;
						}
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (graphicsSettingCount > 0) {
			found = true;
		}
		
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		boolean found = false;
		
		if (cg.getClassName().equals(GraphicsSettingsContainer.get.getInternalName())) {
			for (Field f : cg.getFields()) {
				if (f.isStatic())
					continue;
				
				if (f.getType().getSignature().equals("L" + GraphicsLevel.get.getInternalName() + ";")) {
					GraphicsSettingsContainer.get.level.set(cg.getClassName(), f.getName(), f.getSignature());
				}
				
				if (f.getType().getSignature().equals("L" + GraphicsCpuUsage.get.getInternalName() + ";")) {
					GraphicsSettingsContainer.get.cpuUsage.set(cg.getClassName(), f.getName(), f.getSignature());
				}
			}
		}
		
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (AbstractGraphicsSetting.get.getInternalName() == null) {
			System.out.println("ERROR: GraphicsSettingsContainerAnalyzer: GraphicsSetting internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			GraphicsSettingsContainer.get.setInternalName(cg.getClassName());
		}
	}
	
	public void analyze_pass3(ClassGen cg) {
		if (GraphicsLevel.get.getInternalName() == null) {
			System.out.println("ERROR: GraphicsSettingsContainerAnalyzer: GraphicsLevel internal class name is null!");
			return;
		}
		
		if (GraphicsCpuUsage.get.getInternalName() == null) {
			System.out.println("ERROR: GraphicsSettingsContainerAnalyzer: GraphicsCPUUsage internal class name is null!");
			return;
		}
		
		findFields(cg);
	}

}
