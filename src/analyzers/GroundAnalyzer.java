package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.INSTANCEOF;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractBoundary;
import data.AbstractFloorObject;
import data.AbstractWallObject;
import data.AnimableNode;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.Ground;
import data.GroundBytes;
import data.GroundInfo;
import data.Model;
import data.Npc;
import data.Player;

public class GroundAnalyzer extends Analyzer {
	
	public static GroundAnalyzer get = new GroundAnalyzer();

	//CountInfoItem findTitle = new CountInfoItem("findTitle");
	//CountInfoItem findNpcDefinition = new CountInfoItem("findNpcDefinition");
	
	public GroundAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findTitle, findNpcDefinition);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (("L" + cg.getClassName() + ";").equals(GroundInfo.get.groundArray.getSignature().replace("[", "")))
			found = true;
		
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Ground.get.getInternalName())) {
			for (Field f : cg.getFields()) {
				if (f.getType().getSignature().equals("L" + AnimableNode.get.getInternalName() + ";")) {
					Ground.get.animableList.set(cg.getClassName(), f.getName(), f.getSignature());
					//findAnimableList.add(cg.getClassName() + "." + m.getName(), ih.getPosition());
					
					found = true;
				}
			}
		}
	
		return found;
	}
	
	private boolean findFields_floorDecoration(ClassGen cg) {
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Ground.get.getInternalName())) {
			for (Field f : cg.getFields()) {
				if (f.getType().toString().equals(AbstractFloorObject.get.getInternalName())) {
					Ground.get.floorDecoration.set(cg.getClassName(), f.getName(), f.getSignature());
					//findFloorDecoration.add(cg.getClassName() + "." + m.getName(), ih.getPosition());
					
					found = true;
				}
			}
		}
	
		return found;
	}
	
	private boolean findBoundaries(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Ground.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.isStatic())
					continue;
				
				Type[] argTypes = mg.getArgumentTypes();
				if (argTypes == null || argTypes.length == 0)
					continue;
				else {
					boolean hasGroundArg = false;
					for (Type argType : argTypes) {
						if (argType.toString().equals(Ground.get.getInternalName() + "[][][]")) {
							hasGroundArg = true;
							break;
						}
					}
					
					if (!hasGroundArg)
						continue;
				}
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Ground.get.getInternalName()).adjustTypeString(false, AbstractBoundary.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(16),
						ISHR.class
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(24),
						ISHR.class
				);
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(255),
						IAND.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				patterns.add(pattern3);
				patterns.add(pattern4);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					List<FieldData> fields = new ArrayList<>();
					
					for (InstructionHandle ih : matches[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							FieldData field = FieldData.createTemp();
							field.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							
							boolean isDuplicate = false;
							for (FieldData otherField : fields) {
								if (field.compareData(otherField)) {
									isDuplicate = true;
									break;
								}
							}
							
							if (isDuplicate)
								continue;
							else
								fields.add(field);						
						}
					}
					
					if (fields.size() == 2) {
						Ground.get.boundary1.setData(fields.get(0));
						Ground.get.boundary2.setData(fields.get(1));
						found = true;
						
						//findBoundaries.add(cg.getClassName() + "." + m.getName(), ih.getPosition());	
					}
				}
			}
		//}
	
		return found;
	}
	
	// similar to findBoundaries(...)
	private boolean findWallObjects(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Ground.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.isStatic())
					continue;
				
				Type[] argTypes = mg.getArgumentTypes();
				if (argTypes == null || argTypes.length == 0)
					continue;
				else {
					boolean hasGroundArg = false;
					for (Type argType : argTypes) {
						if (argType.toString().equals(Ground.get.getInternalName() + "[][][]")) {
							hasGroundArg = true;
							break;
						}
					}
					
					if (!hasGroundArg)
						continue;
				}
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Ground.get.getInternalName()).adjustTypeString(false, AbstractWallObject.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(Ground.get.getInternalName()),
						INSTANCEOF.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()
						&& matches[1].length == 5) {
					
					List<FieldData> fields = new ArrayList<>();
					
					for (InstructionHandle ih : matches[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							FieldData field = FieldData.createTemp();
							field.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							
							boolean isDuplicate = false;
							for (FieldData otherField : fields) {
								if (field.compareData(otherField)) {
									isDuplicate = true;
									break;
								}
							}
							
							if (isDuplicate)
								continue;
							else
								fields.add(field);						
						}
					}
					
					if (fields.size() == 2) {
						Ground.get.wallDecoration1.setData(fields.get(0));
						Ground.get.wallDecoration2.setData(fields.get(1));
						found = true;
						
						//findWallDecorations.add(cg.getClassName() + "." + m.getName(), ih.getPosition());	
					}
				}
			}
		//}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (GroundInfo.get.getInternalName() == null) {
			System.out.println("ERROR: GroundAnalyzer: GroundInfo internal class name is null!");
			return;
		}
		
		if (GroundInfo.get.groundArray.getInternalName() == null) {
			System.out.println("ERROR: GroundAnalyzer: GroundInfo groundArray internal field name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			Ground.get.setInternalName(cg.getClassName());
			
			if (AnimableNode.get.getInternalName() == null) {
				System.out.println("ERROR: GroundAnalyzer: AnimableNode internal class name is null!");
				//return;
			} else
				findFields(cg);
		}
	}
	
	public void analyze_pass4(ClassGen cg) {
		if (AbstractFloorObject.get.getInternalName() == null) {
			System.out.println("ERROR: GroundAnalyzer: AbstractFloorObject internal class name is null!");
			//return;
		} else
			findFields_floorDecoration(cg);
	}
	
	public void analyze_statics(ClassGen cg) {
		if (Ground.get.getInternalName() == null) {
			System.out.println("ERROR: GroundAnalyzer: Ground internal class name is null!");
			return;
		}
		
		if (AbstractBoundary.get.getInternalName() == null) {
			System.out.println("ERROR: GroundAnalyzer: AbstractBoundary internal class name is null!");
			//return;
		} else
			findBoundaries(cg);
		
		
		if (AbstractWallObject.get.getInternalName() == null) {
			System.out.println("ERROR: GroundAnalyzer: AbstractWallObject internal class name is null!");
			//return;
		} else
			findWallObjects(cg);
	}

}
