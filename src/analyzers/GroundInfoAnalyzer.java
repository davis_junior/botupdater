package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IDIV;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Character;
import data.GameInfo;
import data.GroundInfo;
import data.Player;

public class GroundInfoAnalyzer extends Analyzer {
	
	public static GroundInfoAnalyzer get = new GroundInfoAnalyzer();

	CountInfoItem findGroundArray = new CountInfoItem("findGroundArray");
	CountInfoItem findTileData = new CountInfoItem("findTileData");
	
	public GroundInfoAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findGroundArray, findTileData);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_SIPUSH(cpg).adjustValue(255)
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(65535L)
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(4294901760L)
			);
			
			final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(281470681743360L)
			);
			
			final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(-281474976710656L)
			);
			
			List<CompareInstruction[]> patterns = new ArrayList<>();
			patterns.add(pattern1);
			patterns.add(pattern2);
			patterns.add(pattern3);
			patterns.add(pattern4);
			patterns.add(pattern5);
			
			InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
			
			if (matches != null && matches.length == patterns.size()) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findGroundArray(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(GroundInfo.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						IMUL.class,
						new PI_BIPUSH(cpg).adjustValue(16)
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(7),
						ISUB.class,
						ISHL.class,
						IDIV.class,
						I2S.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					InstructionHandle fieldIh = searcher.searchFromStart(true, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(new ALOAD(0), GETFIELD.class));
					
					if (fieldIh != null) {
						GETFIELD instr = (GETFIELD) fieldIh.getNext().getInstruction();
						GroundInfo.get.groundArray.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
						found = true;
						
						findGroundArray.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());						
					}
					
					/*for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(true, GETFIELD.class);
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							GameInfo.get.baseInfo.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findBaseInfo.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());						
						}
					}*/
				}
			}
		}
	
		return found;
	}
	
	private boolean findTileData(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(GroundInfo.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustTypeString(true, "[][][]"),
						new PI_PUTFIELD(cpg).adjustTypeString(true, "[][][]")
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustTypeString(true, "[]"),
						new PI_PUTFIELD(cpg).adjustTypeString(true, "[]")
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				List<CompareInstruction[]> patterns2 = new ArrayList<>();
				patterns2.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				InstructionHandle[][] matches2 = null;
				
				if (matches1 != null)
					matches2 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns2);
				
				// TODO: when containsFlag is setup for signature, remove this code and fix the pattern
				// remove multi-dim arrays from matches2
				if (matches2 != null) {
					List<InstructionHandle> matches2fixed = new ArrayList<InstructionHandle>();
					
					for (InstructionHandle ih : matches2[0]) {
						GETFIELD getIh = (GETFIELD) ih.getNext().getNext().getInstruction();
						if (!getIh.getType(cpg).toString().contains("[][][]"))
							matches2fixed.add(ih);
					}
					
					matches2[0] = matches2fixed.toArray(new InstructionHandle[0]);
				}
				
				
				if (matches1 != null && matches2 != null) {
					for (InstructionHandle ih : matches2[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(true, PUTFIELD.class);
						
						if (fieldIh != null) {
							PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
							GroundInfo.get.tileData.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findTileData.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());						
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			GroundInfo.get.setInternalName(cg.getClassName());

			findGroundArray(cg);
			findTileData(cg);
		}
	}

}
