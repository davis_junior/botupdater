package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;

import data.HardReference;
import data.Model;
import data.Reference;
import data.SoftReference;

public class HardReferenceAnalyzer extends Analyzer {
	
	public static HardReferenceAnalyzer get = new HardReferenceAnalyzer();

	@Override
	protected boolean verifyClass(ClassGen cg) {
		//ConstantPoolGen cpg = cg.getConstantPool();

		if (!cg.getSuperclassName().equals(Reference.get.getInternalName()))
			return false;
		
		if (cg.getClassName().equals(SoftReference.get.getInternalName()))
			return false;
		
		boolean found = false;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic() && f.getType().getSignature().equals("Ljava/lang/Object;")) {
				HardReference.get.hardReference.set(cg.getClassName(), f.getName(), f.getSignature());
				found = true;
			}
		}
		
		return found;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (SoftReference.get.getInternalName() == null) {
			System.out.println("ERROR: HardReferenceAnalyzer: SoftReference internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			HardReference.get.setInternalName(cg.getClassName());
		}
	}
}
