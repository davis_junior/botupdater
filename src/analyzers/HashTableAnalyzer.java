package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IF_ACMPNE;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import report.Report;
import searcher.Comparator.CompareType;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.HashTable;
import data.Model;
import data.Node;
import data.Npc;
import data.Player;

public class HashTableAnalyzer extends Analyzer {
	
	public static HashTableAnalyzer get = new HashTableAnalyzer();

	CountInfoItem findBuckets = new CountInfoItem("findBuckets");
	
	public HashTableAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findBuckets);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(java.lang.Object.class.getName()))
			return false;
		
		boolean foundInterface = false;
		for (String iface : cg.getInterfaceNames()) {
			if (iface.equals("java.lang.Iterable"))
				foundInterface = true;
		}
		
		if (!foundInterface)
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				if (f.getType().getSignature().equals("[L" + Node.get.getInternalName() + ";")) {
					HashTable.get.buckets.set(cg.getClassName(), f.getName(), f.getSignature());
					
					found = true;
				}
			}
		}
	
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (Node.get.getInternalName() == null) {
			System.out.println("ERROR: HashTableAnalyzer: Node internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			HashTable.get.setInternalName(cg.getClassName());
			
			//findFields(cg);
		}
	}

}
