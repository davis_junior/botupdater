package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LOR;
import org.apache.bcel.generic.LSHL;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.IdComposite_AnimatedObject;
import data.Model;
import data.ObjectDefinition;

public class IdComposite_AnimatedObjectAnalyzer extends Analyzer {
	
	public static IdComposite_AnimatedObjectAnalyzer get = new IdComposite_AnimatedObjectAnalyzer();

	CountInfoItem findId = new CountInfoItem("findId");
	
	public IdComposite_AnimatedObjectAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findId);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		return false;
	}
	
	protected boolean findClass(ClassGen cg) {
		
		if (!cg.getClassName().equals(ObjectDefinition.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (!mg.getReturnType().toString().equals(Model.get.getInternalName()))
				continue;
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(32),
					LSHL.class,
					LOR.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				for (InstructionHandle ih : matches1[0]) {
					searcher.gotoHandle(ih);
					InstructionHandle nextStore = searcher.searchNext(true, CompareType.Opcode, SynonymInstruction.Store_OR_Put);
					
					if (nextStore != null) {
						InstructionHandle fieldIh = searcher.searchPrev(true, false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustType(Type.LONG));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							IdComposite_AnimatedObject.get.id.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findId.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoHandle(nextStore);
							IdComposite_AnimatedObject.get.id.set(searcher.findPrevGetMultiplier(false, false, IdComposite_AnimatedObject.get.id, cpg));
							
							
							IdComposite_AnimatedObject.get.setInternalName(IdComposite_AnimatedObject.get.id.getInternalClassName());
						}
					}
					
					found = true;
				}
			}
			//}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (ObjectDefinition.get.getInternalName() == null) {
			System.out.println("ERROR: IdComposite_AnimatedObjectAnalyzer: ObjectDefinition internal class name is null!");
			return;
		}
		
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: IdComposite_AnimatedObjectAnalyzer: Model internal class name is null!");
			return;
		}
		
		/*if (verifyClass(cg)) {
			IdComposite_AnimatedObject.get.setInternalName(cg.getClassName());

			findFields(cg);
		}*/
		
		findClass(cg);
	}

}
