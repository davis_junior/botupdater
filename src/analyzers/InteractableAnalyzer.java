package analyzers;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LDC2_W;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Interactable;
import data.Player;

public class InteractableAnalyzer extends Analyzer {
	
	public static InteractableAnalyzer get = new InteractableAnalyzer();

	CountInfoItem findPlane = new CountInfoItem("findPlane");
	
	public InteractableAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findPlane);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			// the pattern used following this is also in RSAnimable; however, Math.max/min is not in Interactable
			if (searcher.searchFromStart(false, CompareType.Opcode, 
					new SynonymInstruction( new PI_INVOKESTATIC(cpg).adjustMethodName("max"), new PI_INVOKESTATIC(cpg).adjustMethodName("min"))
					) != null)
				continue;
			
			searcher.gotoStart();
			
			// the following patterns (3 total LDC2_W checks) are also in RSAnimable
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(48L)
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(65535L)
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(16L)
			);
			
			if (searcher.searchNext(true, CompareType.Opcode, pattern1) != null
					&& searcher.searchNext(true, CompareType.Opcode, pattern2) != null
					&& searcher.searchNext(true, CompareType.Opcode, pattern3) != null) {
				
				found = true;
				
				findPlane(cg, cpg, mg, il);
			}
		}
	
		return found;
	}
	
	public boolean findPlane(ClassGen cg, ConstantPoolGen cpg, MethodGen mg, InstructionList il) {
		boolean found = false;
		
		Searcher searcher = new Searcher(il);
		InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
				new PI_GETFIELD(cpg).adjustType(Type.getType(long[][][].class)),
				new ALOAD(0),
				GETFIELD.class
				));
		
		if (matches != null) {
			//if (matches.length == 2) {
			for (InstructionHandle ih : matches) {
				searcher.gotoHandle(ih);

				GETFIELD instr3 = (GETFIELD) ih.getNext().getNext().getInstruction();
				
				Interactable.get.plane.set(instr3.getReferenceType(cpg).toString(), instr3.getFieldName(cpg), instr3.getSignature(cpg));
				findPlane.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
			}
			//}
		}
		
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Interactable.get.setInternalName(cg.getClassName());

			//findPlane(cg);
		}
	}

}
