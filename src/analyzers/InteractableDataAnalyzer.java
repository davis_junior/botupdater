package analyzers;

import org.apache.bcel.classfile.DescendingVisitor;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import util.DependencyEmitter;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.EntityNode;
import data.InteractableData;
import data.Player;

public class InteractableDataAnalyzer extends Analyzer {
	
	public static InteractableDataAnalyzer get = new InteractableDataAnalyzer();

	CountInfoItem findCenterLocation = new CountInfoItem("findCenterLocation");
	
	public InteractableDataAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findCenterLocation);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		return false;
	}
	
	private boolean findClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (!BotFinder.extendsDefault(cg))
		//	return false;
		
	    DependencyEmitter visitor = new DependencyEmitter(cg.getJavaClass());
	    DescendingVisitor classWalker = new DescendingVisitor(cg.getJavaClass(), visitor);
	    classWalker.visit();
	    
	    //visitor.printDependencies();
	    
	    for (String dependency : visitor.getDependencies()) {
	    	if (dependency.equals("jaggl/OpenGL"))
	    		return false;
	    }
		
	    for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
			InstructionHandle[] matches = searcher.searchAllFromStart(true, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
					new PI_SIPUSH(cpg).adjustValue(256),
					IMUL.class,
					new PI_SIPUSH(cpg).adjustValue(252),
					IADD.class
					));

			if (matches != null) {
				//for (InstructionHandle ih : matches) {
				InstructionHandle ih = null;
				if ((ih = searcher.getNext(true, GETFIELD.class)) != null) {
					GETFIELD instr = (GETFIELD) ih.getInstruction();

					if (verify1(BotUpdater.getClass(instr.getReferenceType(cpg).toString()))) {
						InteractableData.get.setInternalName(instr.getReferenceType(cpg).toString());
						InteractableData.get.centerLocation.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findCenterLocation.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
						found = true;
					}
				}
				//}
			}
			//}
		}
	
		return found;
	}
	
	public boolean verify1(ClassGen cg) {
		ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean foundFCONST = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			if (searcher.getNext(false, FCONST.class) != null) {
				foundFCONST = true;
				break;
			}
		}
		
		if (!foundFCONST)
			return true;
		
		return false;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (findClass(cg)) {
			
		}
	}

}
