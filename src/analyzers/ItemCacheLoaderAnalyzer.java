package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import searcher.Searcher;
import data.Cache;
import data.ItemCacheLoader;
import data.ItemDefinition;
import data.Model;
import data.ObjectComposite;
import data.ObjectCacheLoader;
import data.ObjectDefinition;

public class ItemCacheLoaderAnalyzer extends Analyzer {
	
	public static ItemCacheLoaderAnalyzer get = new ItemCacheLoaderAnalyzer();

	CountInfoItem findId = new CountInfoItem("findId");
	CountInfoItem findModelCache = new CountInfoItem("findModelCache");
	
	public ItemCacheLoaderAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findId, findModelCache);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.isAbstract())
			return false;
		
		if (!cg.getSuperclassName().equals(java.lang.Object.class.getName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		int cacheFieldCount = 0;
		int stringArrayFieldCount = 0;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				if (f.getType().getSignature().equals("L" + Cache.get.getInternalName() + ";"))
					cacheFieldCount++;
				else if(f.getType().getSignature().equals("[Ljava/lang/String;"))
					stringArrayFieldCount++;
			}
		}
		
		if (cacheFieldCount == 1 && stringArrayFieldCount == 2) {
			found = true;
		}
	
		return found;
	}
	
	// same as modelCache except the method is in ObjectDefinition and exclusion patterns are inclusion patterns
	private boolean findModelCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ItemCacheLoader.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustTypeString(false, Cache.get.getInternalName()).adjustClassName(ItemCacheLoader.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						InstructionHandle fieldIh = ih.getNext();
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						ItemCacheLoader.get.modelCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findModelCache.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
						found = true;
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Cache.get.getInternalName() == null) {
			System.out.println("ERROR: ItemCacheLoaderAnalyzer: Cache internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			ItemCacheLoader.get.setInternalName(cg.getClassName());
			
			findModelCache(cg);
		}
	}

}
