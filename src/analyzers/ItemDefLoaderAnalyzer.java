package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.SynonymInstruction;
import searcher.pi.PI_CHECKCAST;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import searcher.Searcher;
import data.AbstractDefinition;
import data.Cache;
import data.DefLoader;
import data.ItemCacheLoader;
import data.ItemDefLoader;
import data.Model;
import data.ObjectComposite;
import data.ObjectCacheLoader;
import data.ObjectDefLoader;
import data.ObjectDefinition;

public class ItemDefLoaderAnalyzer extends Analyzer {
	
	public static ItemDefLoaderAnalyzer get = new ItemDefLoaderAnalyzer();
	
	public ItemDefLoaderAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add();
	}
	
	// about the same as verifyClass(...) from ObjectDefLoaderAnalyzer
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(DefLoader.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_CHECKCAST(cpg).adjustTypeString(false, ItemCacheLoader.get.getInternalName())
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKEVIRTUAL(cpg).adjustClassName(ItemCacheLoader.get.getInternalName())
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				//for (InstructionHandle ih : matches1[0]) {
					found = true;
				//}
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (DefLoader.get.getInternalName() == null) {
			System.out.println("ERROR: ItemDefLoader: DefLoader internal class name is null!");
			return;
		}
		
		if (ItemCacheLoader.get.getInternalName() == null) {
			System.out.println("ERROR: ItemDefLoader: ItemCacheLoader internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			ItemDefLoader.get.setInternalName(cg.getClassName());
		}
	}

}
