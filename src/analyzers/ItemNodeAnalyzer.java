package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ARRAYLENGTH;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC_W;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.LSTORE;
import org.apache.bcel.generic.LUSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.ItemNode;
import data.ItemTable;
import data.Model;
import data.Node;
import data.Player;

public class ItemNodeAnalyzer extends Analyzer {
	
	public static ItemNodeAnalyzer get = new ItemNodeAnalyzer();

	CountInfoItem findId = new CountInfoItem("findId");
	CountInfoItem findStackSize = new CountInfoItem("findStackSize");
	
	public ItemNodeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findId, findStackSize);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(Node.get.getInternalName()))
			return false;
		
		Method[] methods = cg.getMethods();
		int nonStaticMethodCount = 0;
		
		for (Method m : methods) {
			if (!m.isStatic())
				nonStaticMethodCount++;
		}
		
		if (nonStaticMethodCount != 1)
			return false;
		
		Field[] fields = cg.getFields();
		int nonStaticFieldCount = 0;
		int nonStaticIntCount = 0;
		
		for (Field f : fields) {
			if (!f.isStatic()) {
				nonStaticFieldCount++;
				
				if (f.getType().equals(Type.INT))
					nonStaticIntCount++;
			}
		}
		
		if (nonStaticFieldCount != 2)
			return false;
		
		if (nonStaticFieldCount != nonStaticIntCount)
			return false;
		
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (!mg.getName().equals("<init>"))
				continue;
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					IMUL.class,
					new PI_PUTFIELD(cpg).adjustClassName(cg.getClassName())
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				if (matches1[0].length == 2) {
					
					found = true;
					
					for (int i = 0; i < matches1[0].length; i ++) {
						InstructionHandle fieldIh = matches1[0][i].getNext();
						
						if (fieldIh != null) {
							PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
							
							if (i == 0) {
								ItemNode.get.id.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));	
								
								findId.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
								
								//searcher.gotoStart();
								//ItemNode.get.id.set(searcher.findNextPutMultiplier(false, false, ItemNode.get.id, cpg));
								
								searcher.gotoHandle(fieldIh);
								InstructionHandle prevLdc = searcher.searchPrev(false, CompareType.Opcode, new SynonymInstruction(LDC.class, LDC_W.class));
								if (prevLdc != null) {
									LDC ldcInstr = (LDC) prevLdc.getInstruction();
									ItemNode.get.id.set(MultiplierFinder.findInverse((int)ldcInstr.getValue(cpg)));
								}
							} else {
								ItemNode.get.stackSize.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
								
								findStackSize.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
								
								//searcher.gotoStart();
								//ItemNode.get.stackSize.set(searcher.findNextPutMultiplier(false, false, ItemNode.get.stackSize, cpg));
								
								searcher.gotoHandle(fieldIh);
								InstructionHandle prevLdc = searcher.searchPrev(false, CompareType.Opcode, new SynonymInstruction(LDC.class, LDC_W.class));
								if (prevLdc != null) {
									LDC ldcInstr = (LDC) prevLdc.getInstruction();
									ItemNode.get.stackSize.set(MultiplierFinder.findInverse((int)ldcInstr.getValue(cpg)));
								}
							}
						}
					}
				}
			}
		}
		
		return found;
	}
	
	/*private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ItemTable.get.getInternalName())) {
			for (Method m : cg.getMethods()) {
				if (m.isStatic())
					continue;
				
				MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
				
				//if (mg.getArgumentTypes().length != 0)
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(ItemTable.get.getInternalName()).adjustSignature("[I"),
						ARRAYLENGTH.class
				};
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							ItemTable.get.itemIds.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findItemIds.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
							
							
							for (Field f : cg.getFields()) {
								if (f.getSignature().equals("[I") && !f.getName().equals(ItemTable.get.itemIds.getInternalName())) {
									ItemTable.get.itemStackSizes.set(cg.getClassName(), f.getName(), f.getSignature());					
									findItemStackSizes.add("<FIELDS>.<FIELDS>", -1);
								}
							}
						}
					}
				}
			}
		}
	
		return found;
	}*/
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Node.get.getInternalName() == null) {
			System.out.println("ERROR: ItemNodeAnalyzer: Node internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			ItemNode.get.setInternalName(cg.getClassName());

			//findFields(cg);
		}
	}

}
