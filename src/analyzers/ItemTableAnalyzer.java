package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ARRAYLENGTH;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.LSTORE;
import org.apache.bcel.generic.LUSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.ItemTable;
import data.Player;

public class ItemTableAnalyzer extends Analyzer {
	
	public static ItemTableAnalyzer get = new ItemTableAnalyzer();

	CountInfoItem findItemIds = new CountInfoItem("findItemIds");
	CountInfoItem findItemStackSizes = new CountInfoItem("findItemStackSizes");
	
	public ItemTableAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findItemIds, findItemStackSizes);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			if (!mg.getReturnType().equals(Type.LONG))
				continue;
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustValue(-1L),
					LSTORE.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(8),
					LUSHR.class
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustValue(0xFFL),
					LAND.class
			);
			
			// Also:
			// >> 8
			// >> 24
			// >> 16
			// >> 8
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			patterns1.add(pattern3);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				if (matches1[0].length == 1 && matches1[1].length == 8 && matches1[2].length == 8)
					found = true;
			}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ItemTable.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(ItemTable.get.getInternalName()).adjustSignature("[I"),
						ARRAYLENGTH.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							ItemTable.get.itemIds.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findItemIds.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							
							for (Field f : cg.getFields()) {
								if (f.isStatic())
									continue;
								
								if (f.getSignature().equals("[I") && !f.getName().equals(ItemTable.get.itemIds.getInternalName())) {
									ItemTable.get.itemStackSizes.set(cg.getClassName(), f.getName(), f.getSignature());					
									findItemStackSizes.add("<FIELDS>.<FIELDS>", -1);
								}
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			ItemTable.get.setInternalName(cg.getClassName());

			findFields(cg);
		}
	}

}
