package analyzers;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.DUP;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.INVOKESPECIAL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.NEW;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESPECIAL;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Location;
import data.Player;

public class LocationAnalyzer extends Analyzer {
	
	public static LocationAnalyzer get = new LocationAnalyzer();

	CountInfoItem findX = new CountInfoItem("findX");
	CountInfoItem findY = new CountInfoItem("findY");
	CountInfoItem findHeight = new CountInfoItem("findHeight");
	
	public LocationAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findX, findY, findHeight);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (!mg.getName().equals(Constants.STATIC_INITIALIZER_NAME))
				continue;
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
			InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
					NEW.class,
					DUP.class,
					new FCONST(0.0F),
					new FCONST(0.0F),
					new FCONST(0.0F),
					INVOKESPECIAL.class
					));

			if (matches != null) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Location.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!mg.getName().equals(Constants.CONSTRUCTOR_NAME))
					continue;
				
				if (mg.getArgumentTypes().length != 3)
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						PUTFIELD.class
						));

				if (matches != null) {
					if (matches.length == 3) {

						//for (InstructionHandle ih : matches) {
						
							PUTFIELD instr1 = (PUTFIELD) matches[0].getInstruction();
							PUTFIELD instr2 = (PUTFIELD) matches[1].getInstruction();
							PUTFIELD instr3 = (PUTFIELD) matches[2].getInstruction();

							Location.get.x.set(instr1.getReferenceType(cpg).toString(), instr1.getFieldName(cpg), instr1.getSignature(cpg));
							findX.add(cg.getClassName() + "." + mg.getName(), matches[0].getPosition());

							Location.get.height.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
							findHeight.add(cg.getClassName() + "." + mg.getName(), matches[1].getPosition());
							
							Location.get.y.set(instr3.getReferenceType(cpg).toString(), instr3.getFieldName(cpg), instr3.getSignature(cpg));
							findY.add(cg.getClassName() + "." + mg.getName(), matches[2].getPosition());
						//}
						
						found = true;
					}
				}
			}
		}
	
		return found;
	}
	
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Location.get.setInternalName(cg.getClassName());

			findFields(cg);
		}
	}

}
