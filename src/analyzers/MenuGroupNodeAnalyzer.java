package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.Type;

import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.FieldData;
import data.MenuGroupNode;
import data.NodeSub;
import data.NodeSubQueue;

public class MenuGroupNodeAnalyzer extends Analyzer {
	
	public static MenuGroupNodeAnalyzer get = new MenuGroupNodeAnalyzer();

	CountInfoItem findItems = new CountInfoItem("findItems");
	CountInfoItem findOptions = new CountInfoItem("findOptions");
	
	public MenuGroupNodeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findItems, findOptions);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();
		
		if (!cg.getSuperclassName().equals(NodeSub.get.getInternalName()))
			return false;
		
		boolean found = false;
		
		int foundNodeSubQueueCount = 0;
		int foundStringCount = 0;
		int foundIntCount = 0;
		
		FieldData tempItems = FieldData.createTemp("items");
		FieldData tempOptions = FieldData.createTemp("options");
		
		for (Field field : cg.getFields()) {
			if (field.isStatic())
				continue;
			
			if (field.getType().equals(Type.INT))
				foundIntCount++;
			
			if (field.isFinal()) {
				if (field.getType().toString().equals(NodeSubQueue.get.getInternalName())) {
					foundNodeSubQueueCount++;
					tempItems.set(cg.getClassName(), field.getName(), field.getSignature());
				} else if (field.getType().equals(Type.STRING)) {
					foundStringCount++;
					tempOptions.set(cg.getClassName(), field.getName(), field.getSignature());
				} 
			}
		}
		
		if (foundNodeSubQueueCount == 1 && foundStringCount == 1 && foundIntCount > 0) {
			MenuGroupNode.get.items.setData(tempItems);
			MenuGroupNode.get.options.setData(tempOptions);
			found = true;
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (NodeSub.get.getInternalName() == null) {
			System.out.println("ERROR: MenuGroupNodeAnalyzer: NodeSub internal class name is null!");
			return;
		}
		
		if (NodeSubQueue.get.getInternalName() == null) {
			System.out.println("ERROR: MenuGroupNodeAnalyzer: NodeSubQueue internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			MenuGroupNode.get.setInternalName(cg.getClassName());
		}
	}
}
