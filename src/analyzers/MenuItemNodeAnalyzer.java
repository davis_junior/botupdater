package analyzers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FieldInstruction;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IFEQ;
import org.apache.bcel.generic.IFNE;
import org.apache.bcel.generic.IF_ACMPEQ;
import org.apache.bcel.generic.IF_ICMPEQ;
import org.apache.bcel.generic.IF_ICMPNE;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_FieldInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.MenuItemNode;
import data.Model;
import data.NodeSub;
import data.Player;

public class MenuItemNodeAnalyzer extends Analyzer {
	
	public static MenuItemNodeAnalyzer get = new MenuItemNodeAnalyzer();

	CountInfoItem findOption = new CountInfoItem("findOption");
	CountInfoItem findAction = new CountInfoItem("findAction");
	
	public MenuItemNodeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findOption, findAction);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		if (!cg.getSuperclassName().equals(NodeSub.get.getInternalName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean returnTypeMismatch = false;
		int localMethodCount = 0;
		for (Method m : cg.getMethods()) {
			if (m.getName().equals(Constants.STATIC_INITIALIZER_NAME) || m.getName().equals(Constants.CONSTRUCTOR_NAME))
				continue;
			
			if (m.isStatic() || m.isAbstract())
				continue;
			
			localMethodCount++;
			
			if (!m.getReturnType().equals(Type.LONG)) {
				returnTypeMismatch = true;
				break;
			}
		}
		
		if (!returnTypeMismatch && localMethodCount > 0) {
			found = true;
		}
		
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(MenuItemNode.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(MenuItemNode.get.getInternalName()).adjustType(Type.STRING),
						new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.lang.String").adjustMethodName("length")
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_FieldInstruction<>(cpg).adjustClassName(MenuItemNode.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				
				final CompareInstruction[] rejectedPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_FieldInstruction<>(cpg).adjustType(Type.BOOLEAN)
				);
				
				List<CompareInstruction[]> rejectedPatterns1 = new ArrayList<>();
				rejectedPatterns1.add(rejectedPattern1);
				
				
				Searcher searcher = new Searcher(il);
				
				InstructionHandle[][] rejectedMatches1 = searcher.searchAllFromStart(false, false, CompareType.Opcode, rejectedPatterns1);
				
				if (rejectedMatches1 != null)
					continue;
				
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					
					Set<String> fieldNames = new HashSet<>();
					for (InstructionHandle ih : matches1[1]) {
						fieldNames.add(((FieldInstruction) ih.getInstruction()).getFieldName(cpg));
					}
					
					if (fieldNames.size() != 3)
						continue;
					
					
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle ifIh = searcher.getNext(false, IfInstruction.class);
						
						// continue loop if next if instr is (field == 0)
						// we're looking for (field > 0)
						if (ifIh == null || (ifIh.getInstruction() instanceof IFNE 
								|| ifIh.getInstruction() instanceof IFEQ
								|| ifIh.getInstruction() instanceof IF_ICMPNE
								|| ifIh.getInstruction() instanceof IF_ICMPEQ)) {
							
							fieldNames.remove(((FieldInstruction) ih.getInstruction()).getFieldName(cpg));
							continue;
						}
						
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							MenuItemNode.get.option.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findOption.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							fieldNames.remove(instr.getFieldName(cpg));
						}
					}
					
					if (fieldNames.size() == 1) {
						String fieldName = fieldNames.toArray(new String[0])[0];
						
						for (InstructionHandle ih : matches1[1]) {
							FieldInstruction instr = ((FieldInstruction) ih.getInstruction());
							
							if (instr.getFieldName(cpg).equals(fieldName)) {
								MenuItemNode.get.action.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								found = true;
								
								findAction.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
							}
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (NodeSub.get.getInternalName() == null) {
			System.out.println("ERROR: MenuItemNodeAnalyzer: NodeSub internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			MenuItemNode.get.setInternalName(cg.getClassName());
		}
	}
	
	public void analyze_pass2(ClassGen cg) {
		if (MenuItemNode.get.getInternalName() == null) {
			System.out.println("ERROR: MenuItemNodeAnalyzer: MenuItemNode internal class name is null!");
			return;
		}
		
		findFields(cg);
	}
}
