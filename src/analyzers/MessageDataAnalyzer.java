package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.MessageData;
import data.Model;
import data.Player;

public class MessageDataAnalyzer extends Analyzer {
	
	public static MessageDataAnalyzer get = new MessageDataAnalyzer();

	CountInfoItem findMessage = new CountInfoItem("findMessage");
	
	public MessageDataAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findMessage);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		return false;
	}
	
	// about same as Character.findMessageData()
	protected boolean findClass(ClassGen cg) {
		
		if (!cg.getClassName().equals(Character.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(Character.get.getInternalName()),
					ALOAD.class,
					new PI_PUTFIELD(cpg).adjustType(Type.STRING)
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				for (InstructionHandle ih : matches1[0]) {
					InstructionHandle getFieldIh = ih.getNext();
					InstructionHandle putFieldIh = ih.getNext().getNext().getNext();
					
					GETFIELD getField = (GETFIELD) getFieldIh.getInstruction();
					PUTFIELD putField = (PUTFIELD) putFieldIh.getInstruction();
					
					if (getField.getType(cpg).toString().equals(putField.getClassName(cpg))) {
						PUTFIELD instr = putField;
						MessageData.get.message.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
						found = true;
						
						findMessage.add(cg.getClassName() + "." + mg.getName(), putFieldIh.getPosition());
						
						MessageData.get.setInternalName(putField.getClassName(cpg));
					}
				}
			}
			//}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Character.get.getInternalName() == null) {
			System.out.println("ERROR: MessageDataAnalyzer: Character internal class name is null!");
			return;
		}
		
		/*if (verifyClass(cg)) {
			MessageData.get.setInternalName(cg.getClassName());
		}*/
		
		findClass(cg);
	}

}
