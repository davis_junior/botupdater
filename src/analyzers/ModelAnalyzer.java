package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.Type;

import report.Report;
import util.Util;
import data.Model;

public class ModelAnalyzer extends Analyzer {
	
	public static ModelAnalyzer get = new ModelAnalyzer();

	@Override
	protected boolean verifyClass(ClassGen cg) {
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		int fields = cg.getFields().length;
		
		if (Util.extendsDefault(cg) &&
				fields >= 2 && fields <= 5) {
			int protectedBooleans = 0;
			int staticFinalFloats = 0;
			
			for (Field f : cg.getFields()) {
				if (f.isProtected() && f.getType() == Type.BOOLEAN)
					protectedBooleans++;
				
				if (f.isStatic() && f.isFinal() && f.getType() == Type.FLOAT)
					staticFinalFloats++;
			}
			
			if (protectedBooleans == 1 && staticFinalFloats == 1)
				found = true;
		}
	
		return found;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Model.get.setInternalName(cg.getClassName());
		}
	}
}
