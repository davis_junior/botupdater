package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IF_ACMPNE;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Cache;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.Model;
import data.Node;
import data.Npc;
import data.Player;

public class NodeAnalyzer extends Analyzer {
	
	public static NodeAnalyzer get = new NodeAnalyzer();

	CountInfoItem findPrev = new CountInfoItem("findPrev");
	CountInfoItem findNext = new CountInfoItem("findNext");
	CountInfoItem findId = new CountInfoItem("findId");
	
	public NodeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findPrev, findNext, findId);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (!cg.getSuperclassName().equals(java.lang.Object.class.getName()))
			return false;
		
		int nonstaticFieldCount = 0;
		int longFieldCount = 0;
		int sameClassFieldCount = 0;
		
		FieldData tempId = FieldData.createTemp();
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				nonstaticFieldCount++;
			
				if (f.getType().equals(Type.LONG)) {
					longFieldCount++;
					tempId.set(cg.getClassName(), f.getName(), f.getSignature());
				}
				
				if (f.getType().getSignature().equals("L" + cg.getClassName() + ";"))
					sameClassFieldCount++;
			}
		}
		
		if (nonstaticFieldCount == 3 && longFieldCount == 1 && sameClassFieldCount == 2) {
			Node.get.id.setData(tempId);
			
			found = true;
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Node.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						GETFIELD.class,
						IFNONNULL.class,
						RETURN.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						ACONST_NULL.class,
						new ALOAD(0),
						GETFIELD.class,
						IF_ACMPNE.class,
						RETURN.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns);
				
				if (matches != null) {
					InstructionHandle[] matchesCombined = Util.combine(matches[0], matches[1]);
					
					for (InstructionHandle ih : matchesCombined) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						Node.get.prev.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findPrev.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
						for (Field f : cg.getFields()) {
							if (!f.isStatic() 
									&& f.getName() != Node.get.prev.getInternalName()
									&& f.getType().getSignature().equals("L" + cg.getClassName() + ";")) {
								
								Node.get.next.set(cg.getClassName(), f.getName(), f.getSignature());
								findNext.add(cg.getClassName() + "." + mg.getName(), 0);
								
								found = true;
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findIdMultiplier(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(Node.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				//if (mg.isStatic())
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustClassName(Node.get.getInternalName()).adjustFieldName(Node.get.id.getInternalName()).adjustSignature(Node.get.id.getSignature())
				);
				
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(48),
						LSHR.class
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC2_W(cpg).adjustValue(65535L)
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				patterns.add(pattern3);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					searcher.gotoStart();
					Node.get.id.set(searcher.findNextGetMultiplier(false, false, Node.get.id, cpg));
					
					found = true;
					
					for (InstructionHandle ih : matches[0]) {
						//searcher.gotoHandle(ih);
						//InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						//GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						findId.add(cg.getClassName() + "." + mg.getName() + " multiplier", ih.getPosition());
					}
				}
			}
		//}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Node.get.setInternalName(cg.getClassName());
			
			findFields(cg);
		}
	}
	
	public void analyze_pass2(ClassGen cg) {
		if (Node.get.getInternalName() == null) {
			System.out.println("ERROR: NodeAnalyzer: Node internal class name is null!");
			return;
		}
		
		if (Node.get.id.getInternalName() == null) {
			System.out.println("ERROR: NodeAnalyzer: Node 'id' internal field name is null!");
			return;
		}
		
		findIdMultiplier(cg);
	}

}
