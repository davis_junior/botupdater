package analyzers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.Model;
import data.Node;
import data.NodeDeque;
import data.Player;

public class NodeDequeAnalyzer extends Analyzer {
	
	public static NodeDequeAnalyzer get = new NodeDequeAnalyzer();

	CountInfoItem findTail = new CountInfoItem("findTail");
	
	public NodeDequeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findTail);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		if (Util.extendsDefault(cg)) {
			if (cg.getInterfaces() == null)
				return false;
			
			if (cg.getInterfaces().length == 2) {
				boolean hasCollectionInterface = false;
				boolean hasIterableInterface = false;
				
				for (String iface : cg.getInterfaceNames()) {
					if (iface.equals("java.util.Collection"))
						hasCollectionInterface = true;
					else if (iface.equals("java.lang.Iterable"))
						hasIterableInterface = true;
				}
				
				if (hasCollectionInterface && hasIterableInterface)
					found = true;
			}
		}
		
		return found;
	}
	
	// same as NodeSubQueue.findFields
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(NodeDeque.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						ACONST_NULL.class,
						new PI_PUTFIELD(cpg).adjustClassName(NodeDeque.get.getInternalName()).adjustTypeString(false, Node.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(NodeDeque.get.getInternalName()).adjustTypeString(false, Node.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					Set<String> rejectFieldNames = new HashSet<>();
					
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle rejectFieldIh = ih.getNext();
						
						PUTFIELD instr = (PUTFIELD) rejectFieldIh.getInstruction();
						rejectFieldNames.add(instr.getFieldName(cpg));
					}
					
					for (InstructionHandle ih : matches1[1]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							
							if (!rejectFieldNames.contains(instr.getFieldName(cpg))) {
								NodeDeque.get.tail.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								found = true;
								
								findTail.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			NodeDeque.get.setInternalName(cg.getClassName());
		}
	}
	
	public void analyze_pass2(ClassGen cg) {
		if (Node.get.getInternalName() == null) {
			System.out.println("ERROR: NodeDequeAnalyzer: Node internal class name is null!");
			return;
		}
		
		findFields(cg);
	}
}
