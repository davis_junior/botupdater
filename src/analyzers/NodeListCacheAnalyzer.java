package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;

import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.FieldData;
import data.Node;
import data.NodeDeque;
import data.NodeListCache;

public class NodeListCacheAnalyzer extends Analyzer {
	
	public static NodeListCacheAnalyzer get = new NodeListCacheAnalyzer();

	CountInfoItem findNodeList = new CountInfoItem("findNodeList");
	
	public NodeListCacheAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findNodeList);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(Node.get.getInternalName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		int nonstaticFieldCount = 0;
		int nonstaticNodeDequeCount = 0;
		
		FieldData tempNodeList = FieldData.createTemp("nodeList");
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				nonstaticFieldCount++;
				
				if (f.getType().toString().equals(NodeDeque.get.getInternalName())) {
					nonstaticNodeDequeCount++;
					tempNodeList.set(cg.getClassName(), f.getName(), f.getSignature());
				}
			}
		}
		
		if (nonstaticFieldCount == 1 && (nonstaticFieldCount == nonstaticNodeDequeCount)) {
			NodeListCache.get.nodeList.setData(tempNodeList);
			found = true;
			findNodeList.add(cg.getClassName() + ".<FIELDS>", -1);
		}
		
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			NodeListCache.get.setInternalName(cg.getClassName());
		}
	}
}
