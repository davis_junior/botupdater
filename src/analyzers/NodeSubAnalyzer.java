package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IF_ACMPNE;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.Model;
import data.Node;
import data.NodeSub;
import data.Npc;
import data.NpcNode;
import data.Player;

public class NodeSubAnalyzer extends Analyzer {
	
	public static NodeSubAnalyzer get = new NodeSubAnalyzer();

	CountInfoItem findPrevSub = new CountInfoItem("findPrevSub");
	CountInfoItem findNextSub = new CountInfoItem("findNextSub");
	CountInfoItem findSubId = new CountInfoItem("findSubId");
	
	public NodeSubAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findPrevSub, findNextSub);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (!cg.getSuperclassName().equals(Node.get.getInternalName()))
			return false;
		
		int nonstaticFieldCount = 0;
		int longFieldCount = 0;
		int sameClassFieldCount = 0;
		
		FieldData tempId = FieldData.createTemp();
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				nonstaticFieldCount++;
			
				if (f.getType().equals(Type.LONG)) {
					longFieldCount++;
					tempId.set(cg.getClassName(), f.getName(), f.getSignature());
				}
				
				if (f.getType().getSignature().equals("L" + cg.getClassName() + ";"))
					sameClassFieldCount++;
			}
		}
		
		if (nonstaticFieldCount == 3 && longFieldCount == 1 && sameClassFieldCount == 2) {
			NodeSub.get.subId.setData(tempId);
			
			
			
			found = true;
		}
	
		return found;
	}
	
	// pattern same as Node
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(NodeSub.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						GETFIELD.class,
						IFNONNULL.class,
						RETURN.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						ACONST_NULL.class,
						new ALOAD(0),
						GETFIELD.class,
						IF_ACMPNE.class,
						RETURN.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns);
				
				if (matches != null) {
					InstructionHandle[] matchesCombined = Util.combine(matches[0], matches[1]);
					
					for (InstructionHandle ih : matchesCombined) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						NodeSub.get.prevSub.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findPrevSub.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
						for (Field f : cg.getFields()) {
							if (!f.isStatic() 
									&& f.getName() != NodeSub.get.prevSub.getInternalName()
									&& f.getType().getSignature().equals("L" + cg.getClassName() + ";")) {
								
								NodeSub.get.nextSub.set(cg.getClassName(), f.getName(), f.getSignature());
								findNextSub.add(cg.getClassName() + "." + mg.getName(), 0);
								
								found = true;
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (Node.get.getInternalName() == null) {
			System.out.println("ERROR: NodeSubAnalyzer: Node internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			NodeSub.get.setInternalName(cg.getClassName());
			
			findFields(cg);
		}
	}

}
