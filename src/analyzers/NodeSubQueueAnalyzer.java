package analyzers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IF_ACMPNE;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.HashTable;
import data.Model;
import data.Node;
import data.NodeDeque;
import data.NodeSub;
import data.NodeSubQueue;
import data.Npc;
import data.Player;

public class NodeSubQueueAnalyzer extends Analyzer {
	
	public static NodeSubQueueAnalyzer get = new NodeSubQueueAnalyzer();

	CountInfoItem findTail = new CountInfoItem("findTail");
	
	public NodeSubQueueAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findTail);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!Util.extendsDefault(cg))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();
		
		if (cg.getInterfaces() == null || cg.getInterfaces().length != 1)
			return false;
		
		boolean foundInterface = false;
		for (String iface : cg.getInterfaceNames()) {
			if (iface.equals("java.lang.Iterable"))
				foundInterface = true;
		}
		
		if (!foundInterface)
			return false;
		
		boolean found = false;
		
		boolean foundNodeSubField = false;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				if (f.getType().getSignature().equals("L" + NodeSub.get.getInternalName() + ";"))
					foundNodeSubField = true;
			}
		}
		
		if (foundNodeSubField) {
			found = true;
		}
	
		return found;
	}
	

	// same as NodeDeque.findFields
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(NodeSubQueue.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						ACONST_NULL.class,
						new PI_PUTFIELD(cpg).adjustClassName(NodeSubQueue.get.getInternalName()).adjustTypeString(false, NodeSub.get.getInternalName())
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(NodeSubQueue.get.getInternalName()).adjustTypeString(false, NodeSub.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					Set<String> rejectFieldNames = new HashSet<>();
					
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle rejectFieldIh = ih.getNext();
						
						PUTFIELD instr = (PUTFIELD) rejectFieldIh.getInstruction();
						rejectFieldNames.add(instr.getFieldName(cpg));
					}
					
					for (InstructionHandle ih : matches1[1]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							
							if (!rejectFieldNames.contains(instr.getFieldName(cpg))) {
								NodeSubQueue.get.tail.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
								found = true;
								
								findTail.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (NodeSub.get.getInternalName() == null) {
			System.out.println("ERROR: NodeSubQueueAnalyzer: NodeSub internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			NodeSubQueue.get.setInternalName(cg.getClassName());
			
			findFields(cg);
		}
	}

}
