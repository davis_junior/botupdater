package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.CameraLocationData;
import data.Character;
import data.Model;
import data.Npc;
import data.Player;

public class NpcAnalyzer extends Analyzer {
	
	public static NpcAnalyzer get = new NpcAnalyzer();

	CountInfoItem findTitle = new CountInfoItem("findTitle");
	CountInfoItem findNpcDefinition = new CountInfoItem("findNpcDefinition");
	
	public NpcAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findTitle, findNpcDefinition);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (!cg.getSuperclassName().equals(Character.get.getInternalName()))
			return false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("random"),
					new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(4.0D)
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("random"),
					new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(2.0D)
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("random"),
					new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(3.0D)
			);
			
			final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("random"),
					new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(6.0D)
			);
			
			final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("random"),
					new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(12.0D)
			);
			
			List<CompareInstruction[]> patterns = new ArrayList<>();
			patterns.add(pattern1);
			patterns.add(pattern2);
			patterns.add(pattern3);
			patterns.add(pattern4);
			patterns.add(pattern5);
			
			InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
			
			if (matches != null && matches.length == patterns.size()) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
		}
	
		return found;
	}
	
	private boolean findNpcDefinition(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Npc.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()),
						GETFIELD.class,
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(65535), new PI_LDC_W(cpg).adjustValue(65535), new PI_SIPUSH(cpg).adjustValue(255)),
						IAND.class
						));

				if (matches != null) {
					if (matches.length == 4) { // 2 with 0xFFFF and 2 with 0xFF
						for (InstructionHandle ih : matches) {
							searcher.gotoHandle(ih);
	
							GETFIELD instr2 = (GETFIELD) ih.getNext().getInstruction();
							
							Npc.get.npcDefinition.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
							findNpcDefinition.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
							
							found = true;
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Npc.get.setInternalName(cg.getClassName());
			
			findNpcDefinition(cg);
		}
	}

}
