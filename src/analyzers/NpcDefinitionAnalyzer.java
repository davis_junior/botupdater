package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IF_ICMPNE;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LSHL;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.CameraLocationData;
import data.Character;
import data.Model;
import data.Npc;
import data.NpcDefinition;
import data.ObjectDefinition;
import data.Player;

public class NpcDefinitionAnalyzer extends Analyzer {
	
	public static NpcDefinitionAnalyzer get = new NpcDefinitionAnalyzer();

	CountInfoItem findActions = new CountInfoItem("findActions");
	CountInfoItem findId = new CountInfoItem("findId");
	CountInfoItem findName = new CountInfoItem("findName");
	CountInfoItem findNodeTable = new CountInfoItem("findNodeTable");
	
	public NpcDefinitionAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findActions, findId, findName, findNodeTable);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			
			
			final CompareInstruction[] exclusionPattern1_1 = CompareInstruction.Methods.toCompareInstructions(
					ILOAD.class,
					new PI_BIPUSH(cpg).adjustValue(62),
					IF_ICMPNE.class
			);
			
			final CompareInstruction[] exclusionPattern1_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(62),
					ILOAD.class,
					IF_ICMPNE.class
			);
			
			
			final CompareInstruction[] exclusionPattern2_1 = CompareInstruction.Methods.toCompareInstructions(
					ILOAD.class,
					new PI_BIPUSH(cpg).adjustValue(65),
					IF_ICMPNE.class
			);
			
			final CompareInstruction[] exclusionPattern2_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(65),
					ILOAD.class,
					IF_ICMPNE.class
			);
			
			
			final CompareInstruction[] exclusionPattern3_1 = CompareInstruction.Methods.toCompareInstructions(
					ILOAD.class,
					new PI_BIPUSH(cpg).adjustValue(72),
					IF_ICMPNE.class
			);
			
			final CompareInstruction[] exclusionPattern3_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(72),
					ILOAD.class,
					IF_ICMPNE.class
			);
			
			
			final CompareInstruction[] pattern1_1 = CompareInstruction.Methods.toCompareInstructions(
					ILOAD.class,
					new PI_BIPUSH(cpg).adjustValue(42),
					IF_ICMPNE.class
			);
			
			final CompareInstruction[] pattern1_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(42),
					ILOAD.class,
					IF_ICMPNE.class
			);
			
			
			final CompareInstruction[] pattern2_1 = CompareInstruction.Methods.toCompareInstructions(
					ILOAD.class,
					new PI_BIPUSH(cpg).adjustValue(44),
					IF_ICMPNE.class
			);
			
			final CompareInstruction[] pattern2_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(44),
					ILOAD.class,
					IF_ICMPNE.class
			);
			
			
			final CompareInstruction[] pattern3_1 = CompareInstruction.Methods.toCompareInstructions(
					ILOAD.class,
					new PI_BIPUSH(cpg).adjustValue(93),
					IF_ICMPNE.class
			);
			
			final CompareInstruction[] pattern3_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(93),
					ILOAD.class,
					IF_ICMPNE.class
			);
			
			
			final CompareInstruction[] patternName_1 = CompareInstruction.Methods.toCompareInstructions(
					ILOAD.class,
					new ICONST(2),
					IF_ICMPNE.class
			);
			
			final CompareInstruction[] patternName_2 = CompareInstruction.Methods.toCompareInstructions(
					new ICONST(2),
					ILOAD.class,
					IF_ICMPNE.class
			);
			
			List<CompareInstruction[]> exclusionPatterns1 = new ArrayList<>();
			exclusionPatterns1.add(exclusionPattern1_1);
			exclusionPatterns1.add(exclusionPattern1_2);
			
			List<CompareInstruction[]> exclusionPatterns2 = new ArrayList<>();
			exclusionPatterns2.add(exclusionPattern2_1);
			exclusionPatterns2.add(exclusionPattern2_2);
			
			List<CompareInstruction[]> exclusionPatterns3 = new ArrayList<>();
			exclusionPatterns3.add(exclusionPattern3_1);
			exclusionPatterns3.add(exclusionPattern3_2);
			
			InstructionHandle[][] exclusionMatches1 = searcher.searchAllFromStart(false, false, CompareType.Opcode, exclusionPatterns1);
			InstructionHandle[][] exclusionMatches2 = null;
			InstructionHandle[][] exclusionMatches3 = null;
			
			if (exclusionMatches1 != null)
				continue;
			
			exclusionMatches2 = searcher.searchAllFromStart(false, false, CompareType.Opcode, exclusionPatterns2);
			if (exclusionMatches2 != null)
				continue;
			
			exclusionMatches3 = searcher.searchAllFromStart(false, false, CompareType.Opcode, exclusionPatterns3);
			if (exclusionMatches3 != null)
				continue;
			
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1_1);
			patterns1.add(pattern1_2);
			
			List<CompareInstruction[]> patterns2 = new ArrayList<>();
			patterns2.add(pattern2_1);
			patterns2.add(pattern2_2);
			
			List<CompareInstruction[]> patterns3 = new ArrayList<>();
			patterns3.add(pattern3_1);
			patterns3.add(pattern3_2);
			
			List<CompareInstruction[]> patternsName = new ArrayList<>();
			patternsName.add(patternName_1);
			patternsName.add(patternName_2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, false, CompareType.Opcode, patterns1);
			InstructionHandle[][] matches2 = null;
			InstructionHandle[][] matches3 = null;
			InstructionHandle[][] matchesName = null;
			
			if (matches1 != null) {
				matches2 = searcher.searchAllFromStart(false, false, CompareType.Opcode, patterns2);
				
				if (matches2 != null) {
					matches3 = searcher.searchAllFromStart(false, false, CompareType.Opcode, patterns3);
					
					if (matches3 != null) {
						matchesName = searcher.searchAllFromStart(false, true, CompareType.Opcode, patternsName);
						
						if (matchesName != null) {
							InstructionHandle[] matchesNameCombined = Util.combine(matchesName[0], matchesName[1]);
							
							if (matchesNameCombined != null && matchesNameCombined.length > 0) {
								for (InstructionHandle ih : matchesNameCombined) {
									
									searcher.gotoHandle(ih);
									InstructionHandle fieldIh = searcher.getNext(true, PUTFIELD.class);
									
									if (fieldIh != null) {
										PUTFIELD fieldInstr = (PUTFIELD) fieldIh.getInstruction();
										NpcDefinition.get.name.set(fieldInstr.getReferenceType(cpg).toString(), fieldInstr.getFieldName(cpg), fieldInstr.getSignature(cpg));
										findName.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
										
										found = true;
									}
								}
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findId(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(NpcDefinition.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(16),
						ISHL.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(24),
						LSHL.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.searchPrev(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(NpcDefinition.get.getInternalName()).adjustType(Type.INT));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							NpcDefinition.get.id.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findId.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoHandle(ih);
							NpcDefinition.get.id.set(searcher.findPrevGetMultiplier(false, false, NpcDefinition.get.id, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			NpcDefinition.get.setInternalName(cg.getClassName());
			
			findId(cg);
		}
	}

}
