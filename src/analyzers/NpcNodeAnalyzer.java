package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import searcher.Comparator.CompareType;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.Model;
import data.Node;
import data.Npc;
import data.NpcNode;
import data.Player;

public class NpcNodeAnalyzer extends Analyzer {
	
	public static NpcNodeAnalyzer get = new NpcNodeAnalyzer();

	CountInfoItem findNpc = new CountInfoItem("findNpc");
	
	public NpcNodeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findNpc);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (!cg.getSuperclassName().equals(Node.get.getInternalName()))
			return false;
		
		int nonstaticFieldCount = 0;
		int objectFieldCount = 0;
		
		FieldData tempNpc = FieldData.createTemp();
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				nonstaticFieldCount++;
				
				if (f.getType().equals(Type.getType(java.lang.Object.class))) {
					objectFieldCount++;
					tempNpc.set(cg.getClassName(), f.getName(), f.getSignature());
				}
			}
		}
		
		if (nonstaticFieldCount == 1 && objectFieldCount == 1) {
			NpcNode.get.npc.setData(tempNpc);
			
			found = true;
		}
	
		return found;
	}
	

	
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Node.get.getInternalName() == null) {
			System.out.println("ERROR: NpcNodeAnalyzer: Node internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			NpcNode.get.setInternalName(cg.getClassName());
		}
	}

}
