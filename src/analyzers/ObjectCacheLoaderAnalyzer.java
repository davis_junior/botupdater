package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import searcher.Searcher;
import data.Cache;
import data.Model;
import data.ObjectComposite;
import data.ObjectCacheLoader;
import data.ObjectDefinition;

public class ObjectCacheLoaderAnalyzer extends Analyzer {
	
	public static ObjectCacheLoaderAnalyzer get = new ObjectCacheLoaderAnalyzer();

	CountInfoItem findId = new CountInfoItem("findId");
	//CountInfoItem findDefinitionCache = new CountInfoItem("findDefinitionCache");
	CountInfoItem findAnimatedModelCache = new CountInfoItem("findAnimatedModelCache");
	CountInfoItem findModelCache = new CountInfoItem("findModelCache");
	CountInfoItem findCompositeCache = new CountInfoItem("findCompositeCache");
	
	public ObjectCacheLoaderAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findId, /*findDefinitionCache,*/ findAnimatedModelCache, findModelCache, findCompositeCache);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.isAbstract())
			return false;
		
		if (!cg.getSuperclassName().equals(java.lang.Object.class.getName()))
			return false;
		
		//ConstantPoolGen cpg = cg.getConstantPool();
		
		boolean found = false;
		
		int cacheFieldCount = 0;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				if (f.getType().getSignature().equals("L" + Cache.get.getInternalName() + ";"))
					cacheFieldCount++;
			}
		}
		
		if (cacheFieldCount >= 3) {
			found = true;
		}
	
		return found;
	}
	
	/*private boolean findDefinitionCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ObjectCacheLoader.get.getInternalName())) {
			for (Method m : cg.getMethods()) {
				MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
				
				if (mg.isStatic())
					continue;
				
				if (!m.getReturnType().getSignature().equals("L" + ObjectDefinition.get.getInternalName() + ";"))
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustTypeString(false, Cache.get.getInternalName()).adjustClassName(cg.getClassName())
				};
				
				List<Object[]> patterns = new ArrayList<Object[]>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						ObjectCacheLoader.get.definitionCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findDefinitionCache.add(cg.getClassName() + "." + m.getName(), fieldIh.getPosition());
						
						found = true;
					}
				}
			}
		}
	
		return found;
	}*/
	
	// same as definitionCache except the method is in ObjectDefinition
	private boolean findAnimatedModelCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				if (!mg.getReturnType().getSignature().equals("L" + Model.get.getInternalName() + ";"))
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				
				final CompareInstruction[] exclusionPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(16384)
				);
				
				final CompareInstruction[] exclusionPattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(32768), new PI_LDC_W(cpg).adjustValue(32768))
				);
				
				final CompareInstruction[] exclusionPattern3 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(524288), new PI_LDC_W(cpg).adjustValue(524288))
				);
				
				final CompareInstruction[] exclusionPattern4 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(127007), new PI_LDC_W(cpg).adjustValue(127007))
				);
				
				List<CompareInstruction[]> exclusdionPatterns = new ArrayList<>();
				exclusdionPatterns.add(exclusionPattern1);
				exclusdionPatterns.add(exclusionPattern2);
				exclusdionPatterns.add(exclusionPattern3);
				exclusdionPatterns.add(exclusionPattern4);
				
				InstructionHandle[][] exclusionMatches = searcher.searchAllFromStart(false, false, CompareType.Opcode, exclusdionPatterns);
				
				if (exclusionMatches != null)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()),
						new PI_GETFIELD(cpg).adjustTypeString(false, Cache.get.getInternalName()).adjustClassName(ObjectCacheLoader.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null) {
					for (InstructionHandle ih : matches[0]) {
						//searcher.gotoHandle(ih);
						//InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						InstructionHandle fieldIh = ih.getNext().getNext();
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						ObjectCacheLoader.get.animatedModelCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findAnimatedModelCache.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
						found = true;
					}
				}
			}
		}
	
		return found;
	}
	
	// same as modelCache except the method is in ObjectDefinition and exclusion patterns are inclusion patterns
	private boolean findModelCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				if (!mg.getReturnType().getSignature().equals("L" + Model.get.getInternalName() + ";"))
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(16384)
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(32768), new PI_LDC_W(cpg).adjustValue(32768))
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(524288), new PI_LDC_W(cpg).adjustValue(524288))
				);
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(127007), new PI_LDC_W(cpg).adjustValue(127007))
				);
				
				final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()),
						new PI_GETFIELD(cpg).adjustTypeString(false, Cache.get.getInternalName()).adjustClassName(ObjectCacheLoader.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				patterns.add(pattern3);
				patterns.add(pattern4);
				patterns.add(pattern5);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[4]) {
						//searcher.gotoHandle(ih);
						//InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						InstructionHandle fieldIh = ih.getNext().getNext();
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						ObjectCacheLoader.get.modelCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findModelCache.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
						found = true;
					}
				}
			}
		}
	
		return found;
	}
	
	// same as definitionCache except the method is in ObjectDefinition
	private boolean findCompositeCache(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				if (!mg.getReturnType().getSignature().equals("L" + ObjectComposite.get.getInternalName() + ";"))
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				
				final CompareInstruction[] exclusionPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(16384)
				);
				
				final CompareInstruction[] exclusionPattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(32768), new PI_LDC_W(cpg).adjustValue(32768))
				);
				
				final CompareInstruction[] exclusionPattern3 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(524288), new PI_LDC_W(cpg).adjustValue(524288))
				);
				
				final CompareInstruction[] exclusionPattern4 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(127007), new PI_LDC_W(cpg).adjustValue(127007))
				);
				
				List<CompareInstruction[]> exclusdionPatterns = new ArrayList<>();
				exclusdionPatterns.add(exclusionPattern1);
				exclusdionPatterns.add(exclusionPattern2);
				exclusdionPatterns.add(exclusionPattern3);
				exclusdionPatterns.add(exclusionPattern4);
				
				InstructionHandle[][] exclusionMatches = searcher.searchAllFromStart(false, false, CompareType.Opcode, exclusdionPatterns);
				
				if (exclusionMatches != null)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustClassName(cg.getClassName()),
						new PI_GETFIELD(cpg).adjustTypeString(false, Cache.get.getInternalName()).adjustClassName(ObjectCacheLoader.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null) {
					for (InstructionHandle ih : matches[0]) {
						//searcher.gotoHandle(ih);
						//InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						InstructionHandle fieldIh = ih.getNext().getNext();
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						
						ObjectCacheLoader.get.compositeCache.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findCompositeCache.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
						found = true;
					}
				}
			}
		}
	
		return found;
	}
	
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Cache.get.getInternalName() == null) {
			System.out.println("ERROR: ObjectCacheLoaderAnalyzer: Cache internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			ObjectCacheLoader.get.setInternalName(cg.getClassName());
			
			/*if (Cache.get.getInternalName() == null) {
				System.out.println("ERROR: ObjectCacheLoaderAnalyzer: Cache internal class name is null!");
				//return;
			} else
				findDefinitionCache(cg);*/
		}
	}
	
	public void analyze_pass6(ClassGen cg) {
		if (ObjectCacheLoader.get.getInternalName() == null) {
			//System.out.println("ERROR: ObjectCacheLoaderAnalyzer: ObjectCacheLoader internal class name is null!");
			return;
		}
		
		if (Cache.get.getInternalName() == null) {
			System.out.println("ERROR: ObjectCacheLoaderAnalyzer: Cache internal class name is null!");
			return;
		}
		
		if (ObjectDefinition.get.getInternalName() == null) {
			System.out.println("ERROR: ObjectCacheLoaderAnalyzer: ObjectDefinition internal class name is null!");
			return;
		}
		
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: ObjectCacheLoaderAnalyzer: Model internal class name is null!");
			return;
		}
		
		findAnimatedModelCache(cg);
		findModelCache(cg);
		
		
		if (ObjectComposite.get.getInternalName() == null) {
			System.out.println("ERROR: ObjectCacheLoaderAnalyzer: ObjectComposite internal class name is null!");
			return;
		}
		
		findCompositeCache(cg);
	}

}
