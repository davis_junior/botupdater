package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.CHECKCAST;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IF_ACMPNE;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Cache;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.Model;
import data.Node;
import data.Npc;
import data.ObjectComposite;
import data.Player;

public class ObjectCompositeAnalyzer extends Analyzer {
	
	public static ObjectCompositeAnalyzer get = new ObjectCompositeAnalyzer();

	CountInfoItem findModel = new CountInfoItem("findModel");
	
	public ObjectCompositeAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findModel);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		if (!cg.getSuperclassName().equals(java.lang.Object.class.getName()))
			return false;
		
		if (cg.getInterfaces().length > 0)
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean foundHashCodeMethod = false;
		for (Method m : cg.getMethods()) {
			if (m.getName().equals("hashCode")) {
				foundHashCodeMethod = true;
				break;
			}
		}
		
		if (!foundHashCodeMethod)
			return false;
		
		int nonstaticObjectFieldCount = 0;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic()) {
				if (f.getType().getSignature().equals("Ljava/lang/Object;"))
					nonstaticObjectFieldCount++;
			}
		}
		
		if (nonstaticObjectFieldCount >= 2) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.lang.Object").adjustMethodName("hashCode")
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					found = true;
				}
			}
		}
	
		return found;
	}
	
	private boolean findModel(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		//if (cg.getClassName().equals(ObjectComposite.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				//if (mg.isStatic())
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(ObjectComposite.get.getInternalName()).adjustTypeString(false, "java.lang.Object")
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						boolean foundCheckCastModel = false;
						
						InstructionHandle modelIh = ih.getNext();
						int count = 0;
						while (count <= 6) {
							if (modelIh == null)
								break;
							
							if (modelIh.getInstruction() instanceof CHECKCAST) {
								CHECKCAST checkInstr = (CHECKCAST) modelIh.getInstruction();
								if (checkInstr.getType(cpg).toString().equals(Model.get.getInternalName())) {
									foundCheckCastModel = true;
									break;
								}
							}
							
							modelIh = modelIh.getNext();
							count++;
						}
						
						if (foundCheckCastModel) {
							GETFIELD instr = (GETFIELD) ih.getInstruction();
							found = true;
							
							ObjectComposite.get.model.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							findModel.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			ObjectComposite.get.setInternalName(cg.getClassName());
		}
	}
	
	public void analyze_pass2(ClassGen cg) {
		if (ObjectComposite.get.getInternalName() == null) {
			System.out.println("ERROR: ObjectCompositeAnalyzer: ObjectComposite internal class name is null!");
			//return;
		} else if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: ObjectCompositeAnalyzer: Model internal class name is null!");
			//return;
		} else
			findModel(cg);
	}
}
