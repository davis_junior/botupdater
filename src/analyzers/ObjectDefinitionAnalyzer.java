package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractDefLoader;
import data.BaseInfo;
import data.Cache;
import data.Character;
import data.GameInfo;
import data.Model;
import data.Node;
import data.ObjectCacheLoader;
import data.ObjectDefinition;
import data.Player;

public class ObjectDefinitionAnalyzer extends Analyzer {
	
	public static ObjectDefinitionAnalyzer get = new ObjectDefinitionAnalyzer();

	CountInfoItem findName = new CountInfoItem("findName");
	CountInfoItem findActions = new CountInfoItem("findActions");
	CountInfoItem findAnimating = new CountInfoItem("findAnimating");
	CountInfoItem findMirrored = new CountInfoItem("findMirrored");
	CountInfoItem findId = new CountInfoItem("findId");
	CountInfoItem findDefinitionLoader = new CountInfoItem("findDefinitionLoader");
	CountInfoItem findCacheLoader = new CountInfoItem("findCacheLoader");
	
	CountInfoItem findModelCache2byteArray = new CountInfoItem("findModelCache2byteArray");
	CountInfoItem findModelCache2intArray2D = new CountInfoItem("findModelCache2intArray2D");
	
	public ObjectDefinitionAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findName, findActions, findAnimating, findMirrored, findId, findDefinitionLoader, findCacheLoader, findModelCache2byteArray, findModelCache2intArray2D);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_SIPUSH(cpg).adjustValue(180),
					new ICONST(0),
					new PI_SIPUSH(cpg).adjustValue(-180)
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_SIPUSH(cpg).adjustValue(4096)
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_SIPUSH(cpg).adjustValue(8192)
			);
			
			final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
					new PI_SIPUSH(cpg).adjustValue(12288)
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			patterns1.add(pattern3);
			patterns1.add(pattern4);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				//for (InstructionHandle ih : matches) {
					found = true;
					
					for (Field f : cg.getFields()) {
						if (f.isStatic())
							continue;
						
						if (f.getSignature().equals("Ljava/lang/String;"))
							ObjectDefinition.get.name.set(cg.getClassName(), f.getName(), f.getSignature());
						
						if (f.getSignature().equals("[Ljava/lang/String;"))
							ObjectDefinition.get.actions.set(cg.getClassName(), f.getName(), f.getSignature());
					}
				//}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findId(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(10),
						ISHL.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getPrev(false, GETFIELD.class);
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							ObjectDefinition.get.id.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findId.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoStart();
							ObjectDefinition.get.id.set(searcher.findNextGetMultiplier(false, false, ObjectDefinition.get.id, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findDefinitionLoader(ClassGen cg) {
		boolean found = false;
		
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			for (Field f : cg.getFields()) {
				if (f.isStatic())
					continue;
				
				if (f.getType().getSignature().equals("L" + AbstractDefLoader.get.getInternalName() + ";")) {
					ObjectDefinition.get.definitionLoader.set(cg.getClassName(), f.getName(), f.getSignature());
					
					found = true;
				}
			}
		}
		
		return found;
	}
	
	private boolean findCacheLoader(ClassGen cg) {
		boolean found = false;
		
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			for (Field f : cg.getFields()) {
				if (f.isStatic())
					continue;
				
				if (f.getType().getSignature().equals("L" + ObjectCacheLoader.get.getInternalName() + ";")) {
					ObjectDefinition.get.cacheLoader.set(cg.getClassName(), f.getName(), f.getSignature());
					
					found = true;
				}
			}
		}
		
		return found;
	}
	
	// same as modelCache2 in ObjectDefLoaderAnalyzer
	private boolean findModelCache2Fields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				if (!mg.getReturnType().getSignature().equals("L" + Model.get.getInternalName() + ";"))
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(16384)
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(32768), new PI_LDC_W(cpg).adjustValue(32768))
				);
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(524288), new PI_LDC_W(cpg).adjustValue(524288))
				);
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(127007), new PI_LDC_W(cpg).adjustValue(127007))
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				patterns.add(pattern3);
				patterns.add(pattern4);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[2]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.searchNext(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(new ALOAD(0), new PI_GETFIELD(cpg).adjustType(Type.getType(byte[].class))));
						
						boolean foundByteArray = false;
						boolean foundIntArray2D = false;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getNext().getInstruction();
							
							ObjectDefinition.get.modelCache2byteArray.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							findModelCache2byteArray.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
							foundByteArray = true;
						}
						
						searcher.gotoHandle(ih);
						fieldIh = searcher.searchNext(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(new ALOAD(0), new PI_GETFIELD(cpg).adjustType(Type.getType(int[][].class))));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getNext().getInstruction();
							
							ObjectDefinition.get.modelCache2intArray2D.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							findModelCache2intArray2D.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						
							foundIntArray2D = true;
						}
						
						if (foundByteArray && foundIntArray2D)
							found = true;
					}
				}
			}
		}
	
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			ObjectDefinition.get.setInternalName(cg.getClassName());

			findId(cg);
			
			if (Model.get.getInternalName() == null) {
				System.out.println("ERROR: ObjectDefinitionAnalyzer: Model internal class name is null!");
				//return;
			} else
				findModelCache2Fields(cg);
		}
	}
	
	public void analyze_ObjectDefLoader(ClassGen cg) {
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			if (AbstractDefLoader.get.getInternalName() == null) {
				System.out.println("ERROR: ObjectDefinitionAnalyzer: AbstractDefLoader internal class name is null!");
				//return;
			} else 
				findDefinitionLoader(cg);
		}
	}
	
	public void analyze_ObjectCacheLoader(ClassGen cg) {
		if (cg.getClassName().equals(ObjectDefinition.get.getInternalName())) {
			if (ObjectCacheLoader.get.getInternalName() == null) {
				System.out.println("ERROR: ObjectDefinitionAnalyzer: ObjectCacheLoader internal class name is null!");
				//return;
			} else 
				findCacheLoader(cg);
		}
	}
}
