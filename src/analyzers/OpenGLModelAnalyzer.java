package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.DescendingVisitor;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FCMPL;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.IALOAD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IDIV;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISTORE;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.NEWARRAY;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.SALOAD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.InstructionClass;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_NEWARRAY;
import searcher.pi.PI_SIPUSH;
import util.DependencyEmitter;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.FieldData;
import data.Model;
import data.OpenGLModel;
import data.SDModel;

public class OpenGLModelAnalyzer extends Analyzer {
	
	public static OpenGLModelAnalyzer get = new OpenGLModelAnalyzer();
	
	CountInfoItem findIndices_Count = new CountInfoItem("findIndices");
	CountInfoItem findPoints_Count = new CountInfoItem("findPoints");
	
	public OpenGLModelAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findIndices_Count, findPoints_Count);
	}

	@Override
	protected boolean verifyClass(ClassGen cg) {
		/*
		 * classes that extend Model: 3
		 * 		OpenGL Model
		 * 		SDModel
		 * 		? - uses ByteBuffer
		 */
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getSuperclassName().equals(Model.get.getInternalName())) {
			
		    DependencyEmitter visitor = new DependencyEmitter(cg.getJavaClass());
		    DescendingVisitor classWalker = new DescendingVisitor(cg.getJavaClass(), visitor);
		    classWalker.visit();
		    
		    //visitor.printDependencies();
		    
		    boolean foundDependency = false;
		    
		    for (String dependency : visitor.getDependencies()) {
		    	if (dependency.equals("jaggl/OpenGL"))
		    		foundDependency = true;
		    }
		    
		    if (foundDependency)
		    	found = true;
		}
		
		return found;
	}

	private boolean findIndices(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(OpenGLModel.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(65535), new PI_LDC_W(cpg).adjustValue(65535)),
						IAND.class,
						IALOAD.class,
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(-999999), new PI_LDC_W(cpg).adjustValue(-999999))
						));
				
				int loop = 0;
				
				if (matches != null && matches.length == 3) {
					for (InstructionHandle ih : matches) {
						loop++;
						
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getPrev(true, GETFIELD.class);
						
						if (fieldIh != null) {
							if (loop == 1) { // get other fields
								{ // BEGIN numFaces
									searcher.gotoHandle(fieldIh);
									InstructionHandle numFacesIh = searcher.getPrev(false, GETFIELD.class);
									
									if (numFacesIh != null) {
										GETFIELD numFacesInstr = (GETFIELD) numFacesIh.getInstruction();
										OpenGLModel.get.numFaces.set(cg.getClassName(), numFacesInstr.getFieldName(cpg), numFacesInstr.getFieldType(cpg).getSignature());
									}
								} // END numFaces

								{ // BEGIN projectionLength
									final CompareInstruction[] patternProjectionLength1 = CompareInstruction.Methods.toCompareInstructions(
											new ALOAD(0),
											GETFIELD.class,
											new PI_NEWARRAY(cpg).adjustType(Type.getType(int[].class)),
											PUTFIELD.class
									);
									
									List<CompareInstruction[]> patternsProjectionLength = new ArrayList<>();
									patternsProjectionLength.add(patternProjectionLength1);
									
									searcher.gotoHandle(fieldIh);
									InstructionHandle[][] matchesProjectionLength = searcher.searchAllPrev(true, false, CompareType.Opcode, patternsProjectionLength);
									
									if (matchesProjectionLength != null && matchesProjectionLength.length == patternsProjectionLength.size()) {
										for (InstructionHandle projectionLengthIh : matchesProjectionLength[0]) {
											GETFIELD fieldInstr = (GETFIELD) projectionLengthIh.getNext().getInstruction();
											OpenGLModel.get.projectionLength.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
											
											//found = true;
										}
									}
								} // END projectionLength
								
								{ // BEGIN projectionChecks
									final CompareInstruction[] patternProjectionChecks1 = CompareInstruction.Methods.toCompareInstructions(
											new ALOAD(0),
											GETFIELD.class,
											ILOAD.class,
											IALOAD.class,
											ISTORE.class,
											new ALOAD(0),
											GETFIELD.class,
											ILOAD.class,
											new ICONST(1),
											IADD.class,
											IALOAD.class,
											ISTORE.class
									);
									
									List<CompareInstruction[]> patternsProjectionChecks = new ArrayList<>();
									patternsProjectionChecks.add(patternProjectionChecks1);
									
									searcher.gotoHandle(fieldIh);
									InstructionHandle[][] matchesProjectionChecks = searcher.searchAllPrev(true, false, CompareType.Opcode, patternsProjectionChecks);
									
									if (matchesProjectionChecks != null && matchesProjectionChecks.length == patternsProjectionChecks.size()) {
										for (InstructionHandle projectionChecksIh : matchesProjectionChecks[0]) {
											GETFIELD fieldInstr = (GETFIELD) projectionChecksIh.getNext().getInstruction();
											OpenGLModel.get.projectionChecks.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
											
											//found = true;
										}
									}
								} // END projectionChecks
								
								{ // BEGIN projectionIndices
									final CompareInstruction[] patternProjectionIndices1 = CompareInstruction.Methods.toCompareInstructions(
											new ALOAD(0),
											GETFIELD.class,
											ILOAD.class,
											SALOAD.class,
											new PI_LDC(cpg).adjustValue(65535),
											IAND.class,
											new ICONST(1),
											ISUB.class
											//ISTORE.class
									);
									
									List<CompareInstruction[]> patternsProjectionIndices = new ArrayList<>();
									patternsProjectionIndices.add(patternProjectionIndices1);
									
									searcher.gotoHandle(fieldIh);
									InstructionHandle[][] matchesProjectionIndices = searcher.searchAllPrev(true, false, CompareType.Opcode, patternsProjectionIndices);
									
									if (matchesProjectionIndices != null && matchesProjectionIndices.length == patternsProjectionIndices.size()) {
										for (InstructionHandle projectionIndicesIh : matchesProjectionIndices[0]) {
											GETFIELD fieldInstr = (GETFIELD) projectionIndicesIh.getNext().getInstruction();
											OpenGLModel.get.projectionIndices.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
											
											//found = true;
										}
									}
								} // END projectionIndices
							}
							
							GETFIELD fieldInstr = (GETFIELD) fieldIh.getInstruction();
							
							switch (loop) {
								case 1:
									OpenGLModel.get.indices1.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
								case 2:
									OpenGLModel.get.indices2.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
								case 3:
									OpenGLModel.get.indices3.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
							}
						}
					}
					
					if (loop == 3) {
						found = true;
						
						findIndices_Count.add(cg.getClassName() + "." + mg.getName(), matches[0].getPosition());	
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findPoints(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(OpenGLModel.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] exclusionPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(65535), new PI_LDC_W(cpg).adjustValue(65535))
				);
				
				List<CompareInstruction[]> exclusionPatterns = new ArrayList<>();
				exclusionPatterns.add(exclusionPattern1);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(128)
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(7),
						ISHR.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				InstructionHandle[][] exclusionMatches = searcher.searchAllFromStart(true, false, CompareType.Opcode, exclusionPatterns);
				
				if (exclusionMatches != null)
					continue;
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				int loop = 0;
				
				if (matches != null && matches.length == patterns.size() && matches[0].length == 3 && matches[1].length == 3) {
					for (InstructionHandle ih : matches[1]) {
						loop++;
						
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getPrev(true, GETFIELD.class);
						
						if (fieldIh != null) {
							if (loop == 1) { // get numVerticies
								InstructionHandle numVertIh = searcher.searchFromStart(false, CompareType.Opcode, new InstructionClass(GETFIELD.class));
								
								if (numVertIh != null) {
									GETFIELD numVertInstr = (GETFIELD) numVertIh.getInstruction();
									OpenGLModel.get.numVertices.set(cg.getClassName(), numVertInstr.getFieldName(cpg), numVertInstr.getFieldType(cpg).getSignature());
								}
							}
							
							GETFIELD fieldInstr = (GETFIELD) fieldIh.getInstruction();
							
							switch (loop) {
								case 1:
									OpenGLModel.get.xPoints.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
								case 2:
									OpenGLModel.get.yPoints.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
								case 3:
									OpenGLModel.get.zPoints.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
							}
						}
					}
					
					if (loop == 3) {
						found = true;
						
						findPoints_Count.add(cg.getClassName() + "." + mg.getName(), matches[0][0].getPosition());
					}
				}
			}
		}
	
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: SDModelAnalyzer: Model internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			OpenGLModel.get.setInternalName(cg.getClassName());
			
			findIndices(cg);
			findPoints(cg);
		}
	}
	
	/*public void analyze_pass2(ClassGen cg) {
		if (OpenGLModel.get.getInternalName() == null) {
			//System.out.println("ERROR: OpenGLModelAnalyzer: OpenGLModel internal class name is null!");
			return;
		}
		
		if (cg.getClassName().equals(OpenGLModel.get.getInternalName())) {
			//findIndices(cg);
		}
	}*/

}
