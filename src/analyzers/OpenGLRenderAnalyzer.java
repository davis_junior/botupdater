package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DUP_X1;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FDIV;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IINC;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISTORE;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_INVOKEINTERFACE;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import data.GameInfo;
import data.OpenGLRender;
import data.SDRender;

public class OpenGLRenderAnalyzer extends Analyzer {
	
	public static OpenGLRenderAnalyzer get = new OpenGLRenderAnalyzer();

	CountInfoItem findOpenGL = new CountInfoItem("findOpenGL");
	CountInfoItem findClientViewport = new CountInfoItem("findClientViewport");
	CountInfoItem findXMultiplier = new CountInfoItem("findXMultiplier");
	CountInfoItem findYMultiplier = new CountInfoItem("findYMultiplier");
	CountInfoItem findAbsoluteX = new CountInfoItem("findAbsoluteX");
	CountInfoItem findAbsoluteY = new CountInfoItem("findAbsoluteY");
	
	public OpenGLRenderAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findClientViewport, findXMultiplier, findYMultiplier, findAbsoluteX, findAbsoluteY);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;

		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
			InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
					new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glBegin")
					/*new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glTexCoord2f"),
					new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glVertex2f"),
					new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glTexCoord2f"),
					new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glVertex2f"),
					new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glEnd")*/
					);

			if (matches != null) {
				PI_INVOKESTATIC openGLInstruction = new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL");
				
				for (InstructionHandle ih : matches) {
					searcher.gotoHandle(ih);
					
					InstructionHandle[] glListHandles = new InstructionHandle[5];
					InstructionList glList = new InstructionList();
					
					for (int i = 0; i < glListHandles.length; i++) {
						glListHandles[i] = searcher.searchNext(true, CompareType.Opcode, openGLInstruction);
						
						if (glListHandles[i] == null)
							break;
						else
							glList.append(glListHandles[i].getInstruction());
					}
					
					if (glList.getLength() == glListHandles.length) {
						Searcher glListSearcher = new Searcher(glList);
						
						if (glListSearcher.searchFromStart(false, CompareType.Opcode, 
								new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glTexCoord2f"),
								new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glVertex2f"),
								new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glTexCoord2f"),
								new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glVertex2f"),
								new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glEnd")
								) != null) {
							
							found = true;
						}
					}
				}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findOpenGL(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(OpenGLRender.get.getInternalName())) {
			
			for (Field field :  cg.getFields()) {
				if (field.getSignature().equals("Ljaggl/OpenGL;")) {
					OpenGLRender.get.openGL.set(cg.getClassName(), field.getName(), field.getSignature());								
					findOpenGL.add(cg.getClassName(), -1);
					
					found = true;
				}
			}
		}
	
		return found;
	}
	
	private boolean findClientViewport(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(OpenGLRender.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				/*final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(16),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(32),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(1),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(2),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(4),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern6 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(8),
						IOR.class,
						ISTORE.class
				};
				
				List<Object[]> patterns = new ArrayList<Object[]>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				patterns.add(pattern3);
				patterns.add(pattern4);
				patterns.add(pattern5);
				patterns.add(pattern6);*/
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(Float.NaN)
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					if (matches[0].length == 9) {
					//for (InstructionHandle ih : matches[0]) {
						//searcher.gotoHandle(matches[0][matches[0].length - 1].getNext());
						searcher.gotoStart();
						InstructionHandle fieldIh = searcher.getNext(true, GETFIELD.class);
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							OpenGLRender.get.clientViewport.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findClientViewport.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());						
						}
					//}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findOtherFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(OpenGLRender.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKESTATIC(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("glViewport")
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new FCONST(2),
						FDIV.class,
						PUTFIELD.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					if (matches[0].length == 1 && matches[1].length == 2) {
					//for (InstructionHandle ih : matches[1]) {
						searcher.gotoHandle(matches[1][0]);
						InstructionHandle xMultiplierIh = searcher.getNext(true, PUTFIELD.class);
						
						if (xMultiplierIh != null) {
							PUTFIELD instr = (PUTFIELD) xMultiplierIh.getInstruction();
							OpenGLRender.get.xMultiplier.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findXMultiplier.add(cg.getClassName() + "." + mg.getName(), xMultiplierIh.getPosition());						
						}
						
						InstructionHandle yMultiplierIh = searcher.getNext(true, PUTFIELD.class);
						
						if (yMultiplierIh != null) {
							PUTFIELD instr = (PUTFIELD) yMultiplierIh.getInstruction();
							OpenGLRender.get.yMultiplier.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findYMultiplier.add(cg.getClassName() + "." + mg.getName(), yMultiplierIh.getPosition());						
						}
						
						
						InstructionHandle absoluteXIh = searcher.getNext(true, PUTFIELD.class);
						
						if (absoluteXIh != null) {
							PUTFIELD instr = (PUTFIELD) absoluteXIh.getInstruction();
							OpenGLRender.get.absoluteX.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findAbsoluteX.add(cg.getClassName() + "." + mg.getName(), absoluteXIh.getPosition());						
						}
						
						InstructionHandle absoluteYIh = searcher.getNext(true, PUTFIELD.class);
						
						if (absoluteYIh != null) {
							PUTFIELD instr = (PUTFIELD) absoluteYIh.getInstruction();
							OpenGLRender.get.absoluteY.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findAbsoluteY.add(cg.getClassName() + "." + mg.getName(), absoluteYIh.getPosition());						
						}
					//}
					}
				}
			}
		}
	
		return found;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			OpenGLRender.get.setInternalName(cg.getClassName());
			
			findOpenGL(cg);
			findClientViewport(cg);
			findOtherFields(cg);
		}
	}
}
