package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DDIV;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.FieldData;
import data.Node;
import data.Player;
import data.PlayerDef;

public class PlayerAnalyzer extends Analyzer {
	
	public static PlayerAnalyzer get = new PlayerAnalyzer();

	CountInfoItem findDefinition = new CountInfoItem("findDefinition");
	CountInfoItem findName = new CountInfoItem("findName");
	CountInfoItem findTitle = new CountInfoItem("findTitle");
	
	public PlayerAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findDefinition, findName, findTitle);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean found1 = false;
		boolean found2 = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			// also used by npc
			/*if (mg.getArgumentTypes().length == 2) {
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
				I2F.class,
				new PI_LDC_W(cpg).adjustType(Type.FLOAT).adjustValue(10.0F),
				FDIV.class
				);
			}*/
			
			if (mg.getArgumentTypes().length == 2) {
				// also used by npc
				InstructionHandle[] matches1 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new FCONST(0),
						new SynonymInstruction(new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(-5.0F), new PI_LDC_W(cpg).adjustType(Type.FLOAT).adjustValue(-5.0F)),
						new FCONST(0)
						));
	
				if (matches1 != null) {
					//for (InstructionHandle ih : matches1) {
						found1 = true;
					//}
				}
			}
			
			
			if (mg.getArgumentTypes().length >= 5) { // 7 arguments
				searcher.gotoStart();
				InstructionHandle[] matches2 = searcher.searchAllFromStart(false, CompareType.Instance, CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("atan2"),
						new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(2607.5945876176133D),
						DMUL.class
						));
	
				if (matches2 != null) {
					//for (InstructionHandle ih : matches2) {
						found2 = true;
						//System.out.println(cg.getClassName()+ "." + mg.getName() + " " + matches2[0].getPosition());
					//}
				}
			}
		}
		
		if (found1 && found2)
			found = true;
	
		return found;
	}
	
	private boolean findDefinition(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
	
		boolean found = false;
		
		if (cg.getClassName().equals(Player.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new FCONST(0),
						new SynonymInstruction(new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(-5.0F), new PI_LDC_W(cpg).adjustType(Type.FLOAT).adjustValue(-5.0F)),
						new FCONST(0)
						));
	
				if (matches != null) {
					//for (InstructionHandle ih : matches) {
						searcher.gotoStart();
						
						InstructionHandle fieldIh = searcher.searchNext(true, CompareType.Opcode, new PI_GETFIELD(cpg).adjustTypeString(false, PlayerDef.get.getInternalName()).adjustClassName(cg.getClassName()));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
		
							Player.get.definition.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							findDefinition.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());

							found = true;
						}
					//}
				}
			}
		}
	
		return found;
	}
	
	private boolean findName(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
	
		boolean found = false;
		
		//if (cg.getClassName().equals(Player.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(40.0D),
						DDIV.class,
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("sin"),
						new PI_LDC2_W(cpg).adjustType(Type.DOUBLE).adjustValue(256.0D),
						DMUL.class
						));
	
				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);
						
						ih = searcher.searchPrev(true, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(GETSTATIC.class, GETFIELD.class));
						
						if (ih != null) {
							GETFIELD instr = (GETFIELD) ih.getNext().getInstruction();
		
							Player.get.name.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							findName.add(cg.getClassName() + "." + mg.getName(), ih.getPosition());

							found = true;
						}
					}
				}
			}
		//}
	
		return found;
	}
	
	private boolean findTitle(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
	
		boolean found = false;
		
		if (cg.getClassName().equals(Player.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.getArgumentTypes().length != 2)
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches1 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.lang.StringBuilder").adjustMethodName("append"),
						ALOAD.class,
						new PI_GETFIELD(cpg).adjustType(Type.getType(String.class)),
						new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.lang.StringBuilder").adjustMethodName("append")
						));
				
				InstructionHandle[] matches2 = null;
				if (matches1 != null) {
					matches2 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
							//new RejectedInstruction(new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.lang.StringBuilder").adjustMethodName("append")),
							ALOAD.class,
							new PI_GETFIELD(cpg).adjustType(Type.getType(String.class)),
							new RejectedInstruction(new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.lang.StringBuilder").adjustMethodName("append"))
							));
				}
	
				List<FieldData> fields_matches1 = new ArrayList<FieldData>();
				if (matches1 != null) {
					for (InstructionHandle ih : matches1) {
						searcher.gotoHandle(ih);

						GETFIELD instr3 = (GETFIELD) ih.getNext().getNext().getInstruction();
						FieldData field = FieldData.createTemp("title");
						field.set(instr3.getReferenceType(cpg).toString(), instr3.getFieldName(cpg), instr3.getSignature(cpg));
						fields_matches1.add(field);
					}
				}
				
				List<FieldData> fields_matches2 = new ArrayList<FieldData>();
				if (matches2 != null) {
					for (InstructionHandle ih : matches2) {
						searcher.gotoHandle(ih);

						GETFIELD instr2 = (GETFIELD) ih.getNext().getInstruction();
						FieldData field = FieldData.createTemp("title");
						field.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));
						fields_matches2.add(field);
					}
				}
				
				// check uses of the same field in each match collection, which would be the field we're looking for (there should be only one match)
				if (fields_matches1.size() > 0 && fields_matches2.size() > 0) {
					for (FieldData fd1 : fields_matches1) {
						for (FieldData fd2 : fields_matches2) {
							if (fd1.compareData(fd2)) {
								Player.get.title.setData(fd1);
								findTitle.add(cg.getClassName() + "." + mg.getName(), -1);
								
								found = true;
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Player.get.setInternalName(cg.getClassName());
			
			findTitle(cg);
		}
		
		findName(cg);
	}
	
	public void analyze_pass2(ClassGen cg) {
		if (cg.getClassName().equals(Player.get.getInternalName())) {
			if (PlayerDef.get.getInternalName() == null) {
				System.out.println("ERROR: PlayerAnalyzer: PlayerDef internal class name is null!");
				//return;
			} else
				findDefinition(cg);
		}
	}
}
