package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IF_ACMPNE;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.LUSHR;
import org.apache.bcel.generic.LXOR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Cache;
import data.CameraLocationData;
import data.Character;
import data.FieldData;
import data.Model;
import data.Node;
import data.Npc;
import data.ObjectDefinition;
import data.Player;
import data.PlayerDef;

public class PlayerDefAnalyzer extends Analyzer {
	
	public static PlayerDefAnalyzer get = new PlayerDefAnalyzer();

	CountInfoItem findModelHash = new CountInfoItem("findModelHash");
	CountInfoItem findFemale = new CountInfoItem("findFemale");
	CountInfoItem findEquipment = new CountInfoItem("findEquipment");
	
	public PlayerDefAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findModelHash, findFemale, findEquipment);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue(0x40000000), new PI_LDC_W(cpg).adjustValue(0x40000000)),
					IOR.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustValue(0xFFFFFFFFL),
					LXOR.class
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue(0x80000000), new PI_LDC_W(cpg).adjustValue(0x80000000)),
					IAND.class
			);
			
			final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue(0x3FFFFFFF), new PI_LDC_W(cpg).adjustValue(0x3FFFFFFF)),
					IAND.class
			);
			
			List<CompareInstruction[]> patterns = new ArrayList<>();
			patterns.add(pattern1);
			patterns.add(pattern2);
			patterns.add(pattern3);
			patterns.add(pattern4);
			
			InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
			
			if (matches != null && matches.length == patterns.size()) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findModelHash(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(PlayerDef.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				//if (mg.isStatic())
				//	continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(8),
						LUSHR.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC2_W(cpg).adjustType(Type.LONG).adjustValue(255L),
						LAND.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.searchNext(false, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustType(Type.LONG));
						
						if (fieldIh != null) {
							PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
							PlayerDef.get.modelHash.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findModelHash.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoStart();
							PlayerDef.get.modelHash.set(searcher.findNextGetMultiplier(false, false, PlayerDef.get.modelHash, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			PlayerDef.get.setInternalName(cg.getClassName());
			
			findModelHash(cg);
		}
	}

}
