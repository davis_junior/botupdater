package analyzers;

import java.io.ObjectOutputStream.PutField;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2D;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FLOAD;
import org.apache.bcel.generic.FMUL;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Player;
import data.Quaternion;

public class QuaternionAnalyzer extends Analyzer {
	
	public static QuaternionAnalyzer get = new QuaternionAnalyzer();

	CountInfoItem findX = new CountInfoItem("findX");
	CountInfoItem findY = new CountInfoItem("findY");
	CountInfoItem findZ = new CountInfoItem("findZ");
	CountInfoItem findA = new CountInfoItem("findA");
	
	public QuaternionAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findX, findY, findZ, findA);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;

		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						FLOAD.class,
						new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(0.5F),
						FMUL.class,
						F2D.class,
						new SynonymInstruction(new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("cos"), new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("sin"))
						));
	
				if (matches != null) {
					//for (InstructionHandle ih : matches) {
						found = true;
					//}
				}
			//}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Quaternion.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.getArgumentTypes().length != 0)
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						ALOAD.class,
						new FCONST(0),
						PUTFIELD.class,
						ALOAD.class,
						new FCONST(0),
						PUTFIELD.class,
						ALOAD.class,
						new FCONST(0),
						PUTFIELD.class,
						ALOAD.class,
						new FCONST(1),
						PUTFIELD.class
						));

				if (matches != null) {
					for (InstructionHandle ih : matches) {
						searcher.gotoHandle(ih);

						InstructionHandle fieldIH = null;
						PUTFIELD instr = null;
						
						fieldIH = searcher.getNext(true, PUTFIELD.class);
						instr = (PUTFIELD) fieldIH.getInstruction();
						Quaternion.get.z.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findZ.add(cg.getClassName() + "." + mg.getName(), fieldIH.getPosition());
						
						fieldIH = searcher.getNext(true, PUTFIELD.class);
						instr = (PUTFIELD) fieldIH.getInstruction();
						Quaternion.get.y.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findY.add(cg.getClassName() + "." + mg.getName(), fieldIH.getPosition());
						
						fieldIH = searcher.getNext(true, PUTFIELD.class);
						instr = (PUTFIELD) fieldIH.getInstruction();
						Quaternion.get.x.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findX.add(cg.getClassName() + "." + mg.getName(), fieldIH.getPosition());
						
						fieldIH = searcher.getNext(true, PUTFIELD.class);
						instr = (PUTFIELD) fieldIH.getInstruction();
						Quaternion.get.a.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						findA.add(cg.getClassName() + "." + mg.getName(), fieldIH.getPosition());
					}

					found = true;
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Quaternion.get.setInternalName(cg.getClassName());

			findFields(cg);
		}
	}

}
