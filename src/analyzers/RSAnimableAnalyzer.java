package analyzers;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISTORE;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Character;
import data.Player;
import data.RSAnimable;

public class RSAnimableAnalyzer extends Analyzer {
	
	public static RSAnimableAnalyzer get = new RSAnimableAnalyzer();

	CountInfoItem findName = new CountInfoItem("findName");
	CountInfoItem findTitle = new CountInfoItem("findTitle");
	
	public RSAnimableAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findName, findTitle);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			found = false;
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] patternMax = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("max")	
			);
			
			final CompareInstruction[] patternMin = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("min")	
			);
			
			if (searcher.searchNext(true, CompareType.Opcode, patternMax) != null
					&& searcher.searchNext(true, CompareType.Opcode, patternMin) != null
					&& searcher.searchNext(true, CompareType.Opcode, patternMax) != null
					&& searcher.searchNext(true, CompareType.Opcode, patternMin) != null) {
				
				InstructionHandle[] matches1 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						ALOAD.class,
						GETFIELD.class,
						new ICONST(1),
						new SynonymInstruction(IADD.class, ISUB.class),
						ISTORE.class
						));
				
				InstructionHandle[] matches2 = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new ICONST(1),
						ALOAD.class,
						GETFIELD.class,
						new SynonymInstruction(IADD.class, ISUB.class),
						ISTORE.class
						));
				
				int matchTotal = 0;
				
				if (matches1 != null) matchTotal += matches1.length;
				if (matches2 != null) matchTotal += matches2.length;

				if (matchTotal == 8) {
					//System.out.println(matchTotal);
					found = true;
					return true;
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			RSAnimable.get.setInternalName(cg.getClassName());

			//findNameAndTitle(cg);
		}
	}

}
