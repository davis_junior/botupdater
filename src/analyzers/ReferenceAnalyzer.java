package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.Type;

import data.NodeSub;
import data.Reference;

public class ReferenceAnalyzer extends Analyzer {
	
	public static ReferenceAnalyzer get = new ReferenceAnalyzer();

	@Override
	protected boolean verifyClass(ClassGen cg) {
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.isAbstract() && cg.getSuperclassName().equals(NodeSub.get.getInternalName())) {
			if (cg.getMethods().length > 2) {
				int finalIntCount = 0;
				int volatileBooleanCount = 0;
				int finalNonPrimitive = 0;
				
				for (Field f : cg.getFields()) {
					if (f.isStatic())
						continue;
					
					if (f.isFinal() && f.getType().equals(Type.INT))
						finalIntCount++;
					
					if (f.isVolatile() && f.getType().equals(Type.BOOLEAN))
						volatileBooleanCount++;
					
					if (f.isFinal() && f.getType().getSignature().contains("L"))
						finalNonPrimitive++;
				}
				
				if (finalIntCount >= 1 && volatileBooleanCount == 0 && finalNonPrimitive == 0) {
					
					int booleanReturnCount = 0;
					int objectReturnCount = 0;
					
					for (Method m : cg.getMethods()) {
						if (m.getReturnType().equals(Type.BOOLEAN))
							booleanReturnCount++;
						
						if (m.getReturnType().getSignature().equals("Ljava/lang/Object;"))
							objectReturnCount++;
					}
					
					if (booleanReturnCount >= 1 && objectReturnCount >= 1)
						found = true;
				}
			}
		}
	
		return found;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (NodeSub.get.getInternalName() == null) {
			System.out.println("ERROR: ReferenceAnalyzer: NodeSub internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			Reference.get.setInternalName(cg.getClassName());
		}
	}
}
