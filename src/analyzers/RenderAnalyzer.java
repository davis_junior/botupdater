package analyzers;

import java.awt.Canvas;
import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IF_ICMPLT;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LUSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_CHECKCAST;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKEINTERFACE;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_PUTFIELD;
import data.PlayerDef;
import data.Render;

public class RenderAnalyzer extends Analyzer {
	
	public static RenderAnalyzer get = new RenderAnalyzer();
	
	CountInfoItem findIndex = new CountInfoItem("findIndex");
	CountInfoItem findCacheIndex = new CountInfoItem("findCacheIndex");
	
	public RenderAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findIndex, findCacheIndex);
	}

	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;

		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
			InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
					new PI_INVOKEINTERFACE(cpg).adjustClassName("java.util.Enumeration").adjustMethodName("nextElement"),
					new PI_CHECKCAST(cpg).adjustType(Type.getType(Canvas.class))
					);

			if (matches != null) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findIndex(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Render.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(3),
						IF_ICMPLT.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.searchPrev(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(new ALOAD(0), new PI_GETFIELD(cpg).adjustType(Type.INT)));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getNext().getInstruction();
							Render.get.index.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findIndex.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoStart();
							Render.get.index.set(searcher.findNextGetMultiplier(false, false, Render.get.index, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findCacheIndex(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Render.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEINTERFACE(cpg).adjustClassName("java.util.Enumeration").adjustMethodName("hasMoreElements")
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_CHECKCAST(cpg).adjustTypeString(false, "java.awt.Canvas")
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.searchPrev(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(new ALOAD(0), new PI_GETFIELD(cpg).adjustType(Type.INT)));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getNext().getInstruction();
							Render.get.cacheIndex.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findCacheIndex.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoStart();
							Render.get.cacheIndex.set(searcher.findNextGetMultiplier(false, false, Render.get.cacheIndex, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Render.get.setInternalName(cg.getClassName());
			
			findIndex(cg);
			findCacheIndex(cg);
		}
	}
}
