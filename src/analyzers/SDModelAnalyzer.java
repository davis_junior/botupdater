package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.DescendingVisitor;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FCMPL;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IDIV;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import util.DependencyEmitter;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.FieldData;
import data.Model;
import data.OpenGLModel;
import data.SDModel;

public class SDModelAnalyzer extends Analyzer {
	
	public static SDModelAnalyzer get = new SDModelAnalyzer();
	
	CountInfoItem findIndices_Count = new CountInfoItem("findIndices");
	CountInfoItem findPoints_Count = new CountInfoItem("findPoints");
	
	public SDModelAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findIndices_Count, findPoints_Count);
	}

	@Override
	protected boolean verifyClass(ClassGen cg) {
		/*
		 * classes that extend Model: 3
		 * 		OpenGL Model
		 * 		SDModel
		 * 		? - uses ByteBuffer
		 */
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getSuperclassName().equals(Model.get.getInternalName())) {
			
		    DependencyEmitter visitor = new DependencyEmitter(cg.getJavaClass());
		    DescendingVisitor classWalker = new DescendingVisitor(cg.getJavaClass(), visitor);
		    classWalker.visit();
		    
		    //visitor.printDependencies();
		    
		    for (String dependency : visitor.getDependencies()) {
		    	if (dependency.equals("java/nio/ByteBuffer") || dependency.equals("jaggl/OpenGL"))
		    		return false;
		    }
		    
		    found = true;
		}
		
		return found;
	}

	private boolean findIndices(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(SDModel.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC(cpg).adjustValue(-999999.0F),
						FCMPL.class
						));
				
				int loop = 0;
				
				if (matches != null) {
					for (InstructionHandle ih : matches) {
						loop++;
						
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getPrev(true, GETFIELD.class);
						
						if (fieldIh != null) {
							if (loop == 1) { // get numFaces
								InstructionHandle numFacesIh = searcher.searchPrev(true, CompareType.Opcode, new PI_GETFIELD(cpg).adjustType(Type.INT));
								
								if (numFacesIh != null) {
									GETFIELD numFacesInstr = (GETFIELD) numFacesIh.getInstruction();
									SDModel.get.numFaces.set(cg.getClassName(), numFacesInstr.getFieldName(cpg), numFacesInstr.getFieldType(cpg).getSignature());
								}
							}
							
							GETFIELD fieldInstr = (GETFIELD) fieldIh.getInstruction();
							
							switch (loop) {
								case 1:
									SDModel.get.indices1.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
								case 2:
									SDModel.get.indices2.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
								case 3:
									SDModel.get.indices3.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
							}
						}		
					}
					
					if (loop == 3) {
						found = true;
						
						findIndices_Count.add(cg.getClassName() + "." + mg.getName(), matches[0].getPosition());	
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findPoints(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(SDModel.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				Searcher searcher = new Searcher(il);
				
				final CompareInstruction[] exclusionPattern1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(65535), new PI_LDC_W(cpg).adjustValue(65535))
				);
				
				final CompareInstruction[] exclusionPattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_SIPUSH(cpg).adjustValue(128)
				);
				
				List<CompareInstruction[]> exclusionPatterns = new ArrayList<>();
				exclusionPatterns.add(exclusionPattern1);
				exclusionPatterns.add(exclusionPattern2);
				
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						IMUL.class,
						new PI_BIPUSH(cpg).adjustValue(7),
						ISHR.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				
				InstructionHandle[][] exclusionMatches = searcher.searchAllFromStart(true, false, CompareType.Opcode, exclusionPatterns);
				
				if (exclusionMatches != null)
					continue;
				
				/*InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
						IMUL.class,
						new PI_SIPUSH(cpg).adjustValue(128),
						IDIV.class
						);*/
				
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				int loop = 0;
				
				if (matches != null && matches.length == patterns.size() && matches[0].length == 3) {
					for (InstructionHandle ih : matches[0]) {
						loop++;
						
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getPrev(true, GETFIELD.class);
						
						if (fieldIh != null) {
							if (loop == 1) { // get numVerticies
								InstructionHandle numVertIh = searcher.searchPrev(true, CompareType.Opcode, new PI_GETFIELD(cpg).adjustType(Type.INT));
								
								if (numVertIh != null) {
									GETFIELD numVertInstr = (GETFIELD) numVertIh.getInstruction();
									SDModel.get.numVertices.set(cg.getClassName(), numVertInstr.getFieldName(cpg), numVertInstr.getFieldType(cpg).getSignature());
								}
							}
							
							GETFIELD fieldInstr = (GETFIELD) fieldIh.getInstruction();
							
							switch (loop) {
								case 1:
									SDModel.get.xPoints.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
								case 2:
									SDModel.get.yPoints.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
								case 3:
									SDModel.get.zPoints.set(cg.getClassName(), fieldInstr.getFieldName(cpg), fieldInstr.getFieldType(cpg).getSignature());
									break;
							}
						}
					}
					
					if (loop == 3) {
						found = true;
						
						findPoints_Count.add(cg.getClassName() + "." + mg.getName(), matches[0][0].getPosition());
					}
				}
			}
		}
	
		return found;
	}
	
	@Override
	public void analyze(ClassGen cg) {
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: SDModelAnalyzer: Model internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			SDModel.get.setInternalName(cg.getClassName());
			
			findIndices(cg);
			findPoints(cg);
		}
	}

}
