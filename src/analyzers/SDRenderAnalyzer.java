package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DUP_X1;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.FDIV;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IINC;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISTORE;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import data.GameInfo;
import data.SDRender;

public class SDRenderAnalyzer extends Analyzer {
	
	public static SDRenderAnalyzer get = new SDRenderAnalyzer();

	CountInfoItem findClientViewport = new CountInfoItem("findClientViewport");
	CountInfoItem findXMultiplier = new CountInfoItem("findXMultiplier");
	CountInfoItem findYMultiplier = new CountInfoItem("findYMultiplier");
	CountInfoItem findAbsoluteX = new CountInfoItem("findAbsoluteX");
	CountInfoItem findAbsoluteY = new CountInfoItem("findAbsoluteY");
	
	public SDRenderAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findClientViewport, findXMultiplier, findYMultiplier, findAbsoluteX, findAbsoluteY);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;

		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
			InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
					IINC.class,
					new PI_LDC(cpg).adjustType(Type.FLOAT).adjustValue(2.14748365E9F)
					));

			if (matches != null) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findClientViewport(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(SDRender.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				/*final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(16),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(32),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(1),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(2),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(4),
						IOR.class,
						ISTORE.class
				};
				
				final CompareInstruction[] pattern6 = CompareInstruction.Methods.toCompareInstructions(
						new PI_BIPUSH(cpg).adjustValue(8),
						IOR.class,
						ISTORE.class
				};
				
				List<Object[]> patterns = new ArrayList<Object[]>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				patterns.add(pattern3);
				patterns.add(pattern4);
				patterns.add(pattern5);
				patterns.add(pattern6);*/
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						ALOAD.class,
						GETFIELD.class,
						new PI_BIPUSH(cpg).adjustValue(12),
						ISHR.class,
						ISTORE.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					if (matches[0].length == 3) {
					//for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(matches[0][matches[0].length - 1].getNext());
						InstructionHandle fieldIh = searcher.getNext(true, GETFIELD.class);
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							SDRender.get.clientViewport.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findClientViewport.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());						
						}
					//}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findOtherFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(SDRender.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new FCONST(2),
						FDIV.class,
						DUP_X1.class,
						PUTFIELD.class
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					if (matches[0].length == 2) {
					//for (InstructionHandle ih : matches[0]) {
						searcher.gotoHandle(matches[0][0]);
						InstructionHandle xMultiplierIh = searcher.getNext(true, PUTFIELD.class);
						
						if (xMultiplierIh != null) {
							PUTFIELD instr = (PUTFIELD) xMultiplierIh.getInstruction();
							SDRender.get.xMultiplier.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findXMultiplier.add(cg.getClassName() + "." + mg.getName(), xMultiplierIh.getPosition());						
						}
						
						InstructionHandle yMultiplierIh = searcher.getNext(true, PUTFIELD.class);
						
						if (yMultiplierIh != null) {
							PUTFIELD instr = (PUTFIELD) yMultiplierIh.getInstruction();
							SDRender.get.yMultiplier.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findYMultiplier.add(cg.getClassName() + "." + mg.getName(), yMultiplierIh.getPosition());						
						}
						
						
						InstructionHandle absoluteXIh = searcher.getNext(true, PUTFIELD.class);
						
						if (absoluteXIh != null) {
							PUTFIELD instr = (PUTFIELD) absoluteXIh.getInstruction();
							SDRender.get.absoluteX.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findAbsoluteX.add(cg.getClassName() + "." + mg.getName(), absoluteXIh.getPosition());						
						}
						
						InstructionHandle absoluteYIh = searcher.getNext(true, PUTFIELD.class);
						
						if (absoluteYIh != null) {
							PUTFIELD instr = (PUTFIELD) absoluteYIh.getInstruction();
							SDRender.get.absoluteY.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findAbsoluteY.add(cg.getClassName() + "." + mg.getName(), absoluteYIh.getPosition());						
						}
					//}
					}
				}
			}
		}
	
		return found;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			SDRender.get.setInternalName(cg.getClassName());
			
			findClientViewport(cg);
			findOtherFields(cg);
		}
	}
}
