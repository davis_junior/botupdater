package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LADD;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LOR;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import report.Report;
import searcher.Comparator.CompareType;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.Model;
import data.Player;
import data.SettingData;
import data.Settings;
import data.SkillData;

public class SettingDataAnalyzer extends Analyzer {
	
	public static SettingDataAnalyzer get = new SettingDataAnalyzer();

	CountInfoItem findSettings = new CountInfoItem("findSettings");
	CountInfoItem findSkillArray = new CountInfoItem("findSkillArray");
	
	public SettingDataAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findSettings, findSkillArray);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		List<Field> settingsFields = new ArrayList<>();
		List<Field> skillArrayFields = new ArrayList<>();
		
		for (Field field : cg.getFields()) {
			if (field.getType().toString().equals(Settings.get.getInternalName())) {
				settingsFields.add(field);
			} else if (field.getType().toString().equals(SkillData.get.getInternalName() + "[]")) {
				skillArrayFields.add(field);
			}
		}
		
		if (settingsFields.size() > 0 && skillArrayFields.size() > 0) {
			for (Field field : settingsFields) {
				SettingData.get.settings.set(cg.getClassName(), field.getName(), field.getSignature());
				findSettings.add(cg.getClassName() + ".<FIELDS>", 0);
			}
			
			for (Field field : skillArrayFields) {
				SettingData.get.skillArray.set(cg.getClassName(), field.getName(), field.getSignature());
				findSkillArray.add(cg.getClassName() + ".<FIELDS>", 0);
			}
			
			found = true;
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Settings.get.getInternalName() == null) {
			System.out.println("ERROR: SettingDataAnalyzer: Settings internal class name is null!");
			return;
		}
		
		if (SkillData.get.getInternalName() == null) {
			System.out.println("ERROR: SettingDataAnalyzer: SkillData internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			SettingData.get.setInternalName(cg.getClassName());
		}
	}

}
