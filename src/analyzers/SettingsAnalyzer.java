package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LADD;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LOR;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.GameInfo;
import data.Player;
import data.Settings;

public class SettingsAnalyzer extends Analyzer {
	
	public static SettingsAnalyzer get = new SettingsAnalyzer();

	CountInfoItem findData = new CountInfoItem("findData");
	
	public SettingsAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findData);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean found1 = false;
		boolean found2 = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1_1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustValue(0x3FFFFFFFFFFFFFFFL),
					LAND.class
			);
			
			final CompareInstruction[] pattern1_2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustValue(0x4000000000000000L),
					LAND.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1_1);
			patterns1.add(pattern1_2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				//for (InstructionHandle ih : matches) {
					found1 = true;
				//}
			}
			
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_LDC2_W(cpg).adjustValue(500L),
					LADD.class
			);
			
			List<CompareInstruction[]> patterns2 = new ArrayList<>();
			patterns2.add(pattern2);
			
			InstructionHandle[][] matches2 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns2);
			
			if (matches2 != null && matches2.length == patterns2.size()) {
				//for (InstructionHandle ih : matches) {
					found2 = true;
				//}
			}
			//}
		}
		
		if (found1 && found2)
			found = true;
	
		return found;
	}
	
	private boolean findData(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(Settings.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC2_W(cpg).adjustValue(500L),
						LADD.class
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_LDC2_W(cpg).adjustValue(0x4000000000000000L),
						LOR.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					//for (InstructionHandle ih : matches1[0]) {
						searcher.gotoStart();
						InstructionHandle fieldIh = searcher.searchNext(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(Settings.get.getInternalName()).adjustTypeString(false, "int[]"));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							Settings.get.data.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findData.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						}
					//}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Settings.get.setInternalName(cg.getClassName());

			findData(cg);
		}
	}

}
