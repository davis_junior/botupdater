package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_PUTFIELD;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.FieldData;
import data.GameInfo;
import data.Player;
import data.SkillData;

public class SkillDataAnalyzer extends Analyzer {
	
	public static SkillDataAnalyzer get = new SkillDataAnalyzer();

	CountInfoItem findExperience = new CountInfoItem("findExperience");
	CountInfoItem findRealLevel = new CountInfoItem("findRealLevel");
	CountInfoItem findCurrentLevel = new CountInfoItem("findCurrentLevel");
	
	public SkillDataAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findExperience, findRealLevel, findCurrentLevel);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		int intCount = 0;
		for (Field f : cg.getFields()) {
			if (f.getType().equals(Type.INT))
				intCount++;
		}
		
		if (intCount < 3)
			return false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue(2000000000), new PI_LDC_W(cpg).adjustValue(2000000000)),
					IfInstruction.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
		}
	
		return found;
	}
	
	private boolean findFields(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		boolean found1 = false;
		boolean found2 = false;
		boolean found3 = false;
		
		if (cg.getClassName().equals(SkillData.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				// same as verifyClass() pattern
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new SynonymInstruction(new PI_LDC(cpg).adjustValue(2000000000), new PI_LDC_W(cpg).adjustValue(2000000000)),
						IfInstruction.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.searchNext(false, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustClassName(SkillData.get.getInternalName()).adjustType(Type.INT));
						
						if (fieldIh != null) {
							PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
							SkillData.get.experience.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found1 = true;
							
							findExperience.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoStart();
							SkillData.get.experience.set(searcher.findNextGetMultiplier(false, false, SkillData.get.experience, cpg));
						}
					}
				}
			}
			
			
			if (found1) {
				for (MethodGen mg : BotUpdater.methods.get(cg)) {
					if (mg.isStatic())
						continue;
					
					InstructionList il = mg.getInstructionList();
					if (il == null || il.getLength() == 0)
						continue;
					
					final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
							new PI_PUTFIELD(cpg).adjustClassName(SkillData.get.getInternalName()).adjustType(Type.INT)
					);
					
					final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
							new PI_GETFIELD(cpg).adjustClassName(SkillData.get.getInternalName()).adjustType(Type.INT)
					);
					
					final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
							new PI_GETFIELD(cpg).adjustClassName(SkillData.get.getInternalName()).adjustType(Type.BOOLEAN)
					);
					
					List<CompareInstruction[]> patterns1 = new ArrayList<>();
					patterns1.add(pattern1);
					patterns1.add(pattern2);
					patterns1.add(pattern3);
					
					Searcher searcher = new Searcher(il);
					InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
					
					if (matches1 != null && matches1.length == patterns1.size()) {
						boolean uniformPutFields = true;
						boolean uniformDifferentGetFields = true;
						FieldData currentPutField = null;
						FieldData currentGetField = null;
						
						// check if all put fields are the same field
						for (InstructionHandle ih : matches1[0]) {
							InstructionHandle fieldIh = ih;
							
							if (fieldIh != null) {
								PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
								FieldData tempField = FieldData.createTemp("realLevel");
								tempField.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
								
								if (currentPutField != null) {
									if (!currentPutField.compareData(tempField)) {
										uniformPutFields = false;
										break;
									}
								}
								
								currentPutField = tempField;
							}
						}
						
						if (uniformPutFields) {
							// check if all get fields are the same and they are different from the put field
							for (InstructionHandle ih : matches1[1]) {
								InstructionHandle fieldIh = ih;
								
								if (fieldIh != null) {
									GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
									FieldData tempField = FieldData.createTemp();
									tempField.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
									
									if (currentGetField != null) {
										if (!currentGetField.compareData(tempField)) {
											uniformDifferentGetFields = false;
											break;
										}
										
										if (currentGetField.compareData(currentPutField)) {
											uniformDifferentGetFields = false;
											break;
										}
									}
									
									currentGetField = tempField;
								}
							}
						}
						
						if (uniformPutFields && uniformDifferentGetFields) {
							SkillData.get.realLevel.setData(currentPutField);				
							found2 = true;
							
							findRealLevel.add(cg.getClassName() + "." + mg.getName(), -1);
							
							// multiplier is searched in the next loop as getfield
						}
					}
				}
			}
			
			if (found2) {
				for (MethodGen mg : BotUpdater.methods.get(cg)) {
					if (mg.isStatic())
						continue;
					
					InstructionList il = mg.getInstructionList();
					if (il == null || il.getLength() == 0)
						continue;
					
					Searcher searcher = new Searcher(il);
					
					//
					// find multiplier for realLevel
					//
					InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(SkillData.get.getInternalName()).adjustFieldName(SkillData.get.realLevel.getInternalName()).adjustSignature(SkillData.get.realLevel.getSignature()));
					
					if (matches != null) {
						searcher.gotoStart();
						SkillData.get.realLevel.set(searcher.findNextGetMultiplier(false, false, SkillData.get.realLevel, cpg));
					}
					
					
					//
					// find currentLevel
					//
					if (mg.getName().equals("<init>")) {
						// sort through putfields, the one that isn't already identified is currrentLevel
						matches = searcher.searchAllFromStart(false, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustClassName(SkillData.get.getInternalName()).adjustType(Type.INT));
						
						for (InstructionHandle ih : matches) {
							InstructionHandle fieldIh = ih;
							
							if (fieldIh != null) {
								PUTFIELD instr = (PUTFIELD) fieldIh.getInstruction();
								FieldData tempField = FieldData.createTemp("currentLevel");
								tempField.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
								
								if (tempField.compareData(SkillData.get.realLevel) || tempField.compareData(SkillData.get.experience)) {
									continue;
								} else {
									SkillData.get.currentLevel.setData(tempField);
									found3 = true;
									
									findCurrentLevel.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
									
									// multiplier is searched in the next loop as getfield
								}
							}
						}
					}
				}
			}
			
			if (found3) {
				for (MethodGen mg : BotUpdater.methods.get(cg)) {
					if (mg.isStatic())
						continue;
					
					InstructionList il = mg.getInstructionList();
					if (il == null || il.getLength() == 0)
						continue;
					
					Searcher searcher = new Searcher(il);
					
					//
					// find multiplier for currentLevel
					//
					InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName(SkillData.get.getInternalName()).adjustFieldName(SkillData.get.currentLevel.getInternalName()).adjustSignature(SkillData.get.currentLevel.getSignature()));
					
					if (matches != null) {
						searcher.gotoStart();
						SkillData.get.currentLevel.set(searcher.findNextGetMultiplier(false, false, SkillData.get.currentLevel, cpg));
					}
				}
			}
		}
		
		if (found1 && found2 && found3)
			found = true;
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			SkillData.get.setInternalName(cg.getClassName());

			findFields(cg);
		}
	}

}
