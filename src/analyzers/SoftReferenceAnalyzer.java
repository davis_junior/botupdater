package analyzers;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;

import data.Model;
import data.Reference;
import data.SoftReference;

public class SoftReferenceAnalyzer extends Analyzer {
	
	public static SoftReferenceAnalyzer get = new SoftReferenceAnalyzer();

	@Override
	protected boolean verifyClass(ClassGen cg) {
		//ConstantPoolGen cpg = cg.getConstantPool();

		if (!cg.getSuperclassName().equals(Reference.get.getInternalName()))
			return false;
		
		boolean found = false;
		
		for (Field f : cg.getFields()) {
			if (!f.isStatic() && f.getType().getSignature().equals("Ljava/lang/ref/SoftReference;")) {
				SoftReference.get.softReference.set(cg.getClassName(), f.getName(), f.getSignature());
				found = true;
			}
		}
		
		return found;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (Reference.get.getInternalName() == null) {
			System.out.println("ERROR: SoftReferenceAnalyzer: Reference internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			SoftReference.get.setInternalName(cg.getClassName());
		}
	}
}
