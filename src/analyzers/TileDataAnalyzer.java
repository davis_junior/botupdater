package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.InstructionClass;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_SIPUSH;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Character;
import data.GameInfo;
import data.GroundBytes;
import data.GroundInfo;
import data.Player;
import data.TileData;

public class TileDataAnalyzer extends Analyzer {
	
	public static TileDataAnalyzer get = new TileDataAnalyzer();

	CountInfoItem findHeights = new CountInfoItem("findHeights");
	
	public TileDataAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findHeights);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("max")	
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("min")
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new ICONST(1),
					ISUB.class
			);
			
			final CompareInstruction[] pattern4_1 = CompareInstruction.Methods.toCompareInstructions(
					new ICONST(1),
					IADD.class
			);
			
			final CompareInstruction[] pattern4_2 = CompareInstruction.Methods.toCompareInstructions(
					new ICONST(1),
					ILOAD.class,
					IADD.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			patterns1.add(pattern3);
			
			List<CompareInstruction[]> patterns2 = new ArrayList<>();
			patterns2.add(pattern4_1);
			patterns2.add(pattern4_2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				InstructionHandle[][] matches2 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns2);

				if (matches2 != null) {
					int matches4Count = 0;
					if (matches2[0] != null)
						matches4Count += matches2[0].length;
					if (matches2[1] != null)
						matches4Count += matches2[1].length;
		
					if (matches1[2].length == 6 && matches4Count == 2) {
						//for (InstructionHandle ih : matches) {
							found = true;
							
							//System.out.println(cg.getClassName() + " " + matches3.length + ", " + matches4Count);
						//}
					}
				}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findHegihts(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(TileData.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("max")	
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("min")	
				);
				
				List<CompareInstruction[]> patterns = new ArrayList<>();
				patterns.add(pattern1);
				patterns.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns);
				
				if (matches != null && matches.length == patterns.size()) {
					if (matches[0].length == 2 && matches[1].length == 2) {
						InstructionHandle fieldIh = searcher.searchFromStart(true, CompareType.Opcode, new InstructionClass(GETFIELD.class));
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							TileData.get.heights.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findHeights.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());						
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			TileData.get.setInternalName(cg.getClassName());

			findHegihts(cg);
		}
	}

}
