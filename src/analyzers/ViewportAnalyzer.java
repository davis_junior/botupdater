package analyzers;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.F2D;
import org.apache.bcel.generic.FADD;
import org.apache.bcel.generic.FALOAD;
import org.apache.bcel.generic.FLOAD;
import org.apache.bcel.generic.FMUL;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IINC;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_LDC;
import data.GameInfo;
import data.SDRender;
import data.Viewport;

public class ViewportAnalyzer extends Analyzer {
	
	public static ViewportAnalyzer get = new ViewportAnalyzer();
	
	CountInfoItem findFloats = new CountInfoItem("findFloats");
	
	public ViewportAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findFloats);
	}

	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;

		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 2) {
			/*InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, 
					FLOAD.class,
					FLOAD.class,
					FMUL.class,
					FLOAD.class,
					FLOAD.class,
					FMUL.class,
					FADD.class,
					FLOAD.class,
					FLOAD.class,
					FMUL.class,
					FADD.class,
					F2D.class,
					new PI_INVOKESTATIC(cpg).adjustClassName("java.lang.Math").adjustMethodName("sqrt")
					);*/
			
			InstructionHandle[] matches = searcher.searchAllFromStart(false, CompareType.Opcode, CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					GETFIELD.class,
					new SynonymInstruction(ICONST.class, BIPUSH.class),
					FALOAD.class,
					new ALOAD(0),
					GETFIELD.class,
					new SynonymInstruction(ICONST.class, BIPUSH.class),
					FALOAD.class,
					FMUL.class
					));

			if (matches != null && matches.length > 20) { // 24, 88
				//for (InstructionHandle ih : matches) {
					found = true;
					//System.out.println(matches.length);
					
					searcher.gotoStart();
					InstructionHandle fieldIh = searcher.getNext(true, GETFIELD.class);
					
					if (fieldIh != null) {
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						Viewport.get.floats.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
						found = true;
						
						findFloats.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());						
					}
				//}

			}
			//}
		}
	
		return found;
	}

	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			Viewport.get.setInternalName(cg.getClassName());
		}
	}
}
