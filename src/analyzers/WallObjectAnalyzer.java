package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_CHECKCAST;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.AbstractBoundary;
import data.AbstractDefinition;
import data.AbstractFloorObject;
import data.AbstractWallObject;
import data.BoundaryObject;
import data.FloorObject;
import data.Model;
import data.ObjectDefLoader;
import data.ObjectDefinition;
import data.WallObject;

public class WallObjectAnalyzer extends Analyzer {
	
	public static WallObjectAnalyzer get = new WallObjectAnalyzer();

	CountInfoItem findId = new CountInfoItem("findId");
	CountInfoItem findModel = new CountInfoItem("findModel");
	CountInfoItem findDefinitionLoader = new CountInfoItem("findDefinitionLoader");
	CountInfoItem findAnimationId = new CountInfoItem("findAnimationId");
	CountInfoItem findFace = new CountInfoItem("findFace");
	
	public WallObjectAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findId, findModel, findDefinitionLoader, findAnimationId, findFace);
	}
	
	// same as BoundaryObject.verifyClass(...)
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getSuperclassName().equals(AbstractWallObject.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue(262144), new PI_LDC_W(cpg).adjustValue(262144)),
					new ICONST(1)
			);
			
			InstructionHandle[] matches1 = searcher.searchAllFromStart(false, CompareType.Opcode, pattern1);
			
			if (matches1 != null) {
				//for (InstructionHandle ih : matches) {
					found = true;
				//}
			}
			//}
		}
	
		return found;
	}
	
	// same as BoundaryObject.verifyClass(...)
	private boolean findFields(ClassGen cg) {
		if (!cg.getClassName().equals(WallObject.get.getInternalName()))
			return false;
		
		boolean found = false;
		
		for (Field f : cg.getFields()) {
			if (f.getType().toString().equals(ObjectDefLoader.get.getInternalName())) {
				WallObject.get.definitionLoader.set(cg.getClassName(), f.getName(), f.getSignature());					
				found = true;
				
				findDefinitionLoader.add(cg.getClassName() + ".<FIELDS>", 0);
			}
		}
		
		return found;
	}
	
	// same as BoundaryObject.verifyClass(...)
	private boolean findId(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(WallObject.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEVIRTUAL(cpg).adjustTypeString(false, AbstractDefinition.get.getInternalName()),
						new PI_CHECKCAST(cpg).adjustTypeString(false, ObjectDefinition.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getPrev(false, GETFIELD.class);
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							WallObject.get.id.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findId.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoHandle(ih);
							WallObject.get.id.set(searcher.findPrevGetMultiplier(false, false, WallObject.get.id, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findModel(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(WallObject.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustClassName(WallObject.get.getInternalName()).adjustTypeString(false, Model.get.getInternalName())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							WallObject.get.model.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findModel.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
						}
					}
				}
			}
		}
	
		return found;
	}
	
	// TODO: could use actual pattern for animationId instead of searching previous from face
	// similar to AnimatedObject.findAnimationIdAndFace()
	private boolean findAnimationIdAndFace(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		if (cg.getClassName().equals(WallObject.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				
				final CompareInstruction[] pattern1_1 = CompareInstruction.Methods.toCompareInstructions(
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustType(Type.BYTE),
						new ICONST(4),
						IADD.class
				);
				
				final CompareInstruction[] pattern1_2 = CompareInstruction.Methods.toCompareInstructions(
						new ICONST(4),
						new ALOAD(0),
						new PI_GETFIELD(cpg).adjustType(Type.BYTE),
						IADD.class
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1_1);
				patterns1.add(pattern1_2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(false, true, CompareType.Opcode, patterns1);
				
				if (matches1 != null) {
					InstructionHandle[] matches1combined = Util.combine(matches1[0], matches1[1]);
					
					for (InstructionHandle ih : matches1combined) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = searcher.getNext(false, GETFIELD.class);
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							WallObject.get.face.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findFace.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoHandle(ih);
							InstructionHandle animationIh = ih;
							while (animationIh != null) {
								animationIh = searcher.searchPrev(true, CompareType.Opcode, new PI_GETFIELD(cpg).adjustType(Type.BYTE));
								
								if (animationIh != null) {
									GETFIELD instr2 = (GETFIELD) animationIh.getInstruction();
									
									if (!instr2.getFieldName(cpg).equals(WallObject.get.face.getInternalName())) {
										WallObject.get.animationId.set(instr2.getReferenceType(cpg).toString(), instr2.getFieldName(cpg), instr2.getSignature(cpg));					
										found = true;
										
										findAnimationId.add(cg.getClassName() + "." + mg.getName(), animationIh.getPosition());
										
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (AbstractWallObject.get.getInternalName() == null) {
			System.out.println("ERROR: WallObjectAnalyzer: AbstractWallObject internal class name is null!");
			return;
		}
		
		if (verifyClass(cg)) {
			WallObject.get.setInternalName(cg.getClassName());
			
			if (Model.get.getInternalName() == null) {
				System.out.println("ERROR: WallObjectAnalyzer: Model internal class name is null!");
				//return;
			} else
				findModel(cg);
			
			findAnimationIdAndFace(cg);
		}
	}
	
	public void analyze_pass4(ClassGen cg) {
		if (AbstractWallObject.get.getInternalName() == null) {
			//System.out.println("ERROR: WallObjectAnalyzer: AbstractWallObject internal class name is null!");
			return;
		}
		
		
		if (AbstractDefinition.get.getInternalName() == null) {
			System.out.println("ERROR: WallObjectAnalyzer: AbstractDefinition internal class name is null!");
			//return;
		}
		if (ObjectDefinition.get.getInternalName() == null) {
			System.out.println("ERROR: WallObjectAnalyzer: ObjectDefinition internal class name is null!");
			//return;
		}
		else
			findId(cg);
	}
	
	public void analyze_ObjectDefLoader(ClassGen cg) {
		if (WallObject.get.getInternalName() == null) {
			//System.out.println("ERROR: WallObjectAnalyzer: WallObject internal class name is null!");
			//return;
		} else if (ObjectDefLoader.get.getInternalName() == null) {
			System.out.println("ERROR: WallObjectAnalyzer: ObjectDefLoader internal class name is null!");
			//return;
		} else if (cg.getClassName().equals(WallObject.get.getInternalName()))
			findFields(cg);
	}

}
