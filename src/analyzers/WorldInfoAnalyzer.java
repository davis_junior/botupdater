package analyzers;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DMUL;
import org.apache.bcel.generic.F2I;
import org.apache.bcel.generic.FCONST;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.I2F;
import org.apache.bcel.generic.I2S;
import org.apache.bcel.generic.IAND;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.L2I;
import org.apache.bcel.generic.LAND;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LSHR;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import report.Report;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.RejectedInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_InvokeInstruction;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_LDC_W;
import util.Util;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.BaseInfo;
import data.Character;
import data.FieldData;
import data.GameInfo;
import data.Player;
import data.WorldInfo;

public class WorldInfoAnalyzer extends Analyzer {
	
	public static WorldInfoAnalyzer get = new WorldInfoAnalyzer();

	CountInfoItem findHost = new CountInfoItem("findHost");
	CountInfoItem findWorld = new CountInfoItem("findWorld");
	CountInfoItem findPort1 = new CountInfoItem("findPort1");
	CountInfoItem findPort2 = new CountInfoItem("findPort2");
	
	public WorldInfoAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findHost, findWorld, findPort1, findPort2);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			Type returnType = mg.getReturnType();
			if (returnType == null || !returnType.getSignature().equals("Ljava/net/Socket;"))
				continue;
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName("java.net.InetAddress").adjustMethodName("getByName")
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustType(Type.STRING)
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_GETFIELD(cpg).adjustType(Type.INT) // ports
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			patterns1.add(pattern3);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size() 
					&& matches1[2].length == 4) {
				
				for (InstructionHandle ih : matches1[1]) {
					InstructionHandle fieldIh = ih.getNext();
					
					if (fieldIh != null) {
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						WorldInfo.get.host.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
						found = true;
						
						findHost.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
					}
				}
				
				// ports
				for (InstructionHandle ih : matches1[2]) {
					InstructionHandle fieldIh = ih;
					
					if (fieldIh != null) {
						GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
						FieldData fd = FieldData.createTemp();
						fd.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
						
						if (fd.compareData(WorldInfo.get.port1))
							continue;
						else if (WorldInfo.get.port1.getInternalName() == null) {
							WorldInfo.get.port1.setData(fd);
							found = true;
							
							findPort1.add(cg.getClassName() + "." + mg.getName(), -1);
							
							searcher.gotoStart();
							WorldInfo.get.port1.set(searcher.findNextGetMultiplier(false, false, WorldInfo.get.port1, cpg));
						} else if (WorldInfo.get.port2.getInternalName() == null) {
							WorldInfo.get.port2.setData(fd);
							found = true;
							
							findPort2.add(cg.getClassName() + "." + mg.getName(), -1);
							
							searcher.gotoStart();
							WorldInfo.get.port2.set(searcher.findNextGetMultiplier(false, false, WorldInfo.get.port2, cpg));
						}
					}
				}
			}
			//}
		}
	
		return found;
	}
	
	private boolean findWorld(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
	
		boolean found = false;
		
		if (WorldInfo.get.port1.getInternalName() == null || WorldInfo.get.port2.getInternalName() == null)
			return false;
		
		if (cg.getClassName().equals(WorldInfo.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustType(Type.INT)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							FieldData fd = FieldData.createTemp();
							fd.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							
							if (!fd.compareData(WorldInfo.get.port1) && !fd.compareData(WorldInfo.get.port2)) {
								WorldInfo.get.world.setData(fd);					
								found = true;
								
								findWorld.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
								
								searcher.gotoStart();
								WorldInfo.get.world.set(searcher.findNextGetMultiplier(false, false, WorldInfo.get.world, cpg));
							}
						}
					}
				}
			}
		}
	
		return found;
	}
	
	/*private boolean findWorld(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
	
		boolean found = false;
		
		if (cg.getClassName().equals(WorldInfo.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.lang.String").adjustMethodName("equals")
				);
				
				final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg).adjustType(Type.INT)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				patterns1.add(pattern2);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()
						&& matches1[1].length == 2) {
					for (InstructionHandle ih : matches1[1]) {
						searcher.gotoHandle(ih);
						InstructionHandle fieldIh = ih;
						
						if (fieldIh != null) {
							GETFIELD instr = (GETFIELD) fieldIh.getInstruction();
							WorldInfo.get.world.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));					
							found = true;
							
							findWorld.add(cg.getClassName() + "." + mg.getName(), fieldIh.getPosition());
							
							searcher.gotoStart();
							WorldInfo.get.world.set(searcher.findNextGetMultiplier(false, false, WorldInfo.get.world, cpg));
						}
					}
				}
			}
		}
	
		return found;
	}
	
	private boolean findPorts(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();
	
		boolean found = false;
		
		if (cg.getClassName().equals(WorldInfo.get.getInternalName())) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (mg.isStatic())
					continue;
				
				Type returnType = mg.getReturnType();
				if (returnType == null || !returnType.equals(Type.INT))
					continue;
				
				InstructionList il = mg.getInstructionList();
				if (il == null || il.getLength() == 0)
					continue;
				
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_GETFIELD(cpg)
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				Searcher searcher = new Searcher(il);
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()
						&& matches1[0].length == 3) {
					
					int booleanFields = 0;
					List<FieldData> intFields = new ArrayList<>();
					for (InstructionHandle ih : matches1[0]) {
						GETFIELD instr = (GETFIELD) ih.getInstruction();
						if (instr.getType(cpg).equals(Type.BOOLEAN))
							booleanFields++;
						else if (instr.getType(cpg).equals(Type.INT)) {
							FieldData fd = FieldData.createTemp();
							fd.set(instr.getReferenceType(cpg).toString(), instr.getFieldName(cpg), instr.getSignature(cpg));
							intFields.add(fd);
						}
					}
					
					if (booleanFields == 1 && intFields.size() == 2 && !intFields.get(0).compareData(intFields.get(1))) {
						WorldInfo.get.port1.setData(intFields.get(0));
						WorldInfo.get.port2.setData(intFields.get(1));
						found = true;
						
						findPort1.add(cg.getClassName() + "." + mg.getName(), -1);
						findPort2.add(cg.getClassName() + "." + mg.getName(), -1);
						
						searcher.gotoStart();
						WorldInfo.get.port1.set(searcher.findNextGetMultiplier(false, false, WorldInfo.get.port1, cpg));
						WorldInfo.get.port2.set(searcher.findNextGetMultiplier(false, false, WorldInfo.get.port2, cpg));
					}
				}
			}
		}
	
		return found;
	}*/
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			WorldInfo.get.setInternalName(cg.getClassName());

			findWorld(cg);
			//findPorts(cg);
		}
	}

}
