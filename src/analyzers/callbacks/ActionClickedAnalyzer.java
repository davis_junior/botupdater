package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.MenuItemNode;
import data.methods.Callbacks;

public class ActionClickedAnalyzer extends Analyzer {
	
	public static ActionClickedAnalyzer get = new ActionClickedAnalyzer();

	CountInfoItem findActionClick = new CountInfoItem("findActionClick");
	
	public ActionClickedAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findActionClick);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 3)
				continue;
			
			int menuItemNodeCount = 0;
			int intCount = 0;
			for (Type argType : argTypes) {
				if (argType.equals(Type.INT))
					intCount++;
				else if (argType.toString().equals(MenuItemNode.get.getInternalName()))
					menuItemNodeCount++;
			}
			
			if (menuItemNodeCount == 0)
				continue;
			
			if (intCount < 2)
				continue;
			
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue("Null"), new PI_LDC_W(cpg).adjustValue("Null"))
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				Callbacks.get.actionClicked.add(cg.getClassName(), mg.getName(), mg.getSignature());
				found = true;
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (MenuItemNode.get.getInternalName() == null) {
			System.out.println("ERROR: ActionClickAnalyzer: MenuItemNode internal class name is null!");
			return;
		}
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
