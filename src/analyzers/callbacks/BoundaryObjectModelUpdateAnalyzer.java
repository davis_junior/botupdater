package analyzers.callbacks;

import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import boot.BotUpdater;
import data.AnimableObject;
import data.BoundaryObject;
import data.Model;
import data.ObjectComposite;
import data.Render;
import data.methods.Callbacks;

public class BoundaryObjectModelUpdateAnalyzer extends Analyzer {
	
	public static BoundaryObjectModelUpdateAnalyzer get = new BoundaryObjectModelUpdateAnalyzer();

	CountInfoItem findModelUpdate = new CountInfoItem("findModelUpdate");
	
	public BoundaryObjectModelUpdateAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findModelUpdate);
	}
	
	// same as other object model update analyzers
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getClassName().equals(BoundaryObject.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			Type returnType = mg.getReturnType();
			if (returnType == null || !returnType.toString().equals(ObjectComposite.get.getInternalName()))
				continue;
			
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 3)
				continue;
			
			int renderCount = 0;
			int intCount = 0;
			int boolCount = 0;
			for (Type argType : argTypes) {
				if (argType.equals(Type.INT))
					intCount++;
				else if (argType.equals(Type.BOOLEAN))
					boolCount++;
				else if (argType.toString().equals(Render.get.getInternalName()))
					renderCount++;
			}
			
			if (renderCount == 0)
				continue;
			
			if (intCount < 1)
				continue;
			
			if (boolCount < 1)
				continue;
			
			Callbacks.get.boundaryObjectModelUpdate.add(cg.getClassName(), mg.getName(), mg.getSignature());
			found = true;
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (BoundaryObject.get.getInternalName() == null) {
			System.out.println("ERROR: BoundaryObjectModelUpdateAnalyzer: BoundaryObject internal class name is null!");
			return;
		}
		
		if (ObjectComposite.get.getInternalName() == null) {
			System.out.println("ERROR: BoundaryObjectModelUpdateAnalyzer: ObjectComposite internal class name is null!");
			return;
		}
		
		if (Model.get.getInternalName() == null) {
			System.out.println("ERROR: BoundaryObjectModelUpdateAnalyzer: Model internal class name is null!");
			return;
		}
		
		if (Render.get.getInternalName() == null) {
			System.out.println("ERROR: BoundaryObjectModelUpdateAnalyzer: Render internal class name is null!");
			return;
		}
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
