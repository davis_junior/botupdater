package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.IASTORE;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LADD;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_LDC2_W;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Settings;
import data.methods.Callbacks;

public class ChangeSettingAnalyzer extends Analyzer {
	
	public static ChangeSettingAnalyzer get = new ChangeSettingAnalyzer();

	CountInfoItem findChangeSetting = new CountInfoItem("findChangeSetting");
	
	public ChangeSettingAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findChangeSetting);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getClassName().equals(Settings.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 2)
				continue;
			
			//int unknownCount = 0;
			int intCount = 0;
			for (Type argType : argTypes) {
				if (argType.equals(Type.INT))
					intCount++;
				//else if (argType.toString().equals(unknown.get.getInternalName()))
				//	unknownCount++;
			}
			
			//if (unknownCount == 0)
			//	continue;
			
			if (intCount < 1)
				continue;
			
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC2_W(cpg).adjustValue(500L)),
					LADD.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustClassName(data.Settings.get.getInternalName()).adjustFieldName(data.Settings.get.data.getInternalName()).adjustSignature(data.Settings.get.data.getSignature())
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					ILOAD.class,
					IASTORE.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			patterns1.add(pattern3);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				Callbacks.get.changeSetting.add(cg.getClassName(), mg.getName(), mg.getSignature());
				found = true;
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Settings.get.getInternalName() == null) {
			System.out.println("ERROR: ChangeSettingAnalyzer: Settings internal class name is null!");
			return;
		}
		
		if (Settings.get.data.getInternalName() == null) {
			System.out.println("ERROR: ChangeSettingAnalyzer: Settings data internal field name is null!");
			return;
		}
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
