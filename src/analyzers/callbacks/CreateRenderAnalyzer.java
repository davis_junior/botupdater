package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ARETURN;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.IASTORE;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LADD;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_LDC2_W;
import searcher.pi.PI_NEW;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Render;
import data.Settings;
import data.methods.Callbacks;

public class CreateRenderAnalyzer extends Analyzer {
	
	public static CreateRenderAnalyzer get = new CreateRenderAnalyzer();

	CountInfoItem findCreateRender = new CountInfoItem("findCreateRender");
	
	public CreateRenderAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findCreateRender);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (!mg.isStatic())
				continue;
			
			if (!mg.isSynchronized())
				continue;
			
			Type returnType = mg.getReturnType();
			if (returnType == null || !returnType.getSignature().equals("L" + Render.get.getInternalName() + ";"))
				continue;
			
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 11)
				continue;
			
			boolean foundCanvas = false;
			int intCount = 0;
			for (Type argType : argTypes) {
				if (argType.getSignature().equals("Ljava/awt/Canvas;"))
					foundCanvas = true;
				else if (argType.equals(Type.INT))
					intCount++;
			}
			
			if (!foundCanvas)
				continue;
			
			if (intCount < 4)
				continue;
			
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					IfInstruction.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_NEW(cpg).adjustTypeString(false, "java.lang.IllegalArgumentException")
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					ARETURN.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			patterns1.add(pattern3);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				if (matches1[0].length >= 3 && matches1[2].length >= 3) {
					Callbacks.get.createRender.add(cg.getClassName(), mg.getName(), mg.getSignature());
					found = true;
				}
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Render.get.getInternalName() == null) {
			System.out.println("ERROR: CreateRenderAnalyzer: Render internal class name is null!");
			return;
		}
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
