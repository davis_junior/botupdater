package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_ConstantPushInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.OpenGLRender;
import data.SDRender;
import data.Viewport;
import data.methods.Callbacks;

public class GLSwapBuffersAnalyzer extends Analyzer {
	
	public static GLSwapBuffersAnalyzer get = new GLSwapBuffersAnalyzer();

	CountInfoItem findSwapBuffers = new CountInfoItem("findSwapBuffers");
	
	public GLSwapBuffersAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findSwapBuffers);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		//if (!cg.getClassName().equals(OpenGLRender.get.getInternalName()))
		//	return false;
		
		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (!mg.getReturnType().equals(Type.INT))
				continue;
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKEVIRTUAL(cpg).adjustClassName("jaggl.OpenGL").adjustMethodName("swapBuffers")
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				Callbacks.get.glSwapBuffers.add(cg.getClassName(), mg.getName(), mg.getSignature());
				found = true;
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		/*if (OpenGLRender.get.getInternalName() == null) {
			System.out.println("ERROR: GLSwapBuffersAnalyzer: OpenGLRender internal class name is null!");
			return;
		}*/
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
