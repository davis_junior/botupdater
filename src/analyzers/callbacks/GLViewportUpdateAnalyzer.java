package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_ConstantPushInstruction;
import searcher.pi.PI_GETFIELD;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.OpenGLRender;
import data.SDRender;
import data.Viewport;
import data.methods.Callbacks;

public class GLViewportUpdateAnalyzer extends Analyzer {
	
	public static GLViewportUpdateAnalyzer get = new GLViewportUpdateAnalyzer();

	CountInfoItem findViewportUpdate = new CountInfoItem("findViewportUpdate");
	
	public GLViewportUpdateAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findViewportUpdate);
	}
	
	// same as others -- render viewport update verifyClass()
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		if (!cg.getClassName().equals(OpenGLRender.get.getInternalName()))
			return false;
		
		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (!mg.getReturnType().equals(Type.INT))
				continue;
			
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 6)
				continue;
			
			int intCount = 0;
			for (Type argType : argTypes) {
				if (argType.equals(Type.INT))
					intCount++;
			}
			
			if (intCount < 6)
				continue;
			
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] patternViewportFloats = CompareInstruction.Methods.toCompareInstructions(
					new ALOAD(0),
					new PI_GETFIELD(cpg).adjustFieldName(OpenGLRender.get.clientViewport.getInternalName()),
					new PI_GETFIELD(cpg).adjustFieldName(Viewport.get.floats.getInternalName())
			);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_ConstantPushInstruction<>(cpg).adjustValue(0x10),
					IOR.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_ConstantPushInstruction<>(cpg).adjustValue(0x20),
					IOR.class
			);
			
			final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
					new PI_ConstantPushInstruction<>(cpg).adjustValue(0x1),
					IOR.class
			);
			
			final CompareInstruction[] pattern4 = CompareInstruction.Methods.toCompareInstructions(
					new PI_ConstantPushInstruction<>(cpg).adjustValue(0x2),
					IOR.class
			);
			
			final CompareInstruction[] pattern5 = CompareInstruction.Methods.toCompareInstructions(
					new PI_ConstantPushInstruction<>(cpg).adjustValue(0x4),
					IOR.class
			);
			
			final CompareInstruction[] pattern6 = CompareInstruction.Methods.toCompareInstructions(
					new PI_ConstantPushInstruction<>(cpg).adjustValue(0x8),
					IOR.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(patternViewportFloats);
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			patterns1.add(pattern3);
			patterns1.add(pattern4);
			patterns1.add(pattern5);
			patterns1.add(pattern6);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				Callbacks.get.glViewportUpdate.add(cg.getClassName(), mg.getName(), mg.getSignature());
				found = true;
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (OpenGLRender.get.getInternalName() == null) {
			System.out.println("ERROR: GLViewportUpdateAnalyzer: OpenGLRender internal class name is null!");
			return;
		}
		
		if (Viewport.get.getInternalName() == null) {
			System.out.println("ERROR: GLViewportUpdateAnalyzer: Viewport internal class name is null!");
			return;
		}
		
		if (OpenGLRender.get.clientViewport.getInternalName() == null) {
			System.out.println("ERROR: GLViewportUpdateAnalyzer: OpenGLRender clientViewport internal field name is null!");
			return;
		}
		
		if (Viewport.get.floats.getInternalName() == null) {
			System.out.println("ERROR: GLViewportUpdateAnalyzer: Viewport floats internal field name is null!");
			return;
		}
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
