package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_GETSTATIC;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Client;
import data.MenuItemNode;
import data.methods.Callbacks;

public class ItemTablePutAnalyzer extends Analyzer {
	
	public static ItemTablePutAnalyzer get = new ItemTablePutAnalyzer();

	CountInfoItem findPut = new CountInfoItem("findPut");
	
	public ItemTablePutAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findPut);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 5)
				continue;
			
			int intCount = 0;
			int booleanCount = 0;
			for (Type argType : argTypes) {
				if (argType.equals(Type.INT))
					intCount++;
				else if (argType.equals(Type.BOOLEAN))
					booleanCount++;
			}
			
			if (intCount < 4)
				continue;
			
			if (booleanCount < 1)
				continue;
			
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue(Integer.MIN_VALUE), new PI_LDC_W(cpg).adjustValue(Integer.MIN_VALUE))
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_GETSTATIC(cpg).adjustClassName(Client.get.itemTables.getInternalClassName()).adjustFieldName(Client.get.itemTables.getInternalName())
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				Callbacks.get.itemTablePut.add(cg.getClassName(), mg.getName(), mg.getSignature());
				found = true;
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Client.get.itemTables.getInternalName() == null) {
			System.out.println("ERROR: ItemTablePutAnalyzer: Client.itemTables internal field name is null!");
			return;
		}
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
