package analyzers.callbacks;

import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import analyzers.Analyzer;
import boot.BotUpdater;
import data.methods.Callbacks;

public class JaclibNotifyAnalyzer extends Analyzer {
	
	public static JaclibNotifyAnalyzer get = new JaclibNotifyAnalyzer();

	//CountInfoItem findViewportUpdate = new CountInfoItem("findViewportUpdate");
	
	public JaclibNotifyAnalyzer() {
		//countInfo = new CountInfo(getClass().getName());
		//countInfo.add(findViewportUpdate);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		//ConstantPoolGen cpg = cg.getConstantPool();

		if (!cg.getSuperclassName().equals("jaclib.ping.IcmpService"))
			return false;
		
		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (mg.isStatic())
				continue;
			
			if (!mg.getName().equals("notify"))
				continue;
			
			if (!mg.getReturnType().equals(Type.VOID))
				continue;
			
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length == 0)
				continue;
			
			int intCount = 0;
			for (Type argType : argTypes) {
				if (argType.equals(Type.INT))
					intCount++;
			}
			
			if (intCount == 1) {
				Callbacks.get.jaclibNotify1.add(cg.getClassName(), mg.getName(), mg.getSignature());
				found = true;
			} else if (intCount == 3) {
				Callbacks.get.jaclibNotify2.add(cg.getClassName(), mg.getName(), mg.getSignature());
				found = true;
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (verifyClass(cg)) {
			
		}
	}

}