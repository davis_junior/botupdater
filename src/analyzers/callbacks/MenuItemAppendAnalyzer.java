package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import searcher.pi.PI_NEW;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.MenuGroupNode;
import data.MenuItemNode;
import data.methods.Callbacks;

public class MenuItemAppendAnalyzer extends Analyzer {
	
	public static MenuItemAppendAnalyzer get = new MenuItemAppendAnalyzer();

	CountInfoItem findMenuItemAppend = new CountInfoItem("findMenuItemAppend");
	
	public MenuItemAppendAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findMenuItemAppend);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 1)
				continue;
			
			int menuItemNodeCount = 0;
			for (Type argType : argTypes) {
				if (argType.toString().equals(MenuItemNode.get.getInternalName()))
					menuItemNodeCount++;
			}
			
			if (menuItemNodeCount == 0)
				continue;
			
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue(""), new PI_LDC_W(cpg).adjustValue(""))
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_NEW(cpg).adjustTypeString(false, MenuGroupNode.get.getInternalName())
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				if (matches1[0].length == 1 && matches1[1].length == 2) {
					Callbacks.get.menuItemAppend.add(cg.getClassName(), mg.getName(), mg.getSignature());
					found = true;
					
					findMenuItemAppend.add(cg.getClassName() + "." + mg.getName(), -1);
				}
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (MenuItemNode.get.getInternalName() == null) {
			System.out.println("ERROR: MenuItemAppendAnalyzer: MenuItemNode internal class name is null!");
			return;
		}
		
		if (MenuGroupNode.get.getInternalName() == null) {
			System.out.println("ERROR: MenuItemAppendAnalyzer: MenuGroupNode internal class name is null!");
			return;
		}
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
