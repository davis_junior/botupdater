package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.ISHL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.SynonymInstruction;
import searcher.pi.PI_BIPUSH;
import searcher.pi.PI_LDC;
import searcher.pi.PI_LDC_W;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.Npc;
import data.Render;
import data.methods.Callbacks;

public class NpcModelUpdateAnalyzer extends Analyzer {
	
	public static NpcModelUpdateAnalyzer get = new NpcModelUpdateAnalyzer();

	CountInfoItem findModelUpdate = new CountInfoItem("findModelUpdate");
	
	public NpcModelUpdateAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findModelUpdate);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		if (!cg.getClassName().equals(Npc.get.getInternalName()))
			return false;
		
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 2)
				continue;
			
			int renderCount = 0;
			int intCount = 0;
			for (Type argType : argTypes) {
				if (argType.equals(Type.INT))
					intCount++;
				else if (argType.toString().equals(Render.get.getInternalName()))
					renderCount++;
			}
			
			if (renderCount == 0)
				continue;
			
			if (intCount < 1)
				continue;
			
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new SynonymInstruction(new PI_LDC(cpg).adjustValue(0x80000), new PI_LDC_W(cpg).adjustValue(0x80000)),
					IOR.class
			);
			
			final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
					new PI_BIPUSH(cpg).adjustValue(9),
					ISHL.class
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			patterns1.add(pattern2);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				if (matches1[1].length == 2) {
					Callbacks.get.npcModelUpdate.add(cg.getClassName(), mg.getName(), mg.getSignature());
					found = true;
				}
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		if (Npc.get.getInternalName() == null) {
			System.out.println("ERROR: NpcModelUpdateAnalyzer: Npc internal class name is null!");
			return;
		}
		
		if (Render.get.getInternalName() == null) {
			System.out.println("ERROR: NpcModelUpdateAnalyzer: Render internal class name is null!");
			return;
		}
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
