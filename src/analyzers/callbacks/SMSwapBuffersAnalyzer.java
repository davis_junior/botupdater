package analyzers.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.IOR;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;

import boot.BotUpdater;
import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_ConstantPushInstruction;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import analyzers.Analyzer;
import analyzers.countinfo.CountInfo;
import analyzers.countinfo.CountInfoItem;
import data.OpenGLRender;
import data.SDRender;
import data.Viewport;
import data.methods.Callbacks;

public class SMSwapBuffersAnalyzer extends Analyzer {
	
	public static SMSwapBuffersAnalyzer get = new SMSwapBuffersAnalyzer();

	CountInfoItem findSwapBuffers = new CountInfoItem("findSwapBuffers");
	
	public SMSwapBuffersAnalyzer() {
		countInfo = new CountInfo(getClass().getName());
		countInfo.add(findSwapBuffers);
	}
	
	@Override
	protected boolean verifyClass(ClassGen cg) {
		
		ConstantPoolGen cpg = cg.getConstantPool();

		//if (!cg.getClassName().equals(SDRender.get.getInternalName()))
		//	return false;
		
		boolean found = false;
		
		for (MethodGen mg : BotUpdater.methods.get(cg)) {
			if (!mg.getReturnType().equals(Type.INT))
				continue;
			
			Type[] argTypes = mg.getArgumentTypes();
			if (argTypes == null || argTypes.length < 2)
				continue;
			
			boolean containsGraphics = false;
			int intCount = 0;
			for (Type argType : argTypes) {
				if (argType.getSignature().equals("Ljava/awt/Graphics;")) {
					containsGraphics = true;
					break;
				}
				
				if (argType.equals(Type.INT))
					intCount++;
			}
			
			if (containsGraphics)
				continue;
			
			if (intCount < 2)
				continue;
			
			
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			final CompareInstruction[] exclusionPattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.awt.Graphics").adjustMethodName("setColor")
			);
			
			List<CompareInstruction[]> exclusionPatterns1 = new ArrayList<>();
			exclusionPatterns1.add(exclusionPattern1);
			
			InstructionHandle[][] exclusionMatches1 = searcher.searchAllFromStart(false, false, CompareType.Opcode, exclusionPatterns1);
			if (exclusionMatches1 != null)
				continue;
			
			
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKEVIRTUAL(cpg).adjustClassName("java.awt.Graphics").adjustMethodName("drawImage")
			);
			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
				Callbacks.get.smSwapBuffers.add(cg.getClassName(), mg.getName(), mg.getSignature());
				found = true;
			}
		}
		
		return found;
	}
	
	
	@Override
	public void analyze(ClassGen cg) {
		/*if (SDRender.get.getInternalName() == null) {
			System.out.println("ERROR: SMSwapBuffersAnalyzer: SDRender internal class name is null!");
			return;
		}*/
		
		
		if (verifyClass(cg)) {
			
		}
	}

}
