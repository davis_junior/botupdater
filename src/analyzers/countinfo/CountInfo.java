package analyzers.countinfo;
import java.util.ArrayList;
import java.util.List;

public class CountInfo {
	
	String name;
	List<CountInfoItem> items;
	
	public CountInfo(String name) {
		this.name = name;
		this.items = new ArrayList<>();
	}
	
	public void add(CountInfoItem... items) {
		for (CountInfoItem item : items)
			this.items.add(item);
	}
	
	public void reset() {
		for (CountInfoItem item : items) {
			item.clearEntries();
		}
	}
	
	public void print(boolean detailed) {
		System.out.println("[" + name + "] - Count info: ");
		
		for (CountInfoItem item : items)
			System.out.println(" * " + item.toString(detailed));
	}
}