package analyzers.countinfo;

import java.util.ArrayList;
import java.util.List;

public class CountInfoItem {
	String name;
	public List<CountInfoItemEntry> entries;
	
	public String getName() {
		return name;
	}
	
	public CountInfoItem(String name) {
		this.name = name;
		entries = new ArrayList<>();
	}
	
	public void clearEntries() {
		entries = new ArrayList<>();
	}
	
	public void add(String method, int pos) {
		entries.add(new CountInfoItemEntry(method, pos));
	}
	
	@Override
	public String toString() {
		return toString(true);
	}
	
	public String toString(boolean detailed) {
		StringBuilder sb = new StringBuilder();
		sb.append(name + ": " + entries.size());
		
		if (detailed && entries.size() != 0) {
			sb.append(System.lineSeparator());
			
			for (int i = 0; i < entries.size(); i++) {
				sb.append("    - " + entries.get(i).toString());
				if (i != entries.size() - 1)
					sb.append(System.lineSeparator());
			}
		}
		return sb.toString();
	}
}