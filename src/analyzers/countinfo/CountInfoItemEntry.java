package analyzers.countinfo;

public class CountInfoItemEntry {
	String method;
	int pos;
	
	public String getMethod() {
		return method;
	}
	
	public int getPos() {
		return pos;
	}
	
	public CountInfoItemEntry(String method, int pos) {
		this.method = method;
		this.pos = pos;
	}
	
	@Override
	public String toString() {
		return  method + "() - pos: " + pos;
	}
}