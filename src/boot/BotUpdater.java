package boot;
import gamepack.GamePackFetcher;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipException;

import org.apache.bcel.Repository;
import org.apache.bcel.classfile.ClassFormatException;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantCP;
import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantString;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FieldGen;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.ISHR;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.Type;
import org.apache.bcel.generic.TypedInstruction;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.rs3.database.Database;
import org.rs3.database.ServerInfo;
import org.rs3.database.ServerRow;

import proxy.ProxyAuthenticator;
import report.Report;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.Comparator.CompareType;
import searcher.pi.PI_GETFIELD;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;
import searcher.pi.PI_InvokeInstruction;
import searcher.pi.PrecisionInstruction;
import analyzers.AbstractBoundaryAnalyzer;
import analyzers.AbstractCameraAnalyzer;
import analyzers.AbstractCameraLocationDataAnalyzer;
import analyzers.AbstractCentralLocationDataAnalyzer;
import analyzers.AbstractDefLoaderAnalyzer;
import analyzers.AbstractDefinitionAnalyzer;
import analyzers.AbstractFloorObjectAnalyzer;
import analyzers.AbstractWallObjectAnalyzer;
import analyzers.AnimableNodeAnalyzer;
import analyzers.AnimableObjectAnalyzer;
import analyzers.AnimatedAnimableObjectAnalyzer;
import analyzers.AnimatedBoundaryObjectAnalyzer;
import analyzers.AnimatedFloorObjectAnalyzer;
import analyzers.AnimatedObjectAnalyzer;
import analyzers.AnimatedWallObjectAnalyzer;
import analyzers.AnimatorAndAnimationAnalyzer;
import analyzers.BaseInfoAnalyzer;
import analyzers.BoundaryObjectAnalyzer;
import analyzers.CacheAnalyzer;
import analyzers.CameraAnalyzer;
import analyzers.CameraLocationDataAnalyzer;
import analyzers.CanvasAnalyzer;
import analyzers.CentralLocationDataAnalyzer;
import analyzers.CharacterAnalyzer;
import analyzers.ClientAnalyzer;
import analyzers.ComponentAnalyzer;
import analyzers.ComponentNodeAnalyzer;
import analyzers.DefLoaderAnalyzer;
import analyzers.EntityNodeAnalyzer;
import analyzers.FloorObjectAnalyzer;
import analyzers.GEOfferAnalyzer;
import analyzers.GameInfoAnalyzer;
import analyzers.GraphicsAnalyzer;
import analyzers.GraphicsCPUUsageAnalyzer;
import analyzers.GraphicsLevelAnalyzer;
import analyzers.GraphicsSettingAnalyzer;
import analyzers.GraphicsSettingsContainerAnalyzer;
import analyzers.GroundAnalyzer;
import analyzers.GroundBytesAnalyzer;
import analyzers.GroundInfoAnalyzer;
import analyzers.HardReferenceAnalyzer;
import analyzers.HashTableAnalyzer;
import analyzers.IdComposite_AnimatedObjectAnalyzer;
import analyzers.InteractableAnalyzer;
import analyzers.InteractableDataAnalyzer;
import analyzers.ItemCacheLoaderAnalyzer;
import analyzers.ItemDefLoaderAnalyzer;
import analyzers.ItemDefinitionAnalyzer;
import analyzers.ItemNodeAnalyzer;
import analyzers.ItemTableAnalyzer;
import analyzers.LocationAnalyzer;
import analyzers.MenuGroupNodeAnalyzer;
import analyzers.MenuItemNodeAnalyzer;
import analyzers.MessageDataAnalyzer;
import analyzers.ModelAnalyzer;
import analyzers.MultiplierFinder;
import analyzers.NodeAnalyzer;
import analyzers.NodeDequeAnalyzer;
import analyzers.NodeListCacheAnalyzer;
import analyzers.NodeSubAnalyzer;
import analyzers.NodeSubQueueAnalyzer;
import analyzers.NpcAnalyzer;
import analyzers.NpcDefinitionAnalyzer;
import analyzers.NpcNodeAnalyzer;
import analyzers.ObjectCacheLoaderAnalyzer;
import analyzers.ObjectCompositeAnalyzer;
import analyzers.ObjectDefLoaderAnalyzer;
import analyzers.ObjectDefinitionAnalyzer;
import analyzers.OpenGLModelAnalyzer;
import analyzers.OpenGLRenderAnalyzer;
import analyzers.PlayerAnalyzer;
import analyzers.PlayerDefAnalyzer;
import analyzers.QuaternionAnalyzer;
import analyzers.RSAnimableAnalyzer;
import analyzers.ReferenceAnalyzer;
import analyzers.RenderAnalyzer;
import analyzers.SDModelAnalyzer;
import analyzers.SDRenderAnalyzer;
import analyzers.SettingDataAnalyzer;
import analyzers.SettingsAnalyzer;
import analyzers.SkillDataAnalyzer;
import analyzers.SoftReferenceAnalyzer;
import analyzers.TileDataAnalyzer;
import analyzers.ViewportAnalyzer;
import analyzers.WallObjectAnalyzer;
import analyzers.WidgetAnalyzer;
import analyzers.WorldInfoAnalyzer;
import analyzers.callbacks.ActionClickedAnalyzer;
import analyzers.callbacks.AnimableObjectModelUpdateAnalyzer;
import analyzers.callbacks.BoundaryObjectModelUpdateAnalyzer;
import analyzers.callbacks.ChangeSettingAnalyzer;
import analyzers.callbacks.CreateRenderAnalyzer;
import analyzers.callbacks.FloorObjectModelUpdateAnalyzer;
import analyzers.callbacks.GLSwapBuffersAnalyzer;
import analyzers.callbacks.GLViewportUpdateAnalyzer;
import analyzers.callbacks.ItemTablePutAnalyzer;
import analyzers.callbacks.JaclibNotifyAnalyzer;
import analyzers.callbacks.MenuItemAppendAnalyzer;
import analyzers.callbacks.NpcModelUpdateAnalyzer;
import analyzers.callbacks.SMSwapBuffersAnalyzer;
import analyzers.callbacks.SMViewportUpdateAnalyzer;
import analyzers.callbacks.WallObjectModelUpdateAnalyzer;
import data.ClassData;
import data.FieldData;
import data.GEOffer;
import data.GraphicsLevel;
import data.GroundInfo;
import data.methods.Callbacks;
import data.methods.MethodData;
import data.methods.MultipleMethodData;

public class BotUpdater {
	public static boolean iterateGamepacks = false;
	public static boolean iterateProxies = false;
	public static String gamepackVersion = "-1";
	
	public static boolean fetchGamepack = false;
	public static GamePackFetcher gamepackFetcher = null;
	
	public static boolean findMultipliers = false;
	public static boolean includeCountInfo = false;
	
	private static HashMap<String, ClassGen> classes = new HashMap<>();
	private static List<String> fileNames = new ArrayList<>();
	
	public static HashMap<ClassGen, MethodGen[]> methods = new HashMap<>();
	public static HashMap<InstructionList, InstructionHandle[]> iHandles = new HashMap<>();
	
	public static ClassGen getClass(String name) {
		return classes.get(name);
	}
	
	//
	// Marneus901
	//
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\798 1\\3bff2a57206e0ff6ca13125be75c27bff5483e79.jar"; // 798 Marneus901
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\807 1\\d94c7784420c9e0bc6ac16ed5dbfafb129607ea2.jar"; // 807 Marneus901
	//public static String originalFilePath = "C:\\Users\\User\\Downloads\\runescape3.jar"; // 8?? Marneus901 - just fulfilling a requested updaterlog
	public static String originalFilePath = "C:\\Users\\User\\Downloads\\93707009.jar"; // 8?? Marneus901 - just fulfilling a requested updaterlog
	
	//
	// go9090go
	//
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\790 1\\5c778c529e6691e25f60d814974dcfe73ebdc512.jar"; // 788 go9090go
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\794 1\\1bc69fdc4591b95fef921511dae78302b5a5272f.jar"; // 794 go9090go
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\797 1\\38b4acd74cede523befda4e4648cedb9c8982a3f.jar"; // 797 go9090go
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\806 1\\7c7590a181d597d4e8127cd6c2c10f18bd9f4d50.jar"; // 806 go9090go
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\807 1\\091db2fa1629b33a764e98f53bc04f9479c8823f.jar"; // 807 go9090go
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\812 1\\731512c455d43b0c0ea3046a4d89b3aa1818d00b.jar"; // 812 go9090go
	//public static String originalFilePath = "C:\\Documents and Settings\\User\\GamePacks\\827 1\\2d39244b7a80bc6499d67048425bdea71aed685d.jar"; // 827 go9090go
	
	//public static String originalFilePath = "C:\\Users\\User\\Documents\\[Gamepacks]\\own - 830 - 85883cac27288b6c8a7152f8dc3b145e9a6a4ab4.jar";
	
	private static boolean[] foundTransform = new boolean[1];
	private static int[] foundCount = new int[foundTransform.length];
	
	public static boolean loadFromJar = true;
	
	public BotUpdater() {
		//fetchGamepack = true;
		iterateProxies = true;
		//iterateGamepacks = true;
		////findMultipliers = true;
		//includeCountInfo = true;
		
		gamepackVersion = "807 1";
		
		if (iterateGamepacks) {
			List<File> jars = new ArrayList<File>();
			
			FilenameFilter jarFilter = new FilenameFilter() {
				@Override
				public boolean accept(File file, String name) {
					if (FilenameUtils.getExtension(name).equals("jar"))
						return true;
					return false;
				}
			};
			
			File dir = null;
			
			if (gamepackVersion == "-1")
				dir = new File("C:\\Documents and Settings\\User\\GamePacks\\");
			else
				dir = new File("C:\\Documents and Settings\\User\\GamePacks\\" + gamepackVersion + "\\");
			
			if (!dir.exists()) {
				System.out.println("ERROR: " + dir + " doesn't exist!");
				return;
			}
			
			for (File file : dir.listFiles()) {
				if (file.isDirectory()) {
					for (File jar : file.listFiles(jarFilter))
						jars.add(jar);
				} else if (jarFilter.accept(file, file.getName()))
					jars.add(file);
			}
			
			for (File jar : jars) {
				originalFilePath = jar.getAbsolutePath();
				readJar();
				log();
				
				Report.printResults(dir + "\\updaterLog.txt");
				
				if (includeCountInfo)
					printCountInfo();
				
				Report.writeClassesToXML();
				ClassData.clearAll();
				Callbacks.get.clearAll();
			}
			
			return;
		} else if (iterateProxies) {
			List<ServerRow> serverRows = Database.select(ServerRow.class, null);
			if (serverRows != null) {
				for (ServerRow serverRow : serverRows) {
					System.out.println("Using proxy: " + serverRow.getProxyHost() + ":" + serverRow.getProxyPort() + "...");
					Proxy proxy = new Proxy(java.net.Proxy.Type.SOCKS, new InetSocketAddress(serverRow.getProxyHost(), serverRow.getProxyPort()));
					Authenticator.setDefault(new ProxyAuthenticator(serverRow.getProxyUsername(), serverRow.getProxyPassword()));
					
					gamepackFetcher = new GamePackFetcher(proxy, true, true, false);
					originalFilePath = gamepackFetcher.getDecryptedJar().getAbsolutePath();
					readJar();
					log();
					
					Report.printResults(null);
					
					if (includeCountInfo)
						printCountInfo();
					
					Report.writeClassesToXML();
					ClassData.clearAll();
					Callbacks.get.clearAll();
				}
			}
			
			return;
		} else if (fetchGamepack) {
			Proxy proxy = new Proxy(java.net.Proxy.Type.SOCKS, new InetSocketAddress("104.148.11.201", 61336));
			Authenticator.setDefault(new ProxyAuthenticator("davisjunior11", "E25lEePY"));
			
			gamepackFetcher = new GamePackFetcher(proxy, true, true, false);
			//gamepackFetcher = new GamePackFetcher(Proxy.NO_PROXY, true, true, false);
			originalFilePath = gamepackFetcher.getDecryptedJar().getAbsolutePath();
		}
		
		//copyOtherClasses();
		//writeFiles();
		
		readJar();
		
		/*long mark = System.currentTimeMillis();
		for (ClassGen cg : classes.values()) {
			ConstantPoolGen cpg = cg.getConstantPool();
			for (Method m : cg.getMethods()) {
				MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
				
				InstructionList il = mg.getInstructionList();
			}
		}
		
		System.out.println("Time taken: " + (System.currentTimeMillis() - mark));
		System.out.println("Time taken * 200: " + ((System.currentTimeMillis() - mark) * 200));
		
		if (true)
			return;*/
		
		//find();

		//transform();
		//injectObjTrackers();
		//writeNewJar("./injectedObjTrackers.jar");
		//reflectionLog();

		//injectMethodTrackers();
		//writeNewJar("./injectedMethodTrackers.jar");

		//transform();
		//injectActionPerformedTrackers();
		//writeNewJar("./injectedActionPerformedTrackers.jar");

		//injectExceptionHandlers();
		
		long mark = System.currentTimeMillis();
		/*System.out.print("Removing unused methods... ");
		int removedCount = removeUnusedMethods();
		mark = (System.currentTimeMillis() - mark);
		System.out.println(mark + "ms");
		System.out.println("Removed " + removedCount + " unused methods.");
		
		mark = System.currentTimeMillis();*/
		System.out.println("Analyzing...");
		log();
		mark = (System.currentTimeMillis() - mark);
		
		Report.printResults(null);
		//GEOffer.get.printReport();
		System.out.println("Analysis: " + mark + "ms");
		
		if (includeCountInfo)
			printCountInfo();
		
		Report.writeClassesToXML();
		
		
		renameClasses();
		writeJar();
	}

	public void log() {
		ClassData.setClasses();
		
		// 120 analyzers
		/*ClassGen abc = classes.values().toArray(new ClassGen[0])[0];
		//ClassGen abc = cg;
		final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
				new ALOAD(0),
				new PI_GETFIELD(abc.getConstantPool()).adjustClassName(abc.getClassName()).adjustTypeString(false, GroundInfo.get.getInternalName()),
				new PI_GETFIELD(abc.getConstantPool()).adjustClassName(abc.getClassName()).adjustTypeString(false, GroundInfo.get.getInternalName())
		);
		
		final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
				ISHR.class,
				IADD.class,
				new PI_GETFIELD(abc.getConstantPool()).adjustClassName(abc.getClassName()).adjustTypeString(false, GroundInfo.get.getInternalName())
		);
		
		List<CompareInstruction[]> patterns1 = new ArrayList<>();
		patterns1.add(pattern1);
		patterns1.add(pattern2);
		
		
		for (int i = 0; i < 120; i++) {
			for (ClassGen cg : classes.values()) {
				for (CompareInstruction[] cia : patterns1) {
					for (CompareInstruction ci : cia) {
						if (ci instanceof PrecisionInstruction<?>) {
							((PrecisionInstruction) ci).setConstantPool(cg.getConstantPool());
						}
					}
				}
				
				for (MethodGen mg : BotUpdater.methods.get(cg)) {
					InstructionList il = mg.getInstructionList();
					if (il == null || il.getLength() == 0)
						continue;
					
					Searcher searcher = new Searcher(il);
					
					InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				}
			}
		}
		
		if (true)
			return;*/
		
		// 1st pass
		for (ClassGen cg : classes.values()) {
			TileDataAnalyzer.get.analyze(cg);
			GroundBytesAnalyzer.get.analyze(cg);
			GroundInfoAnalyzer.get.analyze(cg);
			BaseInfoAnalyzer.get.analyze(cg);
			GameInfoAnalyzer.get.analyze(cg);
			QuaternionAnalyzer.get.analyze(cg);
			CameraLocationDataAnalyzer.get.analyze(cg);
			CameraAnalyzer.get.analyze(cg);
			LocationAnalyzer.get.analyze(cg);
			InteractableDataAnalyzer.get.analyze(cg);
			InteractableAnalyzer.get.analyze(cg);
			RSAnimableAnalyzer.get.analyze(cg);
			CharacterAnalyzer.get.analyze(cg);
			PlayerAnalyzer.get.analyze(cg);
			PlayerDefAnalyzer.get.analyze(cg);
			ClientAnalyzer.get.analyze(cg);
			ModelAnalyzer.get.analyze(cg);
			RenderAnalyzer.get.analyze(cg);
			SDRenderAnalyzer.get.analyze(cg);
			OpenGLRenderAnalyzer.get.analyze(cg);
			ViewportAnalyzer.get.analyze(cg);
			CanvasAnalyzer.get.analyze(cg);
			GraphicsAnalyzer.get.analyze(cg);
			NpcDefinitionAnalyzer.get.analyze(cg);
			NodeAnalyzer.get.analyze(cg);
			AnimableNodeAnalyzer.get.analyze(cg);
			ObjectDefinitionAnalyzer.get.analyze(cg);
			ItemDefinitionAnalyzer.get.analyze(cg);
			ObjectCompositeAnalyzer.get.analyze(cg);
			AnimatorAndAnimationAnalyzer.get.analyze(cg);
			NodeDequeAnalyzer.get.analyze(cg);
			SettingsAnalyzer.get.analyze(cg);
			ItemTableAnalyzer.get.analyze(cg);
			SkillDataAnalyzer.get.analyze(cg);
			GEOfferAnalyzer.get.analyze(cg);
			WorldInfoAnalyzer.get.analyze(cg);
			GraphicsSettingAnalyzer.get.analyze(cg);
		}
		
		// 2nd pass
		for (ClassGen cg : classes.values()) {
			SDModelAnalyzer.get.analyze(cg);
			OpenGLModelAnalyzer.get.analyze(cg);
			EntityNodeAnalyzer.get.analyze(cg);
			AbstractCameraLocationDataAnalyzer.get.analyze(cg);
			CentralLocationDataAnalyzer.get.analyze(cg);
			ClientAnalyzer.get.analyze_pass2(cg);
			CharacterAnalyzer.get.analyze_pass2(cg);
			NpcNodeAnalyzer.get.analyze(cg);
			GroundAnalyzer.get.analyze(cg);
			AnimatedObjectAnalyzer.get.analyze(cg);
			AnimableObjectAnalyzer.get.analyze(cg);
			NodeSubAnalyzer.get.analyze(cg);
			HashTableAnalyzer.get.analyze(cg);
			NodeAnalyzer.get.analyze_pass2(cg);
			PlayerAnalyzer.get.analyze_pass2(cg);
			ObjectCompositeAnalyzer.get.analyze_pass2(cg);
			ComponentAnalyzer.get.analyze(cg);
			NodeDequeAnalyzer.get.analyze_pass2(cg);
			MessageDataAnalyzer.get.analyze(cg);
			IdComposite_AnimatedObjectAnalyzer.get.analyze(cg);
			SettingDataAnalyzer.get.analyze(cg);
			AbstractDefinitionAnalyzer.get.analyze(cg);
			AbstractBoundaryAnalyzer.get.analyze(cg);
			AbstractWallObjectAnalyzer.get.analyze(cg);
			ItemNodeAnalyzer.get.analyze(cg);
			NodeListCacheAnalyzer.get.analyze(cg);
			GraphicsSettingsContainerAnalyzer.get.analyze(cg);
			GraphicsLevelAnalyzer.get.analyze(cg);
			GraphicsCPUUsageAnalyzer.get.analyze(cg);
		}
		
		// 3rd pass
		for (ClassGen cg : classes.values()) {
			AbstractCentralLocationDataAnalyzer.get.analyze(cg);
			NodeSubQueueAnalyzer.get.analyze(cg);
			NpcAnalyzer.get.analyze(cg);
			ReferenceAnalyzer.get.analyze(cg);
			WidgetAnalyzer.get.analyze(cg);
			ComponentAnalyzer.get.analyze_pass3(cg);
			ClientAnalyzer.get.analyze_pass3(cg);
			MenuItemNodeAnalyzer.get.analyze(cg);
			CharacterAnalyzer.get.analyze_pass3(cg);
			AnimatedAnimableObjectAnalyzer.get.analyze(cg);
			AnimatedBoundaryObjectAnalyzer.get.analyze(cg);
			AnimatedWallObjectAnalyzer.get.analyze(cg);
			AnimatedObjectAnalyzer.get.analyze_pass3(cg);
			AnimableObjectAnalyzer.get.analyze_pass3(cg);
			BoundaryObjectAnalyzer.get.analyze(cg);
			GroundAnalyzer.get.analyze_statics(cg);
			AbstractFloorObjectAnalyzer.get.analyze(cg);
			WallObjectAnalyzer.get.analyze(cg);
			GraphicsSettingsContainerAnalyzer.get.analyze_pass3(cg);
		}
		
		// 4th pass
		for (ClassGen cg : classes.values()) {
			AbstractCameraAnalyzer.get.analyze(cg);
			ClientAnalyzer.get.analyze_pass4(cg);
			SoftReferenceAnalyzer.get.analyze(cg);
			CacheAnalyzer.get.analyze(cg);
			ComponentAnalyzer.get.analyze_pass4(cg);
			ComponentNodeAnalyzer.get.analyze(cg);
			MenuItemNodeAnalyzer.get.analyze_pass2(cg);
			MenuGroupNodeAnalyzer.get.analyze(cg);
			BoundaryObjectAnalyzer.get.analyze_pass4(cg);
			FloorObjectAnalyzer.get.analyze(cg);
			AnimatedFloorObjectAnalyzer.get.analyze(cg);
			GroundAnalyzer.get.analyze_pass4(cg);
			WallObjectAnalyzer.get.analyze_pass4(cg);
		}
		
		// 5th pass
		for (ClassGen cg : classes.values()) {
			HardReferenceAnalyzer.get.analyze(cg);
			ObjectCacheLoaderAnalyzer.get.analyze(cg);
			ItemCacheLoaderAnalyzer.get.analyze(cg);
			ClientAnalyzer.get.analyze_pass5(cg);
			ComponentAnalyzer.get.analyze_pass5(cg);
			DefLoaderAnalyzer.get.analyze(cg);
			FloorObjectAnalyzer.get.analyze_pass5(cg);
		}
		
		// 6th pass
		for (ClassGen cg : classes.values()) {
			AbstractDefLoaderAnalyzer.get.analyze(cg);
			ObjectDefLoaderAnalyzer.get.analyze(cg);
			ItemDefLoaderAnalyzer.get.analyze(cg);
			ObjectCacheLoaderAnalyzer.get.analyze_pass6(cg);
		}
		
		// ObjectDefLoader/ObjectCacheLoader pass
		// ItemDefLoader/ItemCacheLoader pass
		for (ClassGen cg : classes.values()) {
			GameInfoAnalyzer.get.analyze_ObjectDefLoader(cg);
			ObjectDefinitionAnalyzer.get.analyze_ObjectDefLoader(cg);
			ObjectDefinitionAnalyzer.get.analyze_ObjectCacheLoader(cg);
			AnimableObjectAnalyzer.get.analyze_ObjectDefLoader(cg);
			AnimatedObjectAnalyzer.get.analyze_ObjectDefLoader(cg);
			BoundaryObjectAnalyzer.get.analyze_ObjectDefLoader(cg);
			FloorObjectAnalyzer.get.analyze_ObjectDefLoader(cg);
			WallObjectAnalyzer.get.analyze_ObjectDefLoader(cg);
			
			ClientAnalyzer.get.analyze_ItemDefLoader(cg);
			ItemDefinitionAnalyzer.get.analyze_ItemDefLoader(cg);
			ItemDefinitionAnalyzer.get.analyze_ItemCacheLoader(cg);
		}
		
		
		// Method Callbacks
		for (ClassGen cg : classes.values()) {
			ActionClickedAnalyzer.get.analyze(cg);
			SMViewportUpdateAnalyzer.get.analyze(cg);
			GLViewportUpdateAnalyzer.get.analyze(cg);
			SMSwapBuffersAnalyzer.get.analyze(cg);
			GLSwapBuffersAnalyzer.get.analyze(cg);
			NpcModelUpdateAnalyzer.get.analyze(cg);
			ChangeSettingAnalyzer.get.analyze(cg);
			AnimableObjectModelUpdateAnalyzer.get.analyze(cg);
			BoundaryObjectModelUpdateAnalyzer.get.analyze(cg);
			FloorObjectModelUpdateAnalyzer.get.analyze(cg);
			WallObjectModelUpdateAnalyzer.get.analyze(cg);
			CreateRenderAnalyzer.get.analyze(cg);
			ItemTablePutAnalyzer.get.analyze(cg);
			MenuItemAppendAnalyzer.get.analyze(cg);
			JaclibNotifyAnalyzer.get.analyze(cg);
		}
		
		// Remove unused methods
		for (MultipleMethodData mmd : Callbacks.get.getMultipleMethods()) {
			List<MethodData> methods = mmd.getMethods();
			if (methods != null) {
				List<MethodData> toRemove = new ArrayList<>();
				for (MethodData md : methods) {
					if (md.getInternalName() != null) {
						boolean found = false;
						
						// check to see if superclasses contain the same method as an interface or abstract method
						try {
							for (JavaClass jc : classes.get(md.getInternalClassName()).getJavaClass().getSuperClasses()) {
								for (Method m : jc.getMethods()) {
									if (m.isAbstract() || m.isInterface()) {
										
										// TODO: check if interface / abstract method is used anywhere with INVOKEINTERFACE or whatever the instr is for abstract methods
										
										if (m.getName().equals(md.getInternalName()) && m.getSignature().equals(md.getSignature())) {
											found = true;
											break;
										}
									}
								}
							}
							
							if (found)
								break;
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
						
						for (ClassGen cg : classes.values()) {
							ConstantPoolGen cpg = cg.getConstantPool();
							
							for (MethodGen mg : BotUpdater.methods.get(cg)) {
								//if (!mg.isStatic())
								//	continue;
								
								InstructionList il = mg.getInstructionList();
								if (il == null || il.getLength() == 0)
									continue;
								
								Searcher searcher = new Searcher(il);
								
								final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
										new PI_InvokeInstruction<>(cpg).adjustClassName(md.getInternalClassName()).adjustMethodName(md.getInternalName()).adjustSignature(md.getSignature())
								);
								
								InstructionHandle match = searcher.searchFromStart(false, CompareType.Opcode, pattern1);
								
								if (match != null) {
									found = true;
									break;
								}
							}
							
							if (found)
								break;
						}
						
						if (!found) {
							System.out.println("Removing unused method " + md.getInterfacedName() + "() -> " + md.getInternalClassName() + "." + md.getInternalName() + "()...");
							toRemove.add(md);
						}
					}
				}
				
				for (MethodData md : toRemove) {
					methods.remove(md);
				}
			}
		}
		
		// Save multiplier info

		/*for (String key : MultiplierFinder.multipliers.keySet()) {
			for (ClassData classData : ClassData.classes) {
				if (classData.fields != null) {
					for (FieldData fieldData : classData.fields) {
						if (fieldData.getInternalClassName() != null && fieldData.getInternalName() != null) {
							if (key.contains(fieldData.getInternalClassName() + "." + fieldData.getInternalName())) {
								if (fieldData instanceof MultiplierFieldData) {
									((MultiplierFieldData) fieldData).set(MultiplierFinder.multipliers.get(key));
								}
							}
						}
					}
				}
			}
		}*/
		
		// Cache multipliers
		if (findMultipliers) {
			for (ClassGen cg : classes.values()) {
				MultiplierFinder.findMultipliers(cg);
			}
		}
	}
	
	public void printCountInfo() {
		boolean detailed = true;
		System.out.println(
				"*********************************" + "\n" +
				"         Count Info Report" 		+ "\n" +
				"*********************************");
		ClientAnalyzer.get.printCountInfo(detailed);
		SDModelAnalyzer.get.printCountInfo(detailed);
		PlayerAnalyzer.get.printCountInfo(detailed);
		CharacterAnalyzer.get.printCountInfo(detailed);
		RSAnimableAnalyzer.get.printCountInfo(detailed);
		InteractableAnalyzer.get.printCountInfo(detailed);
		EntityNodeAnalyzer.get.printCountInfo(detailed);
		InteractableDataAnalyzer.get.printCountInfo(detailed);
		LocationAnalyzer.get.printCountInfo(detailed);
	}

	public void readJar() {
		readJar(originalFilePath);
	}

	public void readJar(String path) {
		System.out.print("Reading jar: " + path + ": ");
		
		classes.clear();
		BotUpdater.methods.clear();
		iHandles.clear();

		long mark = System.currentTimeMillis();
		ClassGen[] cgs = readJar_Custom(path);
		System.out.println((System.currentTimeMillis() - mark) + "ms");
		
		if (cgs == null)
			return;

		System.out.print("Caching classes, methods, instructions, etc.: ");
		mark = System.currentTimeMillis();
		for (ClassGen cg : cgs) {
			classes.put(cg.getClassName(), cg);
			Repository.addClass(cg.getJavaClass());
			
			ConstantPoolGen cpg = cg.getConstantPool();
			Method[] methods = cg.getMethods();
			MethodGen[] mgs;
			
			if (methods != null) {
				mgs = new MethodGen[methods.length];
				for (int i = 0; i < methods.length; i++) {
					MethodGen mg = new MethodGen(methods[i], cg.getClassName(), cpg);
					mgs[i] = mg;
					
					InstructionList il = mg.getInstructionList();
					if (il != null)
						iHandles.put(il, il.getInstructionHandles());
				}
			} else
				mgs = null;
			
			BotUpdater.methods.put(cg, mgs);
		}
		Searcher.iHandlesCache = iHandles;
		System.out.println((System.currentTimeMillis() - mark) + "ms");
	}

	public ClassGen[] readJar_Custom(String path) {
		ClassGen[] cgs = null;

		File file = new File(path);
		JarFile jarFile = null;

		try {
			jarFile = new JarFile(file);
			Enumeration<JarEntry> en = jarFile.entries();

			cgs = new ClassGen[jarFile.size()];
			int cgi = 0;

			while (en.hasMoreElements()) {
				JarEntry entry = en.nextElement();
				if (entry.getName().endsWith(".class")) {
					ClassParser cp = new ClassParser(jarFile.getInputStream(entry), entry.getName());
					JavaClass jc = cp.parse();
					ClassGen cg = new ClassGen(jc);

					cgs[cgi] = cg;
					cgi++;
				}
				else
					fileNames.add(entry.getName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (jarFile != null)
				try {
					jarFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		if (cgs != null) {
			int count = 0;
			for (ClassGen cg : cgs) {
				if (cg != null)
					count++;
			}

			int i = 0;
			ClassGen[] cgsTrimmed = new ClassGen[count];
			for (ClassGen cg : cgs) {
				if (cg != null) {
					cgsTrimmed[i] = cg;
					i++;
				}
			}

			cgs = cgsTrimmed;
		}

		return cgs;
	}

	public ClassGen[] loadClasses(String path) {
		File dir = new File(path);
		File[] fileList = dir.listFiles(new FileFilter() {

			@Override
			public boolean accept(File arg0) {
				if (arg0.getName().contains(".class"))
					return true;
				else
					return false;
			}
		});

		ClassGen[] customClasses = new ClassGen[fileList.length];

		for (int i = 0; i < fileList.length; i++) {
			try {
				ClassParser cp = new ClassParser(fileList[i].getPath());
				JavaClass jc;
				jc = cp.parse();
				customClasses[i] = new ClassGen(jc);
			} catch (ClassFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return customClasses;
	}
	
	public ClassData searchClassByName(String internalClassName) {
		for (ClassData cd : ClassData.classes) {
			if (internalClassName.equals(cd.getInternalName())) {
				return cd;
			}
		}
		
		return null;
	}
	
	public ClassData searchClassBySignature(String signature) {
		for (ClassData cd : ClassData.classes) {
			String s = "L" + cd.getInternalName() + ";";
			if (signature.equals(s)) {
				return cd;
			}
		}
		
		return null;
	}
	
	/**
	 * E.g., turns (Lyh;Lga;)Lkm; into (LRender;LAnimableObject;)LModel;
	 * 
	 * @param signature
	 * @return
	 */
	public String fixSignature(String signature) {
		for (ClassData cd : ClassData.classes) {
			signature = signature.replace("L" + cd.getInternalName() + ";", "L" + cd.getInterfacedName() + ";");
		}
		
		return signature;
	}
	
	
	public FieldData searchField(String className, String fieldName) {
		for (ClassData cd : ClassData.classes) {
			if (cd.fields != null) {
				for (FieldData fd : cd.fields) {
					if (className.equals(fd.getInternalClassName()) && fieldName.equals(fd.getInternalName())) {
						return fd;
					}
				}
			}
		}
		
		return null;
	}
	
	public void renameClasses() {
		for (ClassGen cg : classes.values()) {
			ConstantPoolGen cpg = cg.getConstantPool();
			ConstantPool cp = cpg.getConstantPool();
			
			Map<Integer, Constant> cUpdates = new HashMap<>();
			
			//
			// set constant pool classes/types, field names
			//
			//System.out.println("Class: " + cg.getClassName());
			int index = 0;
			for (Constant c : cg.getConstantPool().getConstantPool().getConstantPool()) {
				if (c == null) {
					index++;
					continue;
				}
				
				if (c instanceof ConstantClass) {
					ConstantClass cclass = (ConstantClass) c;
					//System.out.println(index + " = " + cclass.toString() + "  " + cclass.getConstantValue(cp));
					
					int nameIndex = cclass.getNameIndex();
					ConstantUtf8 utf8 = (ConstantUtf8) cpg.getConstant(nameIndex);
					String className = utf8.getBytes(); // not signature
					ClassData cd = searchClassByName(className);
					if (cd != null)
						cUpdates.put(nameIndex, new ConstantUtf8(cd.getInterfacedName()));
				} else if (c instanceof ConstantNameAndType) {
					ConstantNameAndType cnt = (ConstantNameAndType) c;
					//System.out.println(index + " = " + cnt.toString() + "  " + cnt.getName(cp) + " " + cnt.getSignature(cp));
					
					int sigIndex = cnt.getSignatureIndex();
					ConstantUtf8 utf8 = (ConstantUtf8) cpg.getConstant(sigIndex);
					String signature = utf8.getBytes();
					String fixedSig = fixSignature(signature);
					if (!signature.equals(fixedSig))
						cUpdates.put(sigIndex, new ConstantUtf8(fixSignature(signature)));
				} else if (c instanceof ConstantFieldref) {
					ConstantFieldref cfr = (ConstantFieldref) c;
					ConstantNameAndType cnt = (ConstantNameAndType) cpg.getConstant(cfr.getNameAndTypeIndex());
					
					String className = cfr.getClass(cp);
					String fieldName = cnt.getName(cp);
					String fieldSig = cnt.getSignature(cp);
					//System.out.println(index + " = " + cfr.toString() + "  " + className + "." + fieldName + " " + fieldSig);
					
					FieldData fd = searchField(className, fieldName);
					if (fd != null) {
						// do not edit name indexes, add new since sometimes the same name can be multiple different fields, etc
						
						int newNameTypeIndex = cpg.addNameAndType(fd.getInterfacedName(), fieldSig);
						
						//System.out.println(className + "." + fieldName + " to " + fd.getInterfacedName());
						cUpdates.put(index, new ConstantFieldref(cfr.getClassIndex(), newNameTypeIndex));
					}
				} else {
					//System.out.println(index + " = " + c.toString());
				}
				
				index++;
			}
			
			for (int key : cUpdates.keySet()) {
				cpg.setConstant(key, cUpdates.get(key));
			}
			
			cg.setConstantPool(cpg);
			
			
			//
			// set class names, super class names, interfaces, field types, field names, method return types, method arg types
			//
			String origClassName = cg.getClassName();
			
			ClassData cd = searchClassByName(origClassName);
			if (cd != null) {
				// set class name (this will affect methods hashmap)
				cg.setClassName(cd.getInterfacedName());
			}
			
			cd = searchClassByName(cg.getSuperclassName());
			if (cd != null) {
				// set super class name
				cg.setSuperclassName(cd.getInterfacedName());
			}
			
			for (String iface : cg.getInterfaceNames()) {
				cd = searchClassByName(iface);
				if (cd != null) {
					// set interface name TODO: test
					cg.removeInterface(iface);
					cg.addInterface(cd.getInterfacedName());
				}
			}
			
			for (Field f : cg.getFields()) {
				// set type
				cd = searchClassBySignature(f.getSignature());
				if (cd != null) {
					FieldGen fg = new FieldGen(f, cpg);
					fg.setType(Type.getType("L" + cd.getInterfacedName() + ";"));
					
					cg.replaceField(f, fg.getField());
				}
			}
			
			for (Field f : cg.getFields()) {
				// set name
				FieldData fd = searchField(origClassName, f.getName());
				if (fd != null) {
					FieldGen fg = new FieldGen(f, cpg);
					fg.setName(fd.getInterfacedName());
					
					cg.replaceField(f, fg.getField());
				}
			}
			
			//ConstantPoolGen cpg = cg.getConstantPool();
			
			for (Method m : cg.getMethods()) {
				MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
				
				// set return type
				cd = searchClassBySignature(m.getReturnType().getSignature());
				if (cd != null) {
					mg.setReturnType(Type.getReturnType("L" + cd.getInterfacedName() + ";"));
				}
				
				// set arg types
				Type[] args = mg.getArgumentTypes();
				if (args != null) {
					for (int i = 0; i < args.length; i++) {
						Type type = args[i];
						cd = searchClassBySignature(type.getSignature());
						if (cd != null) {
							mg.setArgumentType(i, Type.getType("L" + cd.getInterfacedName() + ";"));
						}
					}
				}
				
				cg.replaceMethod(m, mg.getMethod());
			}
		}
	}
	
	public void writeJar() {
		writeJar("./rsrename.jar");
	}

	public void writeJar(String path) {
		try {
			File file = new File(path);

			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			FileOutputStream ostream = new FileOutputStream(file);
			JarOutputStream out = new JarOutputStream(ostream);
			
			for (ClassGen cg : classes.values()) {
				try {
					JarEntry je = new JarEntry(cg.getClassName().replace('.', '/') + ".class");

					out.putNextEntry(je);
					out.write(cg.getJavaClass().getBytes());
				} catch (ZipException ze) {
					if (!ze.getMessage().contains("duplicate"))
						ze.printStackTrace();
				}
			}
			
			out.close();
			ostream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isMethodUsed(MethodGen method) {
		// check own class first since the majority of methods are used inside their own class
		ClassGen methodCg = classes.get(method.getClassName());
		if (isMethodUsed(methodCg, method))
			return true;
		
		for (ClassGen cg : classes.values()) {
			if (cg != methodCg) {
				if (isMethodUsed(cg, method))
					return true;
			}
		}
		
		return false;
	}
	
	// TODO: finish, way too slow
	public static boolean isMethodUsed(ClassGen cgToSearch, MethodGen method) {
		if (method.isInterface() || method.isAbstract() || method.isNative() || method.getName().equals("<init>") || method.getName().equals("<clinit>"))
			return true;
		
		boolean found = false;
		
		ConstantPoolGen cpg = cgToSearch.getConstantPool();
		
		CompareInstruction[] pattern1;
		if (method.isStatic()) {
			pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKESTATIC(cpg).adjustClassName(method.getClassName()).adjustMethodName(method.getName()).adjustSignature(method.getSignature())
					);
		} else {
			pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_INVOKEVIRTUAL(cpg).adjustClassName(method.getClassName()).adjustMethodName(method.getName()).adjustSignature(method.getSignature())
					);
		}
		
		for (MethodGen mg : BotUpdater.methods.get(cgToSearch)) {
			InstructionList il = mg.getInstructionList();
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			InstructionHandle match = searcher.searchFromStart(false, CompareType.Opcode, pattern1);
			if (match != null) {
				found = true;
				break;
			}
		}
		
		return found;
	}
	
	public static int removeUnusedMethods() {
		Map<ClassGen, List<MethodGen>> toRemove = new HashMap<>();
		
		int removedCount = 0;
		
		for (ClassGen cg : classes.values()) {
			for (MethodGen mg : BotUpdater.methods.get(cg)) {
				if (!isMethodUsed(mg)) {
					List<MethodGen> list = toRemove.get(cg);
					if (list == null) {
						list = new ArrayList<>();
						list.add(mg);
						toRemove.put(cg, list);
					} else
						list.add(mg);
					
					removedCount++;
				}
			}
		}
		
		for (ClassGen cg : toRemove.keySet()) {
			for (MethodGen method : toRemove.get(cg)) {
				List<MethodGen> list = Arrays.asList(BotUpdater.methods.get(cg));
				list.remove(method);
				BotUpdater.methods.put(cg, list.toArray(new MethodGen[0]));
			}
		}
		
		return removedCount;
	}
	
	public static void main(String[] args) {
		new BotUpdater();
	}
}
