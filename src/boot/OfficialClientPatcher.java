package boot;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipException;

import org.apache.bcel.Repository;
import org.apache.bcel.classfile.ClassFormatException;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;

import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.Comparator.CompareType;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_INVOKEVIRTUAL;

public class OfficialClientPatcher {
	
	private static HashMap<String, ClassGen> classes = new HashMap<>();
	private static List<String> fileNames = new ArrayList<>();
	
	public static HashMap<ClassGen, MethodGen[]> methods = new HashMap<>();
	public static HashMap<InstructionList, InstructionHandle[]> iHandles = new HashMap<>();
	
	public static String originalFilePath = "C:\\Users\\User\\jagexcache\\jagexlauncher\\bin\\jagexappletviewer.jar";
	
	public OfficialClientPatcher() {
		readJar();
		patch();
		writeJar();
	}
	
	public static ClassGen getClass(String name) {
		return classes.get(name);
	}
	
	public void patch() {
		for (ClassGen cg : classes.values()) {
			injectMainTransformer(cg);
		}
	}
	
	public boolean injectMainTransformer(ClassGen cg) {
		
		boolean found = false;
		
		String className = cg.getClassName();
		
		if (className.startsWith("jagexappletviewer")) {
			try {
				ConstantPoolGen cpg = cg.getConstantPool();
				
				// Applet pre-start injection
				for (Method m : cg.getMethods()) {
					if (m.getName().equals("main")) {
						MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
						
						InstructionList il = mg.getInstructionList();
						if (il == null || il.getLength() == 0)
							continue;
						
						// check if already patched, skip if so
						Searcher searcher = new Searcher(il);
						
						final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
								new PI_INVOKESTATIC(cpg).adjustClassName("org.rs3.boot.OfficialClientInject").adjustMethodName("injectMain").adjustSignature("()V")
						);
						
						List<CompareInstruction[]> patterns1 = new ArrayList<>();
						patterns1.add(pattern1);
						
						InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
						
						if (matches1 != null && matches1.length == patterns1.size()) {
							System.out.println("(injectMainTransformer) Already transformed: " + className + "." + mg.getName() + "()");
							return true;
						}
						
						if (true)
							return true;
						
						System.out.println("(injectMainTransformer) Transforming: " + className + "." + mg.getName() + "()");
						found = true;
						
						InstructionList appendList = new InstructionList();
						appendList.append(new INVOKESTATIC(cpg.addMethodref("org.rs3.boot.OfficialClientInject", "injectMain", "()V")));
						
						il.insert(appendList);
						
						if (found) {
							mg.setInstructionList(il);
							mg.setMaxStack();
							mg.setMaxLocals();
							mg.removeLineNumbers();

							cg.replaceMethod(m, mg.getMethod());
						}
					}
				}
			} catch (Exception ex) {
				System.out.println("injectMainTransformer() exception");
				ex.printStackTrace();
			}
		}
		
		return found;
	}

	public void readJar() {
		readJar(originalFilePath);
	}

	public void readJar(String path) {
		System.out.print("Reading jar: " + path + ": ");
		
		classes.clear();

		long mark = System.currentTimeMillis();
		ClassGen[] cgs = readJar_Custom(path);
		System.out.println((System.currentTimeMillis() - mark) + "ms");
		
		if (cgs == null)
			return;

		System.out.print("Caching classes, methods, instructions, etc.: ");
		mark = System.currentTimeMillis();
		for (ClassGen cg : cgs) {
			classes.put(cg.getClassName(), cg);
			Repository.addClass(cg.getJavaClass());
			
			ConstantPoolGen cpg = cg.getConstantPool();
			Method[] methods = cg.getMethods();
			MethodGen[] mgs;
			
			if (methods != null) {
				mgs = new MethodGen[methods.length];
				for (int i = 0; i < methods.length; i++) {
					MethodGen mg = new MethodGen(methods[i], cg.getClassName(), cpg);
					mgs[i] = mg;
					
					InstructionList il = mg.getInstructionList();
					if (il != null)
						iHandles.put(il, il.getInstructionHandles());
				}
			} else
				mgs = null;
			
			BotUpdater.methods.put(cg, mgs);
		}
		Searcher.iHandlesCache = iHandles;
		System.out.println((System.currentTimeMillis() - mark) + "ms");
	}

	public ClassGen[] readJar_Custom(String path) {
		ClassGen[] cgs = null;

		File file = new File(path);
		JarFile jarFile = null;

		try {
			jarFile = new JarFile(file);
			Enumeration<JarEntry> en = jarFile.entries();

			cgs = new ClassGen[jarFile.size()];
			int cgi = 0;

			while (en.hasMoreElements()) {
				JarEntry entry = en.nextElement();
				if (entry.getName().endsWith(".class")) {
					ClassParser cp = new ClassParser(jarFile.getInputStream(entry), entry.getName());
					JavaClass jc = cp.parse();
					ClassGen cg = new ClassGen(jc);

					cgs[cgi] = cg;
					cgi++;
				}
				else
					fileNames.add(entry.getName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (jarFile != null)
				try {
					jarFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		if (cgs != null) {
			int count = 0;
			for (ClassGen cg : cgs) {
				if (cg != null)
					count++;
			}

			int i = 0;
			ClassGen[] cgsTrimmed = new ClassGen[count];
			for (ClassGen cg : cgs) {
				if (cg != null) {
					cgsTrimmed[i] = cg;
					i++;
				}
			}

			cgs = cgsTrimmed;
		}

		return cgs;
	}

	public ClassGen[] loadClasses(String path) {
		File dir = new File(path);
		File[] fileList = dir.listFiles(new FileFilter() {

			@Override
			public boolean accept(File arg0) {
				if (arg0.getName().contains(".class"))
					return true;
				else
					return false;
			}
		});

		ClassGen[] customClasses = new ClassGen[fileList.length];

		for (int i = 0; i < fileList.length; i++) {
			try {
				ClassParser cp = new ClassParser(fileList[i].getPath());
				JavaClass jc;
				jc = cp.parse();
				customClasses[i] = new ClassGen(jc);
			} catch (ClassFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return customClasses;
	}

	public void writeJar() {
		writeJar(originalFilePath);
	}

	public void writeJar(String path) {
		try {
			File file = new File(path);

			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			FileOutputStream ostream = new FileOutputStream(file);
			JarOutputStream out = new JarOutputStream(ostream);
			
			for (ClassGen cg : classes.values()) {
				try {
					JarEntry je = new JarEntry(cg.getClassName().replace('.', '/') + ".class");

					out.putNextEntry(je);
					out.write(cg.getJavaClass().getBytes());
				} catch (ZipException ze) {
					if (!ze.getMessage().contains("duplicate"))
						ze.printStackTrace();
				}
			}
			
			out.close();
			ostream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new OfficialClientPatcher();
	}
}
