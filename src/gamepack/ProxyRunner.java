package gamepack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ProxyRunner {

	/*public enum ProxySite {
		FreeProxyList("http://free-proxy-list.net");
		
		public final String link;
		
		ProxySite(String link) {
			this.link = link;
		}
	}*/
	
	public static HashMap<String, Integer> fetchProxies(boolean scrape) throws IOException {
		
		HashMap<String, Integer> proxies = new HashMap<String, Integer>();
		
		if (!scrape) {
			FileReader input = new FileReader("proxies.txt");
			BufferedReader bf = new BufferedReader(input);
			String cur = bf.readLine();
	
			while (cur != null) {    
			    String[] splitLine = cur.split(":");
			    
			    if (splitLine.length == 2)
			    	if (splitLine[1].equals("80")
			    			|| splitLine[1].equals("8080")
			    			|| splitLine[1].equals("8088")
			    			|| splitLine[1].equals("8089")
			    			|| splitLine[1].equals("8800")
			    			|| splitLine[1].equals("3128")
			    			|| splitLine[1].equals("3127")
			    			|| splitLine[1].equals("8081")
			    			|| splitLine[1].equals("7808")
			    			|| splitLine[1].equals("8888")
			    			|| splitLine[1].equals("443")
			    			|| splitLine[1].equals("21320"))
			    		proxies.put(splitLine[0], Integer.parseInt(splitLine[1]));
			    	
			    cur = bf.readLine();
			}
			
			bf.close();
		} else {
	        String url = "http://free-proxy-list.net/";

	        Document doc = Jsoup.connect(url).get();
	        Elements rows = doc.select("tbody > tr");

	        System.out.println("Rows: " + rows.size());
	        for (Element row : rows) {
	        	Elements children = row.children();
	        	
	        	Iterator<Element> it = children.iterator();
	        	
	        	while (it.hasNext()) {
	        		Element child = it.next();
	        		
	        		if (StringUtils.countMatches(child.text(), ".") == 3) {
	        			if (it.hasNext()) {
	        				String ip = child.text().trim();
	        				
	        				child = it.next();
	        				
	        				if (StringUtils.isNumeric(child.text().trim())) {
	        					String port = child.text();
	        					
			        			//System.out.println(ip + ":" + port);
			        			proxies.put(ip, Integer.parseInt(port));
			        			
			        			break;
	        				}
	        			}
	        		}
	        	}
	        }
		}
		
		return proxies;
	}
	
	public static void main(String[] args) throws IOException {
		
		boolean infiniteFetch = true;
		
		int fetch = 1;
		
		do {
			HashMap<String, Integer> proxies = fetchProxies(true);
			
			if (proxies == null || proxies.size() == 0)
				return;
			
			//if (true) return;
			
			HashMap<Process, Long> processes = new HashMap<Process, Long>();
			
			int pass = 1;
			
			while (pass <= 2) {
				for (String ip : proxies.keySet()) {
					try {
						System.out.println(ip + ":" + proxies.get(ip).toString());
						
						//System.setProperty("http.proxyHost", ip);
						//System.setProperty("http.proxyPort", proxies.get(ip).toString());
						//System.setProperty("http.proxySet", "true");
						
						//Process p = Runtime.getRuntime().exec("java -Dhttp.proxyHost=" + ip + " -Dhttp.proxyPort=" + proxies.get(ip).toString() + " -cp \"C:\\Documents and Settings\\User\\workspace\\BotUpdater\\bin\" gamepack.GamePackFetcher");
						
						File path = new File(ProxyRunner.class.getClassLoader().getResource(".").toURI());
						//System.out.println(path.getAbsolutePath());
						//File path = new File("C:\\Documents and Settings\\User\\workspace\\BotUpdater\\bin");
						//Process p = Runtime.getRuntime().exec("java -Dhttp.proxyHost=" + ip + " -Dhttp.proxyPort=" + proxies.get(ip).toString() + " -cp \"" + path.getAbsolutePath() + "\\;" + path.getParentFile().getAbsolutePath() + "\\commons-codec-1.8.jar\" gamepack.GamePackFetcher");
						Process p = Runtime.getRuntime().exec("java -Dhttp.proxyHost=" + ip + " -Dhttp.proxyPort=" + proxies.get(ip).toString() + " -cp \"C:\\Documents and Settings\\User\\workspace\\BotInclude\\bin;" + "C:\\Documents and Settings\\User\\workspace\\BotInclude\\commons-codec-1.8.jar;" + "C:\\Documents and Settings\\User\\workspace\\BotInclude\\commons-io-2.3.jar;" + "C:\\Documents and Settings\\User\\workspace\\BotInclude\\asm-5.0_BETA.jar;" + "C:\\Documents and Settings\\User\\workspace\\BotInclude\\asm-tree-5.0_BETA.jar\" gamepack.GamePackFetcher");
						processes.put(p, System.currentTimeMillis());
					} catch (Throwable e) {
						e.printStackTrace();
					}
					
					try {
						Thread.sleep(1000);
						
						Process[] pTemp = processes.keySet().toArray(new Process[0]);
						
						if (pTemp.length >= 10) {
							boolean exit = false;
							
							while (!exit) {
								for (Process p : pTemp) {
									try {
										p.exitValue(); // will throw exception if not terminated
										processes.remove(p);
										exit = true;
									} catch (IllegalThreadStateException e) { // if the subprocess represented by this Process object has not yet terminated
										if ((System.currentTimeMillis() - processes.get(p)) > 5*60*1000) {
											p.destroy();
											processes.remove(p);
											exit = true;
										}
									}
								}
								
								if (exit)
									break;
								
								Thread.sleep(10000);
							}
							
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				System.out.println("Completed pass " + pass);
				pass++;
			}
			
			System.out.println("Completed fetch " + fetch);
			fetch++;
		} while (infiniteFetch);
	}
}
