package report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import boot.BotUpdater;
import data.ClassData;
import data.Client;
import data.FieldData;
import data.MultiplierFieldData;
import data.XMLLoader;
import data.methods.Callbacks;
import data.methods.MethodData;
import data.methods.MultipleMethodData;

public class Report {
	public static StringBuilder printResults(String logFilePath) {
		if (ClassData.classes == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(
				"*********************************" + System.lineSeparator() +
				"          Analysis Report" 		+ System.lineSeparator() +
				"*********************************");
		sb.append(System.lineSeparator());
		
		sb.append("Build Number: " + Client.get.getMajorBuildNumber() + " " + Client.get.getMinorBuildNumber());
		sb.append(System.lineSeparator());
		
		int foundClasses = 0;
		int totalClasses = ClassData.classes.length;
		int foundFields = 0;
		int totalFields = 0;
		int foundMultipliers = 0;
		int totalMultipliers = 0;
		int foundMethods = 0;
		int totalMethods = 0;
		int foundMultipleMethods = 0;
		int totalMultipleMethods = 0;
		
		for (ClassData classData : ClassData.classes) {
			classData.printReport();
			
			if (classData.getInternalName() != null)
				foundClasses++;

			if (classData.fields != null) {
				totalFields += classData.fields.length;
				
				for (FieldData fieldData : classData.fields) {
					if (fieldData instanceof MultiplierFieldData) {
						totalMultipliers++;
						
						if (((MultiplierFieldData) fieldData).getMultiplier() != null)
							foundMultipliers++;
					}
					
					if (fieldData.getInternalClassName() != null
							&& fieldData.getInternalName() != null
							&& fieldData.getSignature() != null) {
						
						foundFields++;
					}
				}
			}
		}
		
		int verifiedFields = verifyFieldSignatures();
		
		
		Callbacks.get.printReport();
		
		totalMethods = Callbacks.get.methods.length;
		
		for (MethodData methodData : Callbacks.get.methods) {
			if (methodData.getInternalClassName() != null
					&& methodData.getInternalName() != null
					&& methodData.getSignature() != null) {
				
				foundMethods++;
			}
		}
		
		totalMultipleMethods = Callbacks.get.multipleMethods.length;
		
		for (MultipleMethodData multipleMethodData : Callbacks.get.multipleMethods) {
			for (MethodData methodData : multipleMethodData.getMethods()) {
				if (methodData.getInternalClassName() != null
						&& methodData.getInternalName() != null
						&& methodData.getSignature() != null) {
					
					foundMultipleMethods++;
					break;
				}
			}
		}
		
		
		sb.append("Found " + foundClasses + "/" + totalClasses + " classes");
		sb.append(System.lineSeparator());
		
		sb.append("Found " + foundFields + "/" + totalFields + " fields");
		sb.append(System.lineSeparator());
		
		sb.append("Found " + foundMultipliers + "/" + totalMultipliers + " multipliers");
		sb.append(System.lineSeparator());
		
		sb.append("Verified " + verifiedFields + "/" + totalFields + " fields");
		sb.append(System.lineSeparator());
		
		sb.append("Found " + foundMethods + "/" + totalMethods + " methods");
		sb.append(System.lineSeparator());
		
		sb.append("Found " + foundMultipleMethods + "/" + totalMultipleMethods + " multiple-methods");
		sb.append(System.lineSeparator());
		
		System.out.println(sb);
		
		if (logFilePath != null) {
			try {
				File logFile = new File(logFilePath);

				BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));
				writer.append(sb);
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return sb;
	}
	
	// @returns  number of fields verified
	public static int verifyFieldSignatures() {
		int verifiedFields = 0;
		
		for (ClassData classData : ClassData.classes) {
			if (classData.fields != null) {
				for (FieldData fieldData : classData.fields) {
					if (!fieldData.verifySignature()) {
						if (fieldData.getSignature() == null) {
							// TODO: uncomment when all field analyzers are complete
							//System.out.println("ERROR: Null Signature [" + classData.getInterfacedName() + "." + fieldData.getInterfacedName()  + "]");
							continue;
						} else if (fieldData.getProperSignature() == null) {
							System.out.println("ERROR: Null properSignature [" + classData.getInterfacedName() + "." + fieldData.getInterfacedName()  + "]");
							continue;
						} else if (fieldData.getProperSignature() instanceof ClassData 
								&& ((ClassData) fieldData.getProperSignature()).getInternalName() == null) {
							System.out.println("ERROR: null internalName [" + classData.getInterfacedName() + "." + fieldData.getInterfacedName()  + "] @ properSignature->ClassData");
							continue;
						}
						
						String properSignature_interfaced = fieldData.getProperSignature_InterfacedName();
						String signature_interfaced = fieldData.getSignature_InterfacedName();
						
						System.out.println("ERROR: Signature Mismatch: [" + classData.getInterfacedName() + "." + fieldData.getInterfacedName()  + "]: " 
								+ signature_interfaced + " should be " + properSignature_interfaced);
					} else
						verifiedFields++;
				}
			}
		}
		
		return verifiedFields;
	}
	
	public static void writeClassesToXML() {
		if (ClassData.classes == null)
			return;
		
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("Data");
			doc.appendChild(rootElement);
			
			Element classesElement = doc.createElement("Classes");
			rootElement.appendChild(classesElement);
			
			for (int i = 0; i < ClassData.classes.length; i++) {
				Element eClass = doc.createElement("Class");
				eClass.setAttribute("id", Integer.toString(i));
				classesElement.appendChild(eClass);
				
				
				Element eClassInterfacedName = doc.createElement("interfacedName");
				String class_interfaceName = ClassData.classes[i].getInterfacedName();
				eClassInterfacedName.appendChild(doc.createTextNode((class_interfaceName != null) ? class_interfaceName : XMLLoader.errorFieldString));
				eClass.appendChild(eClassInterfacedName);
				
				Element eClassInternalName = doc.createElement("internalName");
				String class_internalName = ClassData.classes[i].getInternalName();
				eClassInternalName.appendChild(doc.createTextNode((class_internalName != null) ? class_internalName : XMLLoader.errorFieldString));
				eClass.appendChild(eClassInternalName);
				
				
				Element eFields = doc.createElement("Fields");
				eClass.appendChild(eFields);
				
				if (ClassData.classes[i].fields != null) {
					for (int f = 0; f < ClassData.classes[i].fields.length; f++) {
						Element eField = doc.createElement("Field");
						eField.setAttribute("id", Integer.toString(f));
						eFields.appendChild(eField);
						
						
						Element eFieldInterfacedName = doc.createElement("interfacedName");
						String field_interfacedName = ClassData.classes[i].fields[f].getInterfacedName();
						eFieldInterfacedName.appendChild(doc.createTextNode((field_interfacedName != null) ? field_interfacedName : XMLLoader.errorFieldString));
						eField.appendChild(eFieldInterfacedName);
						
						Element eFieldInternalClassName = doc.createElement("internalClassName");
						String field_internalClassName = ClassData.classes[i].fields[f].getInternalClassName();
						eFieldInternalClassName.appendChild(doc.createTextNode((field_internalClassName != null) ? field_internalClassName : XMLLoader.errorFieldString));
						eField.appendChild(eFieldInternalClassName);
						
						Element eFieldInternalName = doc.createElement("internalName");
						String field_internalName = ClassData.classes[i].fields[f].getInternalName();
						eFieldInternalName.appendChild(doc.createTextNode((field_internalName != null) ? field_internalName : XMLLoader.errorFieldString));
						eField.appendChild(eFieldInternalName);
						
						Element eSignature = doc.createElement("signature");
						String field_signature = ClassData.classes[i].fields[f].getSignature();
						eSignature.appendChild(doc.createTextNode((field_signature != null) ? field_signature : XMLLoader.errorFieldString));
						eField.appendChild(eSignature);
						
						if (ClassData.classes[i].fields[f] instanceof MultiplierFieldData) {
							eField.setAttribute("multiplier", "true");
							eFields.appendChild(eField);
							
							Element eMultiplier = doc.createElement("multiplier");
							Number multiplier = ((MultiplierFieldData)ClassData.classes[i].fields[f]).getMultiplier();
							String field_multiplier = (multiplier != null) ? multiplier.toString() : null;
							eMultiplier.appendChild(doc.createTextNode((field_multiplier != null) ? field_multiplier : XMLLoader.errorFieldString));
							eField.appendChild(eMultiplier);
						}
					}
				}
			}
			
			
			Element methodsElement = doc.createElement("Methods");
			rootElement.appendChild(methodsElement);
			
			MethodData[] methods = Callbacks.get.methods;
			
			for (int i = 0; i < methods.length; i++) {
				Element eMethod = doc.createElement("Method");
				eMethod.setAttribute("id", Integer.toString(i));
				methodsElement.appendChild(eMethod);
				
				
				Element eMethodInterfacedName = doc.createElement("interfacedName");
				String method_interfaceName = methods[i].getInterfacedName();
				eMethodInterfacedName.appendChild(doc.createTextNode((method_interfaceName != null) ? method_interfaceName : XMLLoader.errorFieldString));
				eMethod.appendChild(eMethodInterfacedName);
				
				Element eMethodInternalClassName = doc.createElement("internalClassName");
				String method_internalClassName = methods[i].getInternalClassName();
				eMethodInternalClassName.appendChild(doc.createTextNode((method_internalClassName != null) ? method_internalClassName : XMLLoader.errorFieldString));
				eMethod.appendChild(eMethodInternalClassName);
				
				Element eMethodInternalName = doc.createElement("internalName");
				String method_internalName = methods[i].getInternalName();
				eMethodInternalName.appendChild(doc.createTextNode((method_internalName != null) ? method_internalName : XMLLoader.errorFieldString));
				eMethod.appendChild(eMethodInternalName);
				
				Element eMethodSignature = doc.createElement("signature");
				String method_signature = methods[i].getSignature();
				eMethodSignature.appendChild(doc.createTextNode((method_signature != null) ? method_signature : XMLLoader.errorFieldString));
				eMethod.appendChild(eMethodSignature);
			}
			
			
			Element multipleMethodsElement = doc.createElement("MultipleMethods");
			rootElement.appendChild(multipleMethodsElement);
			
			MultipleMethodData[] multipleMethods = Callbacks.get.multipleMethods;
			
			for (int i = 0; i < multipleMethods.length; i++) {
				Element eMultipleMethod = doc.createElement("MultipleMethod");
				eMultipleMethod.setAttribute("id", Integer.toString(i));
				multipleMethodsElement.appendChild(eMultipleMethod);
				
				Element eMultipleMethodInterfacedName = doc.createElement("interfacedName");
				String multipleMethod_interfaceName = multipleMethods[i].getInterfacedName();
				eMultipleMethodInterfacedName.appendChild(doc.createTextNode((multipleMethod_interfaceName != null) ? multipleMethod_interfaceName : XMLLoader.errorFieldString));
				eMultipleMethod.appendChild(eMultipleMethodInterfacedName);
				
				Element eMethods = doc.createElement("Methods");
				eMultipleMethod.appendChild(eMethods);
				
				methods = multipleMethods[i].getMethods().toArray(new MethodData[0]);
				
				for (int i2 = 0; i2 < methods.length; i2++) {
					Element eMethod = doc.createElement("Method");
					eMethod.setAttribute("id", Integer.toString(i2));
					eMethods.appendChild(eMethod);
					
					
					Element eMethodInterfacedName = doc.createElement("interfacedName");
					String method_interfaceName = methods[i2].getInterfacedName();
					eMethodInterfacedName.appendChild(doc.createTextNode((method_interfaceName != null) ? method_interfaceName : XMLLoader.errorFieldString));
					eMethod.appendChild(eMethodInterfacedName);
					
					Element eMethodInternalClassName = doc.createElement("internalClassName");
					String method_internalClassName = methods[i2].getInternalClassName();
					eMethodInternalClassName.appendChild(doc.createTextNode((method_internalClassName != null) ? method_internalClassName : XMLLoader.errorFieldString));
					eMethod.appendChild(eMethodInternalClassName);
					
					Element eMethodInternalName = doc.createElement("internalName");
					String method_internalName = methods[i2].getInternalName();
					eMethodInternalName.appendChild(doc.createTextNode((method_internalName != null) ? method_internalName : XMLLoader.errorFieldString));
					eMethod.appendChild(eMethodInternalName);
					
					Element eMethodSignature = doc.createElement("signature");
					String method_signature = methods[i2].getSignature();
					eMethodSignature.appendChild(doc.createTextNode((method_signature != null) ? method_signature : XMLLoader.errorFieldString));
					eMethod.appendChild(eMethodSignature);
				}
			}
	 
			
			// write the content into xml file
			File modScriptDirectory = new File("C:\\modscript\\" + Client.get.getBuildNumberString() + "\\");
			File modScriptFile = new File(modScriptDirectory, "modscript.xml");

			if (BotUpdater.gamepackFetcher != null)
				modScriptFile = new File(modScriptDirectory, BotUpdater.gamepackFetcher.getSha1Hex() + ".xml");
			else {
				String fileName = new File(BotUpdater.originalFilePath).getName().replaceFirst("[.][^.]+$", ""); // remove extension
				modScriptFile = new File(modScriptDirectory, fileName + ".xml");
			}
			
			if (!modScriptFile.exists()) {
				modScriptFile.getParentFile().mkdirs();
				
				try {
					modScriptFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(modScriptFile);
	 
			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
	 
			transformer.transform(source, result);
	 
			System.out.println("Modscript written to " + modScriptFile.getAbsolutePath());
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
	}
}